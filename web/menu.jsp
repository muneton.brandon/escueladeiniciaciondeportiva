<%-- 
    Document   : menu
    Created on : 11/03/2020, 03:42:37 PM
    Author     : ecardona
    Version    : 01
    Descriocion: Este es el menú principal, recibe el perfil (admin o Docente) y le muestra las opciones de acuerdo a esto
                 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <title>Menú Principal</title>
    </head>
    <body>
        <%String perfil=request.getParameter("perfil");%>    

        <%if ("Admin".equals(perfil)){%>
  
        <nav class="navbar navbar-dark bg-success">
        <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
        <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
        <ul class="nav nav-pills">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Administración</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=usuarios&perfil=${tipoUsuario}" style="color: black" id="usuarios" name="usuarios">
                    Usuarios</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">
                    Acudientes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">
                    Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=docente&perfil=${tipoUsuario}" style="color: black">
                    Docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?resultadoOperacion=En linea&procedencia=pazYSalvo&perfil=
                   ${tipoUsuario}" style="color: black">Cargue Masivo Paz y Salvo</a>
                </div>
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" name="matriculas" id="matriculas">Matriculas</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">
                      Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">
                      Detalle Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">
                      Seguimiento</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Informes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">
                    Resumen general de grupos</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">
                    Top 10 mejores estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">
                    Paz y salvo Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 4</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 5</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Reportes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=Cerficado&perfil=${tipoUsuario}" style="color: black">Certificado de Matrícula estudiante</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoEstudiantes" style="color: black">Listado de estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoDocentes" style="color: black">Listado de docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupo&perfil=${tipoUsuario}" style="color: black">Reporte de notas final</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoGanaron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Ganaron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoPerdieron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Perdieron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirFactura&perfil=${tipoUsuario}" style="color: black">Imprimir factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirPrefactura&perfil=${tipoUsuario}" style="color: black">Imprimir pre-factura</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Facturación</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=Factura&perfil=${tipoUsuario}" style="color: black">Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=FacturaAnulada&perfil=${tipoUsuario}" style="color: black">Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFactura&perfil=${tipoUsuario}" style="color: black">Pre Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAnulada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAprobada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Aprobadas</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Tablas</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=deporte&perfil=${tipoUsuario}" style="color: black">
                    Deportes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=categoria&perfil=${tipoUsuario}" style="color: black">
                    Categorias</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=tipoDocumento&perfil=${tipoUsuario}" style="color: black">
                    Tipos de documentos</a>
                <div class="dropdown-divider"></div>      
                <a class="dropdown-item" href="consultas.jsp?procedencia=estado&perfil=${tipoUsuario}" style="color: black">
                    Estado</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=genero&perfil=${tipoUsuario}" style="color: black">
                    Genero</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Acerca de</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">
                    Ver información</a>
                </div>
            </div>
        </ul>
        <!--
        <div class="row justify-content-end ">
            <div class="col-1"></div>
            <div class="">
                <form action="" method="post" class="form-inline">
                    <!--class="form-inline= en la misma linea
                    <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                    <button class="btn btn-warning " type="reset" >Buscar</button>
                </form>
            </div>    
        </div>
        -->
        <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
            </div>
        </nav>

        <!--MENÚ NUEVO-->
        <div class="container">
            <p>Bienvenido al sistema: <b>${correo}</b><br>
            Su perfil es: <%=perfil%></p>
                <div class="row">
                    <div class="col">            
                        <div id="demo" class="carousel slide" data-ride="carousel">                               
                            <!--INDICADORES-->
                            <ul class="carousel-indicators">
                                <li data-target="demo" data-slide-to="0" class="active"></li>
                                <li data-target="demo" data-slide-to="1"></li>
                                <li data-target="demo" data-slide-to="2"></li>
                            </ul>            
                            <!--IMAGENES-->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="img/bienvenido.png" class="img-fluid ">
                                    <!--img-fluid=  evita que si la imagen es muy grande se va a salir de la pagina y va a 
                                    crear un escroll, que sea adaptable al ancho-->
                                    <div class="carousel-caption">
                                        <h3>A LA ESCUELA DE INICIACION DEPORTIVA </h3>
                                        <p>........................</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="img/inscripciones-educacion-continua-2020-1.png" class="img-fluid">                                       
                                    <div class="carousel-caption">
                                    </div>
                                </div>           
                                <div class="carousel-item">
                                    <img src="img/fondo-inicio.jpg" class="img-fluid ">
                                    <!--img-fluid=  evita que si la imagen es muy grande se va a salir de la pagina y va a crear un 
                                    escroll, que sea adaptable al ancho-->
                                    <div class="carousel-caption">
                                            <h3>POLITECNICO JAIME ISAZA CADAVID</h3>
                                            <p>Universidad para todos</p>
                                    </div>
                                </div>
                            </div>            
                            <!--CONTROLES DE LA IZQUIERDA Y DERECHA-->
                            <a href="#demo"  class="carousel-control-prev" data-slide="prev"><span 
                                    class="carousel-control-prev-icon"></span></a>
                            <a href="#demo"  class="carousel-control-next" data-slide="next"><span 
                                    class="carousel-control-next-icon"></span></a>
                        </div>
                    </div>
                </div>
            </div>     
        <%}%>
        
        <%if ("Docente".equals(perfil)){%>
        <nav class="navbar navbar-dark bg-success">
        <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
        <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
        <ul class="nav nav-pills">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Administración</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">
                    Acudientes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">
                    Estudiantes</a>
                </div>
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Matriculas</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">
                      Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">
                      Detalle Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">
                      Seguimiento</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Informes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">
                    Resumen general de grupos</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">
                    Top 10 mejores estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">
                    Paz y salvo Estudiantes</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Acerca de</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">
                    Ver información</a>
                </div>
            </div>
        </ul>
        <div class="row justify-content-end ">
            <div class="col-1"></div>
            <div class="">
                <form action="#" method="post" class="form-inline">
                    <!--class="form-inline= en la misma linea-->
                    <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                    <button class="btn btn-warning " type="reset" >Buscar</button>
                </form>
            </div>    
        </div>
        <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
        </div>
        </nav>
            <div class="container">
                <p>Bienvenido al sistema: <b>${correo}</b><br>
                Su perfil es: <%=perfil%></p>
                <div class="row">
                    <div class="col">            
                        <div id="demo" class="carousel slide" data-ride="carousel">                               
                            <!--INDICADORES-->
                            <ul class="carousel-indicators">
                                <li data-target="demo" data-slide-to="0" class="active"></li>
                                <li data-target="demo" data-slide-to="1"></li>
                                <li data-target="demo" data-slide-to="2"></li>
                            </ul>            
                            <!--IMAGENES-->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="img/bienvenido.png" class="img-fluid ">
                                    <!--img-fluid=  evita que si la imagen es muy grande se va a salir de la pagina y va a 
                                    crear un escroll, que sea adaptable al ancho-->
                                    <div class="carousel-caption">
                                        <h3>A LA ESCUELA DE INICIACION DEPORTIVA </h3>
                                        <p>........................</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="img/inscripciones-educacion-continua-2020-1.png" class="img-fluid">                                       
                                    <div class="carousel-caption">
                                    </div>
                                </div>           
                                <div class="carousel-item">
                                    <img src="img/fondo-inicio.jpg" class="img-fluid ">
                                    <!--img-fluid=  evita que si la imagen es muy grande se va a salir de la pagina y va a crear
                                    un escroll, que sea adaptable al ancho-->
                                    <div class="carousel-caption">
                                            <h3>POLITECNICO JAIME ISAZA CADAVID</h3>
                                            <p>Universidad para todos</p>
                                    </div>
                                </div>
                            </div>            
                            <!--CONTROLES DE LA IZQUIERDA Y DERECHA-->
                            <a href="#demo"  class="carousel-control-prev" data-slide="prev"><span 
                                    class="carousel-control-prev-icon"></span></a>
                            <a href="#demo"  class="carousel-control-next" data-slide="next"><span 
                                    class="carousel-control-next-icon"></span></a>
                        </div>
                    </div>
                </div>
            </div>       
        <%}%>
        
        <br>
        <div class="col-12">
            <p class="mt-2 mb-3 text-muted text-center ">&copy;
                Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4)
                319
                79 00 |
                NIT: 890980136-6<br>
                Institución de Educación Superior de caracter pública y departamental sujeta a
                inspección y
                vigilancia
                por parte del Ministerio de Educación.
                <img src="img/icontec.png" width="70px">           
            </p>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
