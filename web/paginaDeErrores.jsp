<%-- 
    Document   : paginaDeErrores
    Created on : 11/03/2020, 09:05:09 PM
    Author     : ecardona
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Error</title>
    </head>
    <body>
        <div class=" container" >
        <div class="row">
            <div class="col-12 badge-success fixed-top ">
                <p></p>
            </div>
            <div class="col-12 badge-success fixed-bottom ">
                <p></p>
            </div>
        <div class="container " style="margin-top:130px">
            <div class="row justify-content-center ">
                <div class="col-4 border ">
                <form name="FormularioError" action="index.jsp" method="post">
                    <div class="form-group text-center">
                        <img src="imagenes/oops-robot.png" width=300" height="250"/>
                        <h3>Error</h3>
                    </div>
                    <input class="btn btn-success btn-block" type="submit" name="accion" value="Regresar">
                </form>
                </div>
            </div>
        </div>
        </div>
            <p class="mt-2 mb-3 text-muted text-center">&copy;
                    Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                    Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |
                    NIT: 890980136-6<br>
                    Institución de Educación Superior de caracter pública y departamental sujeta a inspección y
                    vigilancia
                    por parte del Ministerio de Educación.
                <img src="img/icontec.png" width="70px">
            </p>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
