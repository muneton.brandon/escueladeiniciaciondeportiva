<%-- 
    Document   : index
    Created on : 11/03/2020, 03:42:37 PM
    Author     : ecardona
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!--<link href="css2/estilosFormularios.css" rel="stylesheet" type="text/css"/>-->
        <title>Login</title>
    </head>
    
    
    <body>
        <div class=" container" >
        <div class="row">
            <div class="col-12 badge-success fixed-top ">
                <p></p>
            </div>
            <div class="col-12 badge-success fixed-bottom ">
                <p></p>
            </div>
            <div class="container " >
                <div class="row justify-content-center ">
                    <div class="col-4 border ">
                        <div class="text-center "><br>
                            <img class=" w-75 justify-content-center " src="img/logo.png"><br><br>
                            <h5>ESCUELA DE INICIACION DEPORTIVA</h5> <br>
                            <h7>Iniciar sesion</h7>
                        </div>
            
                    <form class="form" name="Login" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Bienvenidos al sistema</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Correo:</label>
                            <input class="form-control" type="email" name="email" placeholder="Ingrese su correo del Poli JIC" required="true">
                        </div>
                        <div class="form-group">
                            <label>Clave:</label>
                            <input class="form-control" type="password" name="clave" placeholder="Ingrese su clave" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Acceder al Sistema">
                        <a href="formularioRecuperacion.jsp?resultadoOperacion=En linea" style="color: gray"> <font size="2"><i>¿olvidó su clave?</i></font> </a>
                    </form>
                    </div>
                </div>
            <p class="mt-2 mb-3 text-muted text-center">&copy;
                    Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                    Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |
                    NIT: 890980136-6<br>
                    Institución de Educación Superior de caracter pública y departamental sujeta a inspección y
                    vigilancia
                    por parte del Ministerio de Educación.
                <img src="img/icontec.png" width="70px">
            </p>
            </div>
        </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
