
package persistencia;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Grupo;

/**
 * La clase GrupoDAO recibe un objeto de la clase Grupo
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class GrupoDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
 
    
    
/**
 * el método GuardarGrupo recibe un objeto de la clase Grupo y guarda los datos de 
 * ese objeto en la bd y devuelve el resultado de esa operación
 * @param gp Grupo
 * @return String Resulado de la operación
 */
       
    public String GuardarGrupo (Grupo gp){
        int r=0;
        String sql="INSERT INTO grupo(idgrupo, deporte_iddeporte, categoria_idcategoria,"
                + "horarioiniciogrupo, horariofingrupo, objetivogrupo,docente_id_docente)"
                + "values (?,?,?,?,?,?,?)";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, gp.getIdGrupo());
            ps.setInt(2, gp.getIdDeporte());
            ps.setInt(3, gp.getIdCategoria());
            ps.setString(4, gp.getHorarioInicio());
            ps.setString(5, gp.getHorarioFin());
            ps.setString(6, gp.getObjetivo());
            ps.setInt(7, gp.getIdDocente());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    
/**
 * el método ActualizarGrupo recibe un objeto de la clase Grupo y actualiza los datos de
 * ese objeto en la bd y devuelve el resulado de esa operación
 * @param gp Grupo
 * @return String Resultado de esa operación
 */   
    
    public String ActualizarGrupo(Grupo gp){
        int r=0;
        String sql="UPDATE grupo SET idgrupo = ?,deporte_iddeporte = ?,"
                + "categoria_idcategoria = ?,horarioiniciogrupo = ?,"
                + "horariofingrupo = ?,objetivogrupo = ?, docente_id_docente = ? WHERE idgrupo = ?";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, gp.getIdGrupo());
            ps.setInt(2, gp.getIdDeporte());
            ps.setInt(3, gp.getIdCategoria());
            ps.setString(4, gp.getHorarioInicio());
            ps.setString(5, gp.getHorarioFin());
            ps.setString(6, gp.getObjetivo());
            ps.setInt(7, gp.getIdDocente());
            ps.setInt(8, gp.getIdGrupo());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
        
    }

/**
* el método BorrarGrupo recibe un objeto de la clase Grupo y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param gp Grupo
* @return String Resultado de la operación
 */
    
    public String BorrarGrupo(Grupo gp){
        int r=0;
        String sql="DELETE FROM grupo WHERE idgrupo = ? AND deporte_iddeporte = ? "
                + "AND categoria_idcategoria = ?";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, gp.getIdGrupo());
            ps.setInt(2, gp.getIdDeporte());
            ps.setInt(3, gp.getIdCategoria());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }

   
/**
* el método ConsultarGrupo consulta todos los datos de la tabla Grupo de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de grupos
*/ 
    
    public List<Grupo> ConsultarGrupos(){
        int r=0;
        List<Grupo> result = new ArrayList();
        String sql="SELECT idgrupo, deporte_iddeporte, categoria_idcategoria, horarioiniciogrupo,"
                + " horariofingrupo, objetivogrupo, docente_id_docente FROM grupo";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Grupo gp = new Grupo();
                gp.setIdGrupo(rs.getInt("idgrupo"));
                gp.setIdDeporte(rs.getInt("deporte_iddeporte"));
                gp.setIdCategoria(rs.getInt("categoria_idcategoria"));
                gp.setHorarioInicio(rs.getString("horarioiniciogrupo"));
                gp.setHorarioFin(rs.getString("horariofingrupo"));
                gp.setObjetivo(rs.getString("objetivogrupo"));
                gp.setIdDocente(rs.getInt("docente_id_docente"));
                result.add(gp);
            }
        } catch (Exception e){
         }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
        }
    }
    
    
