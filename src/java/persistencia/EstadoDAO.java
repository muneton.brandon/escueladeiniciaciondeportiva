
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import utilidades.ConexionBD;
import modelo.Estado;

/**
 * La clase EstadoDAO recibe un objeto de la clase Estado
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class EstadoDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
    /**
* el método GuardarEstado recibe un objeto de la clase estado y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param es estado
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarEstado(Estado es) {
        int r=0;
        String sql="INSERT INTO estado(idestado,descripcionestado) values(?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, es.getId());
            ps.setString(2, es.getNombreEstado());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarEstado recibe un objeto de la clase estado y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param es estado
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarEstado(Estado es) {
        int r=0;
        String sql="UPDATE estado SET idestado = ?, descripcionestado = ? WHERE idestado = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, es.getId());
            ps.setString(2, es.getNombreEstado());
            ps.setInt(3, es.getId());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarEstado recibe un objeto de la clase estado y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param es estado
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarEstado(Estado es) {
        int r=0;
        String sql="DELETE FROM Estado WHERE idestado = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, es.getId());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarEstado consulta todos los datos de la tabla Estado de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de estados
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Estado> ConsultarEstados() {
        int r=0;
        List<Estado> result = new ArrayList();
        String sql="SELECT idestado, descripcionestado FROM Estado";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Estado es = new Estado();
                es.setId(rs.getInt("idestado"));
                es.setNombreEstado(rs.getString("descripcionestado"));
                result.add(es);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
