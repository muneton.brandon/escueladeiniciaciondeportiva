
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.DetalleGrupo;

/**
 * La clase DetalleGrupoDAO recibe un objeto de la clase DetalleGrupoDAO
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class DetalleGrupoDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
    
/**
* el método GuardarDetGrupo recibe un objeto de la clase DetalleGrupo y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param DetG DetalleGrupo
* @return String Resultado de la operación
*/     
    public String GuardarDetGrupo(DetalleGrupo DetG) {
        int r=0;
        String sql="INSERT INTO grupodetalle(grupo_idgrupo, estudiante_identestudiante, notafinalestud) "
                + "VALUES (?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, DetG.getIdGrupo());
            ps.setInt(2, DetG.getIdEstudiante());
            ps.setFloat(3, DetG.getNotaFinalEst());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarDetGrupo recibe un objeto de la clase DetalleGrupo y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param DetG DetalleGrupo
* @return String Resultado de la operación
*/ 
    public String ActualizarDetGrupo(DetalleGrupo DetG) {
        int r=0;
        String sql="UPDATE grupodetalle SET grupo_idgrupo = ?, estudiante_identestudiante = ?,"
                + "notafinalestud = ? WHERE grupo_idgrupo = ? And estudiante_identestudiante = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, DetG.getIdGrupo());
            ps.setInt(2, DetG.getIdEstudiante());
            ps.setFloat(3, DetG.getNotaFinalEst());
            ps.setInt(4, DetG.getIdGrupo());
            ps.setInt(5, DetG.getIdEstudiante());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarDetGrupo recibe un objeto de la clase DetalleGrupo y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param DetG DetalleGrupo
* @return String Resultado de la operación
*/ 
    public String BorrarDetGrupo(DetalleGrupo DetG) {
        int r=0;
        String sql="DELETE FROM grupodetalle WHERE (grupo_idgrupo = ?) And (estudiante_idestudiante = ?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, DetG.getIdGrupo());
            ps.setInt(2, DetG.getIdEstudiante());
            ps.execute();
            r=r+1; 
            return "Corecto";
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarDetGrupo consulta todos los datos de la tabla DetalleGrupo de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de DetalleGrupos
*/ 
    public List<DetalleGrupo> ConsultarDetGrupo() {
        int r=0;
        List<DetalleGrupo> result = new ArrayList();
        String sql="SELECT grupo_idgrupo, estudiante_identestudiante, notafinalestud FROM grupodetalle";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                DetalleGrupo DetG= new DetalleGrupo();
                DetG.setIdGrupo(rs.getInt("grupo_idgrupo"));
                DetG.setIdEstudiante(rs.getInt("estudiante_identestudiante"));
                DetG.setNotaFinalEst(rs.getFloat("notafinalestud"));
                result.add(DetG);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
