
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Deporte;

/**
 * La clase DeporteDAO recibe un objeto de la clase Deporte
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class DeporteDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método GuardarDeporte recibe un objeto de la clase Deporte y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d deporte
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarDeporte(Deporte d) {
        int r=0;
        String sql="INSERT INTO deporte(iddeporte,nombredeporte) values(?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, d.getIdDeporte());
            ps.setString(2, d.getDescripcionDeporte());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarDeporte recibe un objeto de la clase Deporte y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d Deporte
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarDeporte(Deporte d) {
        int r=0;
        String sql="UPDATE deporte SET iddeporte = ?, nombredeporte = ? WHERE iddeporte = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, d.getIdDeporte());
            ps.setString(2, d.getDescripcionDeporte());
            ps.setInt(3, d.getIdDeporte());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarDeporte recibe un objeto de la clase Deporte y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d Deporte
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarDeporte(Deporte d) {
        int r=0;
        String sql="DELETE FROM deporte WHERE iddeporte = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, d.getIdDeporte());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarDeporte consulta todos los datos de la tabla Deporte de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de deportes
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Deporte> ConsultarDeportes() {
        int r=0;
        List<Deporte> result = new ArrayList();
        String sql="SELECT iddeporte, nombredeporte FROM deporte";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Deporte d = new Deporte();
                d.setIdDeporte(rs.getInt("iddeporte"));
                d.setDescripcionDeporte(rs.getString("nombredeporte"));
                result.add(d);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
