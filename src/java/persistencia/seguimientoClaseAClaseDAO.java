
package persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.seguimientoClaseAClase;

/**
 * La clase seguimientoClaseAClaseDAO recibe un objeto de la clase
 * seguimientoClaseAClase y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class seguimientoClaseAClaseDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método GuardarControlAsistencia recibe un objeto de la clase seguimientoClaseAClase y guarda los datos de
 ese objeto en la bd y devuelve el resultado de esa operación
* @param Cas seguimientoClaseAClase
* @return String Resultado de la operación
*/     
    public String GuardarControlAsistencia(seguimientoClaseAClase Cas) {
        int r=0;
        String sql="INSERT INTO seguimientoclaseaclase(grupo_idgrupo, estudiante_identestudiante, "
                + "fechaclasecontrol, objetivoclasecontrol, notaclasecontrol, observclasecontrol)"
                + "values(?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, Cas.getIdGrupo());
            ps.setInt(2, Cas.getIdEstudiante());
            ps.setDate(3, Cas.getFechaClaseControl());
            ps.setString(4, Cas.getObjetivosClaseControl());
            ps.setFloat(5, Cas.getNotaClaseControl());
            ps.setString(6, Cas.getObervClaseControl());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarControlAsistencia recibe un objeto de la clase seguimientoClaseAClase y actualiza los datos de
 ese objeto en la bd y devuelve el resultado de esa operación
* @param Cas seguimientoClaseAClase
* @return String Resultado de la operación
*/ 
    public String ActualizarControlAsistencia(seguimientoClaseAClase Cas) {
        int r=0;
        String sql="UPDATE seguimientoclaseaclase SET grupo_idgrupo = ?,estudiante_identestudiante = ?,"
                + "fechaclasecontrol = ?, objetivoclasecontrol = ?, notaclasecontrol = ?,"
                + "observclasecontrol = ? WHERE (grupo_idgrupo = ?) And (estudiante_identestudiante = ?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, Cas.getIdGrupo());
            ps.setInt(2, Cas.getIdEstudiante());
            ps.setDate(3, Cas.getFechaClaseControl());
            ps.setString(4, Cas.getObjetivosClaseControl());
            ps.setFloat(5, Cas.getNotaClaseControl());
            ps.setString(6, Cas.getObervClaseControl());
            ps.setInt(7, Cas.getIdGrupo());
            ps.setInt(8, Cas.getIdEstudiante());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarControlAsistencia recibe un objeto de la clase seguimientoClaseAClase y borra los datos de
 ese objeto en la bd y devuelve el resultado de esa operación
* @param Cas seguimientoClaseAClase
* @return String Resultado de la operación
*/ 
    public String BorrarControlAsistencia(seguimientoClaseAClase Cas) {
        int r=0;
        String sql="DELETE FROM seguimientoclaseaclase WHERE (grupo_idgrupo = ?) And (estudiante_identestudiante = ?)"
                + "AND (fechaclasecontrol = ?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, Cas.getIdGrupo());
            ps.setInt(2, Cas.getIdEstudiante());
            ps.setDate(3, Cas.getFechaClaseControl());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarControlAsistencia consulta todos los datos de la tabla seguimientoClaseAClase de
 la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de control de asistencia
*/ 
    public List<seguimientoClaseAClase> ConsultarSeguimiento() {
        int r=0;
        List<seguimientoClaseAClase> result = new ArrayList();
        String sql="SELECT grupo_idgrupo, estudiante_identestudiante, fechaclasecontrol, objetivoclasecontrol,"
                + " notaclasecontrol, observclasecontrol FROM seguimientoclaseaclase";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                seguimientoClaseAClase Cas = new seguimientoClaseAClase();
                Cas.setIdGrupo(rs.getInt("grupo_idgrupo"));
                Cas.setIdEstudiante(rs.getInt("estudiante_identestudiante"));
                Cas.setFechaClaseControl(rs.getDate("fechaclasecontrol"));
                Cas.setObjetivosClaseControl(rs.getString("objetivoclasecontrol"));
                Cas.setNotaClaseControl(rs.getFloat("notaclasecontrol"));
                Cas.setObervClaseControl(rs.getString("observclasecontrol"));
                result.add(Cas);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
/**
* el método ConvertirFecha recibe un string con una fecha
* y la convierte en una fecha del tipo java.sql.date
* @param fechaRecibida string
* @return fecha del tipo java.sql.date
* @since abril 2020
* @version 1
* @author ecardona
*/ 
    public Date ConvertirFecha(String fechaRecibida) {
        Date convertido=null;
        try {
            //En este caso buscará en el String dia/mes/año
            DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
            convertido = new java.sql.Date(fecha.parse(fechaRecibida).getTime());
            return convertido;
            } catch (ParseException e) {
                    System.out.println("Error: " + e.getMessage());
            }
        return convertido;
    }
}
