
package persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Estudiante;

/**
 * La clase EstudianteDAO recibe un objeto de la clase Estudiante
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class EstudianteDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método GuardarEstudiante recibe un objeto de la clase Estudiante y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param e Estudiante
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarEstudiante(Estudiante e) {
        int r=0;
        String sql="INSERT INTO estudiante(tipodocumento_idtipodocumento, identestudiante, nombreestudiante,"
                + "fechanacimientoestudiante, direccionestudiante, telefonoestudiante, genero_idgenero, estado_idestado,"
                + "acudiente_identacudiente) values(?,?,?,?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, e.getTipoIdentificacionEstudiante());
            ps.setInt(2, e.getIdentificacionEstudiante());
            ps.setString(3, e.getNombreEstudiante());
            ps.setDate(4, e.getFechaNacimientoEstudiante());
            ps.setString(5, e.getDireccionEstudiante());
            ps.setString(6, e.getTelefonoEstudiante());
            ps.setString(7, e.getGeneroEstudiante());
            ps.setString(8, e.getEstadoEstudiante());
            ps.setInt(9, e.getAcudienteEstudiante());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception ex){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarEstutiante recibe un objeto de la clase Estudiante y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param e Estudiante
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarEstudiante(Estudiante e) {
        int r=0;
        String sql="UPDATE estudiante SET tipodocumento_idtipodocumento = ?, identestudiante = ?, nombreestudiante = ?,"
                + "fechanacimientoestudiante = ?, direccionestudiante = ?, telefonoestudiante = ?, genero_idgenero = ?,"
                + "estado_idestado = ?, acudiente_identacudiente = ? WHERE identestudiante = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, e.getTipoIdentificacionEstudiante());
            ps.setInt(2, e.getIdentificacionEstudiante());
            ps.setString(3, e.getNombreEstudiante());
            ps.setDate(4, e.getFechaNacimientoEstudiante());
            ps.setString(5, e.getDireccionEstudiante());
            ps.setString(6, e.getTelefonoEstudiante());
            ps.setString(7, e.getGeneroEstudiante());
            ps.setString(8, e.getEstadoEstudiante());
            ps.setInt(9, e.getAcudienteEstudiante());
            ps.setInt(10, e.getIdentificacionEstudiante());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception ex){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarEstudiante recibe un objeto de la clase Estudiante y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param e Estudiante
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarEstudiante(Estudiante e) {
        int r=0;
        String sql="DELETE FROM estudiante WHERE identestudiante = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, e.getIdentificacionEstudiante());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception ex){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarEstudiante consulta todos los datos de la tabla estudiante de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de estudiantes
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Estudiante> ConsultarEstudiantes() {
        int r=0;
        List<Estudiante> result = new ArrayList();
        String sql="SELECT tipodocumento_idtipodocumento, identestudiante, nombreestudiante,"
                + "fechanacimientoestudiante, direccionestudiante, telefonoestudiante, genero_idgenero, estado_idestado,"
                + "acudiente_identacudiente, pazysalvoestudiante FROM estudiante";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Estudiante e = new Estudiante();
                e.setTipoIdentificacionEstudiante(rs.getString("tipodocumento_idtipodocumento"));
                e.setIdentificacionEstudiante(rs.getInt("identestudiante"));
                e.setNombreEstudiante(rs.getString("nombreestudiante"));
                e.setFechaNacimientoEstudiante(rs.getDate("fechanacimientoestudiante"));
                e.setDireccionEstudiante(rs.getString("direccionestudiante"));
                e.setTelefonoEstudiante(rs.getString("telefonoestudiante"));
                e.setGeneroEstudiante(rs.getString("genero_idgenero"));
                e.setEstadoEstudiante(rs.getString("estado_idestado"));
                e.setAcudienteEstudiante(rs.getInt("acudiente_identacudiente"));
                e.setPazYSalvoEstudiante(rs.getString("pazysalvoestudiante"));
                result.add(e);
            }
        } catch (Exception ex){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    public String ActualizarPazYSalvoEstudiante(String idest, String PazySalvo) {
        int r=0;
        String sql="UPDATE estudiante SET pazysalvoestudiante = ? WHERE identestudiante = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, PazySalvo);
            ps.setInt(2, Integer.parseInt(idest.replace(" ","")));
            
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception ex){
            return "Error en BD" + ex;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }

/**
* el método ConvertirFecha recibe un string con una fecha
* y la convierte en una fecha del tipo java.sql.date
* @param fechaRecibida string
* @return fecha del tipo java.sql.date
* @since abril 2020
* @version 1
* @author ecardona
*/ 
    public Date ConvertirFecha(String fechaRecibida) {
        Date convertido=null;
        try {
            //En este caso buscará en el String dia/mes/año
            DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
            convertido = new java.sql.Date(fecha.parse(fechaRecibida).getTime());
            return convertido;
            } catch (ParseException e) {
                    System.out.println("Error: " + e.getMessage());
            }
        return convertido;
    }
}
