
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.tipoDocumento;

/**
 * La clase tipoDocumentoDAO recibe un objeto de la clase tipoDocumento
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class tipoDocumentoDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;

/**
* el método GuardarTipoDocumento recibe un objeto de la clase tipoDocumento y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param td tipoDocumento
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarTipoDocumento(tipoDocumento td) {
        int r=0;
        String sql="INSERT INTO tipodocumento(idtipodocumento,descripciontipodocumento) values(?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, td.getIdDocumento());
            ps.setString(2, td.getNombreDocumento());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarTipoDocumento recibe un objeto de la clase tipoDocumenti y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param td tipoDocumento
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarTipoDocumento(tipoDocumento td) {
        int r=0;
        String sql="UPDATE tipodocumento SET idtipodocumento = ?, descripciontipodocumento = ? "
                + "WHERE idtipodocumento = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, td.getIdDocumento());
            ps.setString(2, td.getNombreDocumento());
            ps.setString(3, td.getIdDocumento());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarTipoDocumento recibe un objeto de la clase tipoDocumento y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param td tipoDocumento
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarTipoDocumento(tipoDocumento td) {
        int r=0;
        String sql="DELETE FROM tipodocumento WHERE idtipodocumento = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, td.getIdDocumento());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarTipoDocumentos consulta todos los datos de la tabla tipodocumento de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de tipo tipodocumento
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<tipoDocumento> ConsultarTipoDocumentos() {
        int r=0;
        List<tipoDocumento> result = new ArrayList();
        String sql="SELECT idtipodocumento, descripciontipodocumento FROM tipodocumento";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                tipoDocumento td = new tipoDocumento();
                td.setIdDocumento(rs.getString("idtipodocumento"));
                td.setNombreDocumento(rs.getString("descripciontipodocumento"));
                result.add(td);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
