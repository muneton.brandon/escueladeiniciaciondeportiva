
package persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Docente;

/**
 * La clase DocenteDAO recibe un objeto de la clase Docente
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class DocenteDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método GuardarDocente recibe un objeto de la clase Docente y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d Docente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarDocente(Docente d) {
        int r=0;
        String sql="INSERT INTO docente(tipodocumento_idtipodocumento,	identdocente,nombredocente,"
                + "fechanacdocente,direcciondocente,telefonodocente,correodocente,"
                + "estudiosdocente,genero_idgenero,estado_idestado) values(?,?,?,?,?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, d.getTipoDocumento());
            ps.setInt(2, d.getIdentificacionDocente());
            ps.setString(3, d.getNombreDocente());
            ps.setDate(4, d.getFechaNacimientoDocente());
            ps.setString(5, d.getDireccionDocente());
            ps.setString(6, d.getTelefonoDocente());
            ps.setString(7, d.getCorreoDocente());
            ps.setString(8, d.getEstudiosDocente());
            ps.setString(9, d.getGeneroDocente());
            ps.setString(10, d.getEstadoDocente());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarDocente recibe un objeto de la clase Docente y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d Docente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarDocente(Docente d) {
        String sql="UPDATE docente SET tipodocumento_idtipodocumento = ?, identdocente = ?, nombredocente = ?,"
                + "fechanacdocente = ?, direcciondocente = ?, telefonodocente = ?, correodocente = ?, estudiosdocente = ?,"
                + "genero_idgenero = ?, estado_idestado = ? WHERE identdocente = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, d.getTipoDocumento());
            ps.setInt(2, d.getIdentificacionDocente());
            ps.setString(3, d.getNombreDocente());
            ps.setDate(4, d.getFechaNacimientoDocente());
            ps.setString(5, d.getDireccionDocente());
            ps.setString(6, d.getTelefonoDocente());
            ps.setString(7, d.getCorreoDocente());
            ps.setString(8, d.getEstudiosDocente());
            ps.setString(9, d.getGeneroDocente());
            ps.setString(10, d.getEstadoDocente());
            ps.setInt(11, d.getIdentificacionDocente());
            ps.execute();
            return "Corecto";
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarDocente recibe un objeto de la clase Docente y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param d Docente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarDocente(Docente d) {
        int r=0;
        String sql="DELETE FROM deporte WHERE identdocente = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, d.getIdentificacionDocente());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarDocente consulta todos los datos de la tabla Docente de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de docentes
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Docente> ConsultarDocentes() {
        int r=0;
        List<Docente> result = new ArrayList();
        String sql="SELECT tipodocumento_idtipodocumento, identdocente,nombredocente, fechanacdocente,"
                + "direcciondocente,telefonodocente,correodocente,estudiosdocente,genero_idgenero,estado_idestado"
                + " FROM docente";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Docente d = new Docente();
                d.setTipoDocumento(rs.getString("tipodocumento_idtipodocumento"));
                d.setIdentificacionDocente(rs.getInt("identdocente"));
                d.setNombreDocente(rs.getString("nombredocente"));
                d.setFechaNacimientoDocente(rs.getDate("fechanacdocente"));
                d.setDireccionDocente(rs.getString("direcciondocente"));
                d.setTelefonoDocente(rs.getString("telefonodocente"));
                d.setCorreoDocente(rs.getString("correodocente"));
                d.setEstudiosDocente(rs.getString("estudiosdocente"));
                d.setGeneroDocente(rs.getString("genero_idgenero"));
                d.setEstadoDocente(rs.getString("estado_idestado"));
                result.add(d);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
/**
* el método ConvertirFecha recibe un string con una fecha
* y la convierte en una fecha del tipo java.sql.date
* @param fechaRecibida string
* @return fecha del tipo java.sql.date
* @since abril 2020
* @version 1
* @author ecardona
*/ 
    public Date ConvertirFecha(String fechaRecibida) {
        Date convertido=null;
        try {
            //En este caso buscará en el String dia/mes/año
            DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
            convertido = new java.sql.Date(fecha.parse(fechaRecibida).getTime());
            return convertido;
            } catch (ParseException e) {
                    System.out.println("Error: " + e.getMessage());
            }
        return convertido;
    }
}
