package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.informes;
import utilidades.ConexionBD;

/**
 * Esta clase intermedia entre el servlet de DescargaInformes y la BD
 *
 * @author ecardona
 * @since abril 2020
 * @version 1
 */
public class informesDAO {

    Connection con;
    ConexionBD cn = new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;

    /**
     * Este método genera un query con top 10 de los mejores estudiantes por
     * docente
     *
     * @author ecardona
     * @return Lista con la información del query
     * @since abril 2020
     * @version 1
     */
    public List<informes> ConsultarEstudiantesTop() {
        List<informes> result = new ArrayList();
        String sql = "SELECT	c.nombrecategoria, d.nombredeporte, g.idgrupo, gd.estudiante_identestudiante,  "
                + "e.nombreestudiante, gd.notafinalestud\n"
                + "FROM 	grupo g\n"
                + "JOIN	grupodetalle gd ON g.idgrupo = gd.grupo_idgrupo\n"
                + "JOIN 	estudiante e ON e.identestudiante = gd.estudiante_identestudiante\n"
                + "JOIN	categoria c ON c.idcategoria = g.categoria_idcategoria\n"
                + "JOIN	deporte d ON D.iddeporte = g.deporte_iddeporte \n"
                + "ORDER BY gd.notafinalestud DESC\n"
                + "LIMIT 10";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();
                rg.setNombreCategoria(rs.getString("c.nombrecategoria"));
                rg.setNombreDeporte(rs.getString("d.nombredeporte"));
                rg.setIdGrupo(rs.getInt("g.idgrupo"));
                rg.setIdentificacionEstudiante(rs.getInt("gd.estudiante_identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNotaFinalEstudiante(rs.getFloat("gd.notafinalestud"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }

    /**
     * Este método genera un query con el resumen de los grupos, el cual
     * contiene categorias, deportes, grupos, nombres de docentes, estudiantes,
     * etc.
     *
     * @author ecardona
     * @return Lista con la información del query
     * @since abril 2020
     * @version 1
     */
    public List<informes> ConsultarResumenGrupos() {
        List<informes> result = new ArrayList();
        String sql = "SELECT c.nombrecategoria, d.nombredeporte, g.idgrupo, g.objetivogrupo, doc.nombredocente, "
                + "gd.estudiante_identestudiante, e.nombreestudiante, gd.notafinalestud\n"
                + "FROM grupo g\n"
                + "JOIN categoria c ON c.idcategoria = g.categoria_idcategoria\n"
                + "JOIN deporte d ON d.iddeporte = g.deporte_iddeporte\n"
                + "JOIN grupodetalle gd ON g.idgrupo = gd.grupo_idgrupo\n"
                + "JOIN docente doc ON doc.identdocente = g.docente_id_docente\n"
                + "JOIN estudiante e ON e.identestudiante = gd.estudiante_identestudiante";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();
                rg.setNombreCategoria(rs.getString("c.nombrecategoria"));
                rg.setNombreDeporte(rs.getString("d.nombredeporte"));
                rg.setIdGrupo(rs.getInt("g.idgrupo"));
                rg.setObjetivoGrupo(rs.getString("g.objetivogrupo"));
                rg.setNombDocente(rs.getString("doc.nombredocente"));
                rg.setIdentificacionEstudiante(rs.getInt("gd.estudiante_identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNotaFinalEstudiante(rs.getFloat("gd.notafinalestud"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }

    /**
     * Este método genera un query con el listado de estudiantes y su estado en
     * el paz y salvo (¿Al día?, S=Sí, N=No) docente
     *
     * @author ecardona
     * @return Lista con la información del query
     * @since abril 2020
     * @version 1
     */
    public List<informes> ConsultarPazYSalvoEstudiantes() {
        List<informes> result = new ArrayList();
        String sql = "SELECT e.identestudiante, e.nombreestudiante, e.pazysalvoestudiante\n"
                + "FROM estudiante e";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();
                rg.setIdentificacionEstudiante(rs.getInt("e.identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setPazYSalvo(rs.getString("e.pazysalvoestudiante"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }

    /**
     * Este método genera un query con top 10 de los mejores estudiantes por
     * docente
     *
     * @author ecardona
     * @return Lista con la información del query
     * @since abril 2020
     * @version 1
     */
    public List<informes> ConsultarEstudiante(String idEstudiante) {
        List<informes> result = new ArrayList();
        String sql = "SELECT 	e.identestudiante, e.nombreestudiante, d.nombredeporte, c.nombrecategoria\n"
                + "FROM 	estudiante e\n"
                + "JOIN 	grupodetalle gd ON gd.estudiante_identestudiante = e.identestudiante\n"
                + "JOIN	grupo g ON g.idgrupo = gd.grupo_idgrupo\n"
                + "JOIN	deporte d ON G.deporte_iddeporte = D.iddeporte\n"
                + "JOIN 	categoria c ON c.idcategoria = g.categoria_idcategoria \n"
                + "WHERE 	e.identestudiante = '" + idEstudiante + "'";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();
                rg.setIdentificacionEstudiante(rs.getInt("e.identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNombreDeporte(rs.getString("d.nombredeporte"));
                rg.setNombreCategoria(rs.getString("c.nombrecategoria"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }

    public List<informes> ConsultarGrupoNotas(String idGrupo) {
        List<informes> result = new ArrayList();
        String sql = "SELECT gd.grupo_idgrupo, gd.estudiante_identestudiante, e.nombreestudiante, "
                + "gd.notafinalestud FROM grupodetalle gd JOIN estudiante e "
                + "ON e.identestudiante = gd.estudiante_identestudiante where gd.grupo_idgrupo = " + idGrupo;
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();

                rg.setIdGrupo(rs.getInt("gd.grupo_idgrupo"));
                rg.setIdentificacionEstudiante(rs.getInt("gd.estudiante_identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNotaFinalEstudiante(rs.getFloat("gd.notafinalestud"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }

    public List<informes> ConsultarEstudiantesGanaron(String idGrupo) {
        List<informes> result = new ArrayList();
        String sql = "SELECT e.identestudiante, e.nombreestudiante, gd.notafinalestud "
                + "FROM grupo g "
                + "JOIN grupodetalle gd ON g.idgrupo = gd.grupo_idgrupo "
                + "JOIN estudiante e ON gd.estudiante_identestudiante = e.identestudiante "
                + "WHERE g.idgrupo = "+idGrupo+" AND gd.notafinalestud >= 3";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();

                rg.setIdentificacionEstudiante(rs.getInt("e.identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNotaFinalEstudiante(rs.getFloat("gd.notafinalestud"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }
    
    public List<informes> ConsultarEstudiantesPerdieron(String idGrupo) {
        List<informes> result = new ArrayList();
        String sql = "SELECT e.identestudiante, e.nombreestudiante, gd.notafinalestud "
                + "FROM grupo g "
                + "JOIN grupodetalle gd ON g.idgrupo = gd.grupo_idgrupo "
                + "JOIN estudiante e ON gd.estudiante_identestudiante = e.identestudiante "
                + "WHERE g.idgrupo = "+idGrupo+" AND gd.notafinalestud < 3";
        try {
            con = cn.getConection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                informes rg = new informes();

                rg.setIdentificacionEstudiante(rs.getInt("e.identestudiante"));
                rg.setNombEstudiante(rs.getString("e.nombreestudiante"));
                rg.setNotaFinalEstudiante(rs.getFloat("gd.notafinalestud"));
                result.add(rg);
            }
        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (Exception eo) {

            }
        }
        return result;
    }
}
