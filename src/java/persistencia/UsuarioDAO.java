
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Usuario;
import utilidades.ValidarUsuarioYClave;

/**
 * La clase UsuarioDAO recibe un objeto de la clase Usuario
 * y lo procesa en la BD
 * @since noviembre 2020
 * @version 1
 * @author Manuela Acevedo
 */
public class UsuarioDAO implements ValidarUsuarioYClave{
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método Validar recibe un objeto de la clase usuario y valida si los datos de
* ese objeto se encuentran registrados en la bd y devuelve el tipo de usuario 
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return TipoUsuario
* @since noviembre 2020
 * @version 1
 * @author Manuela Acevedo
*/   
    @Override
    public String validar(Usuario u) {
        int r=0;
        String sql="SELECT CorreoUsuario, Clave, docente_identdocente, TipoUsuario FROM usuario "
                + "WHERE CorreoUsuario=? and Clave=?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, u.getCorreoUsuario());
            ps.setString(2, u.getClave());
            rs=ps.executeQuery();
            while (rs.next()){
                r=r+1;
                u.setCorreoUsuario(rs.getString("CorreoUsuario"));
                u.setClave(rs.getString("Clave"));
                u.setIdentificacionDocente(rs.getString("docente_identdocente"));
                u.setTipoUsuario(rs.getString("TipoUsuario"));
            }
            if(r==1){
                return u.getTipoUsuario();
            }else{
                return "0";
            }
        
        } catch (Exception e){
            return "0";
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "0";
            }
        }
    }
    
/**
* el método GuardarUsuario recibe un objeto de la clase usuario y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since noviembre 2020
 * @version 1
 * @author Manuela Acevedo
*/ 
    public String GuardarUsuario(Usuario u) {
        int r=0;
        String sql="INSERT INTO usuario(CorreoUsuario,Clave,docente_identdocente,TipoUsuario) values(?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, u.getCorreoUsuario());
            ps.setString(2, u.getClave());
            ps.setString(3, u.getIdentificacionDocente());
            ps.setString(4, u.getTipoUsuario());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarUsuario recibe un objeto de la clase usuario y actualoza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since noviembre 2020
* @version 1
* @author Manuela Acevedo
*/ 
    public String ActualizarUsuario(Usuario u) {
        int r=0;
        String sql="UPDATE usuario SET CorreoUsuario = ?, Clave = ?, docente_identdocente = ?, "
                + "TipoUsuario = ? WHERE CorreoUsuario = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, u.getCorreoUsuario());
            ps.setString(2, u.getClave());
            ps.setString(3, u.getIdentificacionDocente());
            ps.setString(4, u.getTipoUsuario());
            ps.setString(5, u.getCorreoUsuario());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    
/**
* el método BorrarUsuario recibe un objeto de la clase usuario y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since noviembre 2020
* @version 1
* @author Manuela Acevedo
*/ 
    public String BorrarUsuario(Usuario u) {
        int r=0;
        String sql="DELETE FROM usuario WHERE CorreoUsuario = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, u.getCorreoUsuario());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarUsuario consulta todos los datos de la tabla usuarios de
* la bd y devuelve una lista con los datos de esa consulta
* para saber qué permisos se le van a mostrar
* @return List Lista de tipo Usuario
* @since noviembre 2020
* @version 1
* @author Manuela Acevedo
*/ 
    public List<Usuario> ConsultarUsuarios() {
        int r=0;
        List<Usuario> result = new ArrayList();
        String sql="SELECT CorreoUsuario, Clave, docente_identdocente, TipoUsuario FROM usuario";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Usuario u = new Usuario();
                u.setCorreoUsuario(rs.getString("CorreoUsuario"));
                u.setClave(rs.getString("Clave"));
                u.setIdentificacionDocente(rs.getString("docente_identdocente"));
                u.setTipoUsuario(rs.getString("TipoUsuario"));
                result.add(u);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
/**
* el método RecuperarClave consulta recibe el correo del usuario
* que desea recuperar la clave, valida que se encuentre registrado y
* le envía la clave al correo registrado en la bd.
* @return Clave
* @since noviembre 2020
* @version 1
* @author Manuela Acevedo
*/ 
    public String RecuperarClave(Usuario u) {
        String sql="SELECT Clave FROM usuario WHERE CorreoUsuario=?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, u.getCorreoUsuario());
            rs=ps.executeQuery();
            while (rs.next()){
                u.setClave(rs.getString("Clave"));
            }
            return u.getClave();
        } catch (Exception e){
            return "Error: " + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error: " + eo;
            }
        }
    
    }
}