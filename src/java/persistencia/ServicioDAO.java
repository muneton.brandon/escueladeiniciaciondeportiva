/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Servicio;
import utilidades.ConexionBD;

/**
 *
 * @author USUARIO
 */
public class ServicioDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
    public String GuardarServicio(Servicio s) {
        int r=0;
        String sql="INSERT INTO servicio(Numero_servicio, servicio, valor_servicio) VALUES (?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, s.getIdServicio());
            ps.setString(2, s.getNombreServicio());
            ps.setInt(3, s.getValor());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    

    public String ActualizarServicio(Servicio s) {
        int r=0;
        String sql="UPDATE servicio SET Numero_servicio= ?, servicio= ?, valor_servicio= ? WHERE Numero_servicio= ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, s.getIdServicio());
            ps.setString(2, s.getNombreServicio());
            ps.setInt(3, s.getValor());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarCategoria recibe un objeto de la clase Categoria y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param c Categoria
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarServicio(Servicio s) {
        int r=0;
        String sql="DELETE FROM servicio WHERE Numero_servicio= ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, s.getIdServicio());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarCategoria consulta todos los datos de la tabla Categoria de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de categorias
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Servicio> ConsultarServicios() {
        int r=0;
        List<Servicio> result = new ArrayList();
        String sql="SELECT Numero_servicio, servicio, valor_servicio FROM servicio";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Servicio s = new Servicio();
                s.setIdServicio(rs.getInt("Numero_servicio"));
                s.setNombreServicio(rs.getString("servicio"));
                s.setValor(rs.getInt("valor_servicio"));
                result.add(s);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    public List<Servicio> ConsultarServicio(int idServicio) {
        int r=0;
        List<Servicio> result = new ArrayList();
        String sql="SELECT Numero_servicio, servicio, valor_servicio FROM servicio "
                + "WHERE Numero_servicio = " + idServicio;
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Servicio s = new Servicio();
                s.setIdServicio(rs.getInt("Numero_servicio"));
                s.setNombreServicio(rs.getString("servicio"));
                s.setValor(rs.getInt("valor_servicio"));
                result.add(s);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
