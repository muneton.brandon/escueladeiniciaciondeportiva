
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Categoria;
import utilidades.ConexionBD;

/**
 * La clase CategoriaDAO recibe un objeto de la clase Categoria
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class CategoriaDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
    /**
* el método GuardarCategoria recibe un objeto de la clase Categoria y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param c categoria
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarCategoria(Categoria c) {
        int r=0;
        String sql="INSERT INTO categoria(idcategoria,nombrecategoria) values(?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, c.getIdCategoria());
            ps.setString(2, c.getDescripcionCategoria());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarCategoria recibe un objeto de la clase Categoria y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param c Categoria
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarCategoria(Categoria c) {
        int r=0;
        String sql="UPDATE categoria SET idcategoria = ?, nombrecategoria = ? WHERE idcategoria = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, c.getIdCategoria());
            ps.setString(2, c.getDescripcionCategoria());
            ps.setInt(3, c.getIdCategoria());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarCategoria recibe un objeto de la clase Categoria y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param c Categoria
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarCategoria(Categoria c) {
        int r=0;
        String sql="DELETE FROM categoria WHERE idcategoria = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, c.getIdCategoria());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarCategoria consulta todos los datos de la tabla Categoria de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de categorias
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Categoria> ConsultarCategorias() {
        int r=0;
        List<Categoria> result = new ArrayList();
        String sql="SELECT idcategoria, nombrecategoria FROM categoria";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Categoria c = new Categoria();
                c.setIdCategoria(rs.getInt("idcategoria"));
                c.setDescripcionCategoria(rs.getString("nombrecategoria"));
                result.add(c);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
