/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Factura;
import utilidades.ValidarUsuarioYClave;

public class FacturaDAO{
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;

    
/**
* el método GuardarUsuario recibe un objeto de la clase usuario y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String GuardarFactura(Factura f) {
        int r=0;
        String sql="INSERT INTO factura(fecha_exp, fecha_venc, tipo_documento, "
                + "numero_documento, nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva,"
                + " porc_desc, descuento, valortotal, estado, observaciones) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            //ps.setInt(1, f.getNroFactura());
            ps.setString(1, f.getFechaExp());
            ps.setString(2, f.getFechaVenc());
            ps.setString(3, f.getTipoDocumento());
            ps.setInt(4, f.getIdentificacion());
            ps.setString(5, f.getAcudiente());
            ps.setString(6, f.getAlumno());
            ps.setString(7, f.getDescripcion());
            ps.setDouble(8, f.getSubtotal());
            ps.setDouble(9, f.getPorcIva());
            ps.setDouble(10, f.getIva());
            ps.setDouble(11, f.getPorcDescuento());
            ps.setDouble(12, f.getDescuento());
            ps.setDouble(13, f.getValorTotal());
            ps.setString(14, f.getEstado());
            ps.setString(15, f.getObservaciones());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarUsuario recibe un objeto de la clase usuario y actualoza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarFactura(Factura f) {
        int r=0;
        String sql="UPDATE factura SET fecha_exp = ?, fecha_venc = ?, tipo_documento =  ?, "
                + "numero_documento = ?, nombre_persona = ?, Alumno = ?, descripcion = ?, subtotal = ?, "
                + "porc_iva = ?, iva = ?, porc_desc = ?, descuento = ?, valortotal = ?, estado = ?, observaciones = ? "
                + "WHERE numero_factura = ?" ;
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, f.getFechaExp());
            ps.setString(2, f.getFechaVenc());
            ps.setString(3, f.getTipoDocumento());
            ps.setInt(4, f.getIdentificacion());
            ps.setString(5, f.getAcudiente());
            ps.setString(6, f.getAlumno());
            ps.setString(7, f.getDescripcion());
            ps.setDouble(8, f.getSubtotal());
            ps.setDouble(9, f.getPorcIva());
            ps.setDouble(10, f.getIva());
            ps.setDouble(11, f.getPorcDescuento());
            ps.setDouble(12, f.getDescuento());
            ps.setDouble(13, f.getValorTotal());
            ps.setString(14, f.getEstado());
            ps.setString(15, f.getObservaciones());
            ps.setInt(16, f.getNroFactura());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarUsuario consulta todos los datos de la tabla usuarios de
* la bd y devuelve una lista con los datos de esa consulta
* para saber qué permisos se le van a mostrar
* @return List Lista de tipo Usuario
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Factura> ConsultarFacturas() {
        int r=0;
        List<Factura> result = new ArrayList();
        String sql="SELECT numero_factura, fecha_exp, fecha_venc, tipo_documento, numero_documento, "
                + "nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva, porc_desc, descuento, "
                + "valortotal, estado, observaciones FROM factura";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Factura f = new Factura();
                f.setNroFactura(rs.getInt("numero_factura"));
                f.setFechaExp(rs.getString("fecha_exp"));
                f.setFechaVenc(rs.getString("fecha_venc"));
                f.setTipoDocumento(rs.getString("tipo_documento"));
                f.setIdentificacion(rs.getInt("numero_documento"));
                f.setAcudiente(rs.getString("nombre_persona"));
                f.setAlumno(rs.getString("Alumno"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setSubtotal(rs.getInt("subtotal"));
                f.setPorcIva(rs.getFloat("porc_iva"));
                f.setIva(rs.getInt("iva"));
                f.setPorcDescuento(rs.getFloat("porc_desc"));
                f.setDescuento(rs.getInt("descuento"));
                f.setValorTotal(rs.getInt("valortotal"));
                f.setEstado(rs.getString("estado"));
                f.setObservaciones(rs.getString("observaciones"));
                result.add(f);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    
    public String AnularFactura(Factura f) {
        int r=0;
        String sql="UPDATE factura SET estado = ?, observaciones = ? WHERE numero_factura = ?";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, f.getEstado());
            ps.setString(2, f.getObservaciones());
            ps.setInt(3, f.getNroFactura());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarFactura consulta una factura en especifico
* y devuelve una lista con los datos de esa consulta
* @return List Lista de tipo Usuario
* @since junio 2020
* @version 1
* @author ecardona
*/ 
    public List<Factura> ConsultarFactura(int nroFactura) {
        int r=0;
        List<Factura> result = new ArrayList();
        String sql="SELECT numero_factura, fecha_exp, fecha_venc, tipo_documento, numero_documento, "
                + "nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva, porc_desc, descuento, "
                + "valortotal, estado, observaciones FROM factura WHERE numero_factura =" + nroFactura;
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Factura f = new Factura();
                f.setNroFactura(rs.getInt("numero_factura"));
                f.setFechaExp(rs.getString("fecha_exp"));
                f.setFechaVenc(rs.getString("fecha_venc"));
                f.setTipoDocumento(rs.getString("tipo_documento"));
                f.setIdentificacion(rs.getInt("numero_documento"));
                f.setAcudiente(rs.getString("nombre_persona"));
                f.setAlumno(rs.getString("Alumno"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setSubtotal(rs.getInt("subtotal"));
                f.setPorcIva(rs.getFloat("porc_iva"));
                f.setIva(rs.getInt("iva"));
                f.setPorcDescuento(rs.getFloat("porc_desc"));
                f.setDescuento(rs.getInt("descuento"));
                f.setValorTotal(rs.getInt("valortotal"));
                f.setEstado(rs.getString("estado"));
                f.setObservaciones(rs.getString("observaciones"));
                result.add(f);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
