/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Prefactura;
import utilidades.ConexionBD;

/**
 *
 * @author USUARIO
 */
public class PrefacturaDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;

    
/**
* el método GuardarUsuario recibe un objeto de la clase usuario y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String GuardarPreactura(Prefactura pf) {
        int r=0;
        String sql="INSERT INTO prefactura(nro_prefactura, fecha_exp, fecha_venc, tipo_documento, "
                + "numero_documento, nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva,"
                + " porc_desc, descuento, valortotal, estado, observaciones) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, pf.getNroPrefactura());
            ps.setString(2, pf.getFechaExp());
            ps.setString(3, pf.getFechaVenc());
            ps.setString(4, pf.getTipoDocumento());
            ps.setInt(5, pf.getIdentificacion());
            ps.setString(6, pf.getAcudiente());
            ps.setString(7, pf.getAlumno());
            ps.setString(8, pf.getDescripcion());
            ps.setDouble(9, pf.getSubtotal());
            ps.setDouble(10, pf.getPorcIva());
            ps.setDouble(11, pf.getIva());
            ps.setDouble(12, pf.getPorcDescuento());
            ps.setDouble(13, pf.getDescuento());
            ps.setDouble(14, pf.getValorTotal());
            ps.setString(15, pf.getEstado());
            ps.setString(16, pf.getObservaciones());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarUsuario recibe un objeto de la clase usuario y actualoza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param U usuario
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarPrefactura(Prefactura pf) {
        int r=0;
        String sql="UPDATE prefactura SET nro_prefactura ="+pf.getNroPrefactura()+", fecha_exp ="+pf.getFechaExp()+", fecha_venc ="+pf.getFechaVenc()+", tipo_documento ="+pf.getTipoDocumento()+","
                + " numero_documento ="+pf.getIdentificacion()+", nombre_persona ="+pf.getAcudiente()+", Alumno ="+pf.getAlumno()+", descripcion ="+pf.getDescripcion()+", subtotal = "+pf.getSubtotal()+","
               + " porc_iva = "+pf.getPorcIva()+", iva = "+pf.getIva()+", porc_desc = "+pf.getPorcDescuento()+", descuento ="+pf.getDescuento()+", valortotal = "+pf.getValorTotal()+", estado = "+pf.getEstado()+","
                + " observaciones = "+pf.getObservaciones()+" WHERE numero_factura ="+pf.getNroPrefactura();
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, pf.getNroPrefactura());
            ps.setString(2, pf.getFechaExp());
            ps.setString(3, pf.getFechaVenc());
            ps.setString(4, pf.getTipoDocumento());
            ps.setInt(5, pf.getIdentificacion());
            ps.setString(6, pf.getAcudiente());
            ps.setString(7, pf.getAlumno());
            ps.setString(8, pf.getDescripcion());
            ps.setDouble(9, pf.getSubtotal());
            ps.setDouble(10, pf.getPorcIva());
            ps.setDouble(11, pf.getIva());
            ps.setDouble(12, pf.getPorcDescuento());
            ps.setDouble(13, pf.getDescuento());
            ps.setDouble(14, pf.getValorTotal());
            ps.setString(15, pf.getEstado());
            ps.setString(16, pf.getObservaciones());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
        
    
/**
* el método ConsultarUsuario consulta todos los datos de la tabla usuarios de
* la bd y devuelve una lista con los datos de esa consulta
* para saber qué permisos se le van a mostrar
* @return List Lista de tipo Usuario
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Prefactura> ConsultarPrefacturas() {
        int r=0;
        List<Prefactura> result = new ArrayList();
        String sql="SELECT nro_prefactura, fecha_exp, fecha_venc, tipo_documento, numero_documento, "
                + "nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva, porc_desc, descuento, "
                + "valortotal, estado, observaciones FROM prefactura";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Prefactura pf = new Prefactura();
                pf.setNroPrefactura(rs.getInt("nro_prefactura"));
                pf.setFechaExp(rs.getString("fecha_exp"));
                pf.setFechaVenc(rs.getString("fecha_venc"));
                pf.setTipoDocumento(rs.getString("tipo_documento"));
                pf.setIdentificacion(rs.getInt("numero_documento"));
                pf.setAcudiente(rs.getString("nombre_persona"));
                pf.setAlumno(rs.getString("Alumno"));
                pf.setDescripcion(rs.getString("descripcion"));
                pf.setSubtotal(rs.getInt("subtotal"));
                pf.setPorcIva(rs.getFloat("porc_iva"));
                pf.setIva(rs.getInt("iva"));
                pf.setPorcDescuento(rs.getFloat("porc_desc"));
                pf.setDescuento(rs.getInt("descuento"));
                pf.setValorTotal(rs.getInt("valortotal"));
                pf.setEstado(rs.getString("estado"));
                pf.setObservaciones(rs.getString("observaciones"));
                result.add(pf);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    //Procedimeinto de consulta de la factura individual solicitada
    public List<Prefactura> ConsultarPreFactura(int idPrefactura) {
        int r=0;
        List<Prefactura> result = new ArrayList();
        String sql="SELECT nro_prefactura, fecha_exp, fecha_venc, tipo_documento, numero_documento, "
                + "nombre_persona, Alumno, descripcion, subtotal, porc_iva, iva, porc_desc, descuento, "
                + "valortotal, estado, observaciones FROM prefactura where nro_prefactura = " + idPrefactura;
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Prefactura f = new Prefactura();
                f.setNroPrefactura(rs.getInt("nro_prefactura"));
                f.setFechaExp(rs.getString("fecha_exp"));
                f.setFechaVenc(rs.getString("fecha_venc"));
                f.setTipoDocumento(rs.getString("tipo_documento"));
                f.setIdentificacion(rs.getInt("numero_documento"));
                f.setAcudiente(rs.getString("nombre_persona"));
                f.setAlumno(rs.getString("Alumno"));
                f.setDescripcion(rs.getString("descripcion"));
                f.setSubtotal(rs.getInt("subtotal"));
                f.setPorcIva(rs.getFloat("porc_iva"));
                f.setIva(rs.getInt("iva"));
                f.setPorcDescuento(rs.getFloat("porc_desc"));
                f.setDescuento(rs.getInt("descuento"));
                f.setValorTotal(rs.getInt("valortotal"));
                f.setEstado(rs.getString("estado"));
                f.setObservaciones(rs.getString("observaciones"));
                result.add(f);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    public String AprobarPreFactura(Prefactura pf) {
        int r=0;
        String sql="UPDATE prefactura SET estado = ? WHERE nro_prefactura = ?";
        try{
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, pf.getEstado());
            ps.setInt(2, pf.getNroPrefactura());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
}

