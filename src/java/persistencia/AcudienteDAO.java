
package persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import modelo.Acudiente;
import utilidades.ConexionBD;

/**
 * La clase AcudienteDAO recibe un objeto de la clase Acudiente
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class AcudienteDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;
    
/**
* el método GuardarAcudiente recibe un objeto de la clase Acudiente y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param a Acudiente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarAcudiente(Acudiente a) {
        int r=0;
        String sql="INSERT INTO acudiente(tipodocumento_idtipodocumento, identacudiente, nombreacudiente,"
                + "fechanacacudiente, direccionacudiente, telefonoacudiente, genero_idgenero, vinculoinstucionacudiente) "
                + "values(?,?,?,?,?,?,?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, a.getTipoDocumentoAcudiente());
            ps.setInt(2, a.getIdentificacionAcudiente());
            ps.setString(3, a.getNombreAcudiente());
            ps.setDate(4, a.getFechaNacimientoAcudiente());
            ps.setString(5, a.getDireccionAcudiente());
            ps.setString(6, a.getTelefonoAcudiente());
            ps.setString(7, a.getGeneroAcudiente());
            ps.setString(8, a.getVinculoInstitucionAcudiente());
            ps.execute();
            r=r+1;
                return "Corecto";        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarAcudiente recibe un objeto de la clase Acudiente y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param a Acudiente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarAcudiente(Acudiente a) {
        int r=0;
        String sql="UPDATE acudiente SET tipodocumento_idtipodocumento = ?, identacudiente = ?, nombreacudiente = ?,"
                + "fechanacacudiente = ?, direccionacudiente = ?, telefonoacudiente = ?, genero_idgenero = ?,"
                + "vinculoinstucionacudiente = ? WHERE identacudiente = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, a.getTipoDocumentoAcudiente());
            ps.setInt(2, a.getIdentificacionAcudiente());
            ps.setString(3, a.getNombreAcudiente());
            ps.setDate(4, a.getFechaNacimientoAcudiente());
            ps.setString(5, a.getDireccionAcudiente());
            ps.setString(6, a.getTelefonoAcudiente());
            ps.setString(7, a.getGeneroAcudiente());
            ps.setString(8, a.getVinculoInstitucionAcudiente());
            ps.setInt(9, a.getIdentificacionAcudiente());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarAcudiente recibe un objeto de la clase Acudiente y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param a Acudiente
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarAcudiente(Acudiente a) {
        int r=0;
        String sql="DELETE FROM acudiente WHERE identacudiente = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setInt(1, a.getIdentificacionAcudiente());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ConsultarAcudiente consulta todos los datos de la tabla Acudiente de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de acudientes
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Acudiente> ConsultarAcudientes() {
        int r=0;
        List<Acudiente> result = new ArrayList();
        String sql="SELECT tipodocumento_idtipodocumento, identacudiente, nombreacudiente, fechanacacudiente,"
                + "direccionacudiente, telefonoacudiente, genero_idgenero, vinculoinstucionacudiente FROM acudiente";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Acudiente a = new Acudiente();
                a.setTipoDocumentoAcudiente(rs.getString("tipodocumento_idtipodocumento"));
                a.setIdentificacionAcudiente(rs.getInt("identacudiente"));
                a.setNombreAcudiente(rs.getString("nombreacudiente"));
                a.setFechaNacimientoAcudiente(rs.getDate("fechanacacudiente"));
                a.setDireccionAcudiente(rs.getString("direccionacudiente"));
                a.setTelefonoAcudiente(rs.getString("telefonoacudiente"));
                a.setGeneroAcudiente(rs.getString("genero_idgenero"));
                a.setVinculoInstitucionAcudiente(rs.getString("vinculoinstucionacudiente"));
                result.add(a);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
    
    /**
* el método ConvertirFecha recibe un string con una fecha
* y la convierte en una fecha del tipo java.sql.date
* @param fechaRecibida string
* @return fecha del tipo java.sql.date
* @since abril 2020
* @version 1
* @author ecardona
*/ 
    public Date ConvertirFecha(String fechaRecibida) {
        Date convertido=null;
        try {
            //En este caso buscará en el String dia/mes/año
            DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
            convertido = new java.sql.Date(fecha.parse(fechaRecibida).getTime());
            return convertido;
            } catch (ParseException e) {
                    System.out.println("Error: " + e.getMessage());
            }
        return convertido;
    }
}
