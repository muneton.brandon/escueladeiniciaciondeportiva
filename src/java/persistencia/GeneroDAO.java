
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utilidades.ConexionBD;
import modelo.Genero;

/**
 * La clase GeneroDAO recibe un objeto de la clase Genero
 * y lo procesa en la BD
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class GeneroDAO {
    Connection con;
    ConexionBD cn=new ConexionBD();
    PreparedStatement ps;
    ResultSet rs;

/**
* el método GuardarGenero recibe un objeto de la clase genero y guarda los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param g genero
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/     
    public String GuardarGenero(Genero g) {
        int r=0;
        String sql="INSERT INTO genero(idgenero,descripciongenero) values(?,?)";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, g.getIdGenero());
            ps.setString(2, g.getDescripcionGenero());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método ActualizarGenero recibe un objeto de la clase Genero y actualiza los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* @param g Genero
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String ActualizarGenero(Genero g) {
        int r=0;
        String sql="UPDATE genero SET idgenero = ?, descripciongenero = ? WHERE idgenero = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, g.getIdGenero());
            ps.setString(2, g.getDescripcionGenero());
            ps.setString(3, g.getIdGenero());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
/**
* el método BorrarGenero recibe un objeto de la clase Genero y borra los datos de
* ese objeto en la bd y devuelve el resultado de esa operación
* para saber qué permisos se le van a mostrar
* @param g Genero
* @return String Resultado de la operación
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public String BorrarGenero(Genero g) {
        int r=0;
        String sql="DELETE FROM genero WHERE idgenero = ?";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            ps.setString(1, g.getIdGenero());
            ps.execute();
            r=r+1;
            if(r==1){
                return "Corecto";
            }else{
                return "Error";
            }
        
        } catch (Exception e){
            return "Error en BD" + e;
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                return "Error cerrando la conexión" + eo;
            }
        }
    }
    
    /**
* el método ConsultarGeneros consulta todos los datos de la tabla Genero de
* la bd y devuelve una lista con los datos de esa consulta
* @return List Lista de generos
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
    public List<Genero> ConsultarGeneros() {
        int r=0;
        List<Genero> result = new ArrayList();
        String sql="SELECT idgenero, descripciongenero FROM genero";
        try {
            con=cn.getConection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()){
                Genero g = new Genero();
                g.setIdGenero(rs.getString("idgenero"));
                g.setDescripcionGenero(rs.getString("descripciongenero"));
                result.add(g);
            }
        } catch (Exception e){
        }finally{
            try{
                con.close();
            }catch(Exception eo){
                
            }
        }
        return result;
    }
}
