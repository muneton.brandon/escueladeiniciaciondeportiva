-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-06-2020 a las 02:47:01
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectocsweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acudiente`
--

CREATE TABLE `acudiente` (
  `tipodocumento_idtipodocumento` varchar(2) NOT NULL,
  `identacudiente` int(11) NOT NULL,
  `nombreacudiente` varchar(50) NOT NULL,
  `fechanacacudiente` date NOT NULL,
  `direccionacudiente` varchar(50) DEFAULT NULL,
  `telefonoacudiente` varchar(20) NOT NULL,
  `genero_idgenero` varchar(1) NOT NULL,
  `vinculoinstucionacudiente` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acudiente`
--

INSERT INTO `acudiente` (`tipodocumento_idtipodocumento`, `identacudiente`, `nombreacudiente`, `fechanacacudiente`, `direccionacudiente`, `telefonoacudiente`, `genero_idgenero`, `vinculoinstucionacudiente`) VALUES
('CC', 34526, 'fgjhfwiuh fjd', '2020-06-01', 'gfjdfhf56', '35435456', 'm', 'hermano'),
('CC', 159875, 'Emel rodriguez', '1998-10-08', 'calle del diablo', '84498484', 'm', 'Docente'),
('CC', 254685, 'Emilio Garibaldi', '1996-10-08', 'cale muritonimoquilito', '4651', 'm', 'Estudiante'),
('CC', 567890, 'Daniel', '0000-00-00', 'sdftert45', '346467564', 'm', 'padre'),
('CC', 1545244, 'Josefiuna Marinitiamilo', '1954-09-10', 'calle del profe', '4851', 'f', 'Docente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `nombrecategoria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `nombrecategoria`) VALUES
(1, 'sub 6'),
(2, 'sub 7'),
(3, 'sub 8'),
(4, 'sub 9'),
(5, 'sub 10'),
(6, 'sub 11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `controlasistencia`
--

CREATE TABLE `controlasistencia` (
  `grupo_idgrupo` int(11) NOT NULL,
  `estudiante_identestudiante` int(11) NOT NULL,
  `fechaclasecontrol` date NOT NULL,
  `objetivoclasecontrol` varchar(100) NOT NULL,
  `notaclasecontrol` float NOT NULL,
  `observclasecontrol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deporte`
--

CREATE TABLE `deporte` (
  `iddeporte` int(11) NOT NULL,
  `nombredeporte` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `deporte`
--

INSERT INTO `deporte` (`iddeporte`, `nombredeporte`) VALUES
(1, 'futbol'),
(2, 'BascketBall'),
(3, 'VoleyBall'),
(4, 'BaseBall');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `tipodocumento_idtipodocumento` varchar(2) NOT NULL,
  `identdocente` int(11) NOT NULL,
  `nombredocente` varchar(50) NOT NULL,
  `fechanacdocente` date NOT NULL,
  `direcciondocente` varchar(50) DEFAULT NULL,
  `telefonodocente` varchar(50) NOT NULL,
  `correodocente` varchar(25) NOT NULL,
  `estudiosdocente` varchar(50) NOT NULL,
  `genero_idgenero` varchar(1) NOT NULL,
  `estado_idestado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`tipodocumento_idtipodocumento`, `identdocente`, `nombredocente`, `fechanacdocente`, `direcciondocente`, `telefonodocente`, `correodocente`, `estudiosdocente`, `genero_idgenero`, `estado_idestado`) VALUES
('CC', 1, 'Rigonel Rodriguez', '1972-01-09', 'carrera 52 numero 6 - 2', '58745698', 'rrodriguez@elpoli.edu.co', 'universidad del deporte que practican los deportis', 'm', 1),
('CC', 12345, 'geteger', '2020-05-14', 'hrjrtggg', '45654', 'a@a.com', 'dfgfhgfgdfgd', 'm', 1),
('CC', 67995, 'Daniell', '2020-05-11', 'dfgg46', '463547', 'd@d.com', 'dfmjghdfiu dshd dfg', 'm', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idestado` int(11) NOT NULL,
  `descripcionestado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idestado`, `descripcionestado`) VALUES
(1, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `tipodocumento_idtipodocumento` varchar(2) NOT NULL,
  `identestudiante` int(11) NOT NULL,
  `nombreestudiante` varchar(50) NOT NULL,
  `fechanacimientoestudiante` date NOT NULL,
  `direccionestudiante` varchar(50) DEFAULT NULL,
  `telefonoestudiante` varchar(50) NOT NULL,
  `genero_idgenero` varchar(1) NOT NULL,
  `estado_idestado` int(11) NOT NULL,
  `acudiente_identacudiente` int(11) NOT NULL,
  `pazysalvoestudiante` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`tipodocumento_idtipodocumento`, `identestudiante`, `nombreestudiante`, `fechanacimientoestudiante`, `direccionestudiante`, `telefonoestudiante`, `genero_idgenero`, `estado_idestado`, `acudiente_identacudiente`, `pazysalvoestudiante`) VALUES
('CC', 1, 'Brandon', '1994-09-18', 'calle de brandon', '845455', 'm', 1, 34526, 's'),
('CC', 2, 'Milena Peinada', '2002-06-04', 'call 90 numero 90 90', '54541', 'f', 1, 567890, 'n'),
('CC', 673, 'Felipe Farcia', '2010-06-28', 'call 90 numero 90 56', '75', 'm', 1, 254685, 's'),
('CC', 741, 'Edison Cardona', '2020-06-03', 'calle 71 - 71', '49641', 'm', 1, 567890, 's'),
('CC', 5453, 'Daniel Monsalve', '2020-06-18', 'call 90 numero 90 565656', '51961', 'm', 1, 254685, 's'),
('CC', 12312, 'anderson', '2020-06-17', 'call 90 numero 90 90', '3424', 'm', 1, 34526, 's'),
('CC', 222222244, 'anderson', '2020-06-17', 'call 90 numero 90 90', '3424', 'm', 1, 34526, 's');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `numero_factura` int(6) NOT NULL,
  `fecha_exp` date NOT NULL,
  `fecha_venc` date NOT NULL,
  `tipo_documento` varchar(2) NOT NULL,
  `numero_documento` int(10) NOT NULL,
  `nombre_persona` varchar(100) NOT NULL COMMENT 'Es el nombre del acudiente',
  `Alumno` varchar(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `subtotal` int(10) NOT NULL,
  `porc_iva` float NOT NULL,
  `iva` int(10) NOT NULL,
  `porc_desc` float NOT NULL,
  `descuento` int(10) NOT NULL,
  `valortotal` int(10) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `observaciones` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`numero_factura`, `fecha_exp`, `fecha_venc`, `tipo_documento`, `numero_documento`, `nombre_persona`, `Alumno`, `descripcion`, `subtotal`, `porc_iva`, `iva`, `porc_desc`, `descuento`, `valortotal`, `estado`, `observaciones`) VALUES
(1, '2020-06-02', '2020-06-04', 'CC', 24532, 'emel rodriguez', '12312', 'casa del retiro', 22222, 22, 22, 22, 222, 222222, 'proceso', 'todo'),
(2, '2020-06-09', '2020-06-26', 'TI', 123344, 'awaqwad', '222222244', 'qwerty', 234234, 2342, 4242, 234234, 4234, 42, 'Pagar', 'redfsedf'),
(3, '2020-06-09', '2020-06-26', 'TI', 123344, 'awaqwad', '222222244', 'qwerty', 234234, 2342, 4242, 234234, 4234, 42, 'Pagar', 'redfsedf'),
(4, '2020-06-18', '2020-06-16', 'CC', 12312, 'ssssssss', '12312', 'qwergsdfgs', 12, 12, 12, 12, 12, 12, 'proceso', '12'),
(5, '2020-06-10', '2020-06-11', 'CC', 741852963, 'Alberto Camacho', '12312', 'cobro de balones', 50000, 19, 9500, 15, 7500, 52500, 'Pagado', 'se hace todo organizado'),
(6, '2020-06-08', '2020-06-12', 'CC', 457899656, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0.1, 21000, 228900, 'Anulado', '                                Pro que se rechazo el pago del banco'),
(7, '2020-06-09', '2020-06-11', 'CC', 145625251, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0.1, 21000, 228900, 'Anulado', '                                Sin fondos'),
(8, '2020-06-08', '2020-06-11', 'CC', 9876543, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0.1, 21000, 228900, 'Sin Pagar', '                                    ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idgenero` varchar(1) NOT NULL,
  `descripciongenero` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idgenero`, `descripciongenero`) VALUES
('f', 'Femenina'),
('m', 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL,
  `deporte_iddeporte` int(11) NOT NULL,
  `categoria_idcategoria` int(11) NOT NULL,
  `horarioiniciogrupo` date NOT NULL,
  `horariofingrupo` date NOT NULL,
  `objetivogrupo` varchar(100) NOT NULL,
  `docente_id_docente` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`idgrupo`, `deporte_iddeporte`, `categoria_idcategoria`, `horarioiniciogrupo`, `horariofingrupo`, `objetivogrupo`, `docente_id_docente`) VALUES
(1, 1, 3, '2020-06-22', '2020-06-30', 'pasarla bueno', 12345),
(2, 1, 4, '2020-06-22', '2020-06-30', 'pasarla bueno', 67995),
(3, 4, 5, '2020-06-17', '2020-07-16', 'bascketball', 67995),
(4, 3, 6, '2020-06-01', '2020-06-30', 'VolleyBall', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupodetalle`
--

CREATE TABLE `grupodetalle` (
  `grupo_idgrupo` int(11) NOT NULL,
  `estudiante_identestudiante` int(11) NOT NULL,
  `notafinalestud` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupodetalle`
--

INSERT INTO `grupodetalle` (`grupo_idgrupo`, `estudiante_identestudiante`, `notafinalestud`) VALUES
(1, 12312, 4.2),
(2, 222222244, 3),
(4, 12312, 4.2),
(4, 673, 4.2),
(4, 741, 3.8),
(4, 1, 2.2),
(4, 2, 2.9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prefactura`
--

CREATE TABLE `prefactura` (
  `nro_prefactura` int(6) NOT NULL,
  `fecha_exp` date NOT NULL,
  `fecha_venc` date NOT NULL,
  `tipo_documento` varchar(3) NOT NULL,
  `numero_documento` int(10) NOT NULL,
  `nombre_persona` varchar(100) NOT NULL,
  `Alumno` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `subtotal` int(10) NOT NULL,
  `porc_iva` float NOT NULL,
  `iva` int(10) NOT NULL,
  `porc_desc` float NOT NULL,
  `descuento` int(10) NOT NULL,
  `valortotal` int(10) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `observaciones` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prefactura`
--

INSERT INTO `prefactura` (`nro_prefactura`, `fecha_exp`, `fecha_venc`, `tipo_documento`, `numero_documento`, `nombre_persona`, `Alumno`, `descripcion`, `subtotal`, `porc_iva`, `iva`, `porc_desc`, `descuento`, `valortotal`, `estado`, `observaciones`) VALUES
(1, '2020-06-02', '2020-06-10', 'CC', 142536, 'Anderson Camacho', 'Julian Emilio', 'Compra de utencilios', 50000, 19, 9500, 20, 10000, 45000, 'Sin Pagar', 'Nnguna observacion necesaria'),
(2, '2020-06-02', '2020-06-10', 'CC', 85245684, 'Edison', 'Emilio Emilio', 'Compra de de material por daños', 100000, 19, 19000, 20, 20000, 99000, 'Sin Pagar', 'Nnguna observacion necesaria'),
(3, '2020-06-08', '2020-06-12', 'CC', 457899656, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0, 0, 249900, 'Aprobado', '                                    Debe hacer el pago lo antes posible'),
(4, '2020-06-09', '2020-06-11', 'CC', 145625251, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0.1, 21000, 228900, 'Aprobado', '                                    '),
(5, '2020-06-08', '2020-06-11', 'CC', 9876543, 'fgjhfwiuh fjd', 'anderson', '43567', 210000, 0.19, 39900, 0, 0, 249900, 'Aprobado', '                                    ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `Numero_servicio` int(10) NOT NULL,
  `servicio` varchar(100) NOT NULL,
  `valor_servicio` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`Numero_servicio`, `servicio`, `valor_servicio`) VALUES
(43567, 'Matricula ', 210000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

CREATE TABLE `tipodocumento` (
  `idtipodocumento` varchar(2) NOT NULL,
  `descripciontipodocumento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodocumento`
--

INSERT INTO `tipodocumento` (`idtipodocumento`, `descripciontipodocumento`) VALUES
('CC', 'Cedula ciudadania');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `correousuario` varchar(25) NOT NULL,
  `docente_identdocente` int(11) NOT NULL,
  `clave` varchar(25) NOT NULL,
  `tipousuario` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`correousuario`, `docente_identdocente`, `clave`, `tipousuario`) VALUES
('a@a.com', 12345, '1234', 'Admin'),
('d@d.com', 67995, '012345', 'Docente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acudiente`
--
ALTER TABLE `acudiente`
  ADD PRIMARY KEY (`identacudiente`),
  ADD KEY `acudiente_genero_fk` (`genero_idgenero`),
  ADD KEY `acudiente_tipodocumento_fk` (`tipodocumento_idtipodocumento`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `controlasistencia`
--
ALTER TABLE `controlasistencia`
  ADD PRIMARY KEY (`grupo_idgrupo`),
  ADD KEY `controlasistencia_estudiante_fk` (`estudiante_identestudiante`);

--
-- Indices de la tabla `deporte`
--
ALTER TABLE `deporte`
  ADD PRIMARY KEY (`iddeporte`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`identdocente`),
  ADD KEY `docente_estado_fk` (`estado_idestado`),
  ADD KEY `docente_genero_fk` (`genero_idgenero`),
  ADD KEY `docente_tipodocumento_fk` (`tipodocumento_idtipodocumento`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`identestudiante`),
  ADD KEY `estudiante_acudiente_fk` (`acudiente_identacudiente`),
  ADD KEY `estudiante_estado_fk` (`estado_idestado`),
  ADD KEY `estudiante_genero_fk` (`genero_idgenero`),
  ADD KEY `estudiante_tipodocumento_fk` (`tipodocumento_idtipodocumento`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`numero_factura`),
  ADD KEY `descripcion` (`descripcion`),
  ADD KEY `tipo_documento` (`tipo_documento`),
  ADD KEY `subtotal` (`subtotal`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idgenero`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idgrupo`),
  ADD KEY `grupo_categoria_fk` (`categoria_idcategoria`),
  ADD KEY `grupo_deporte_fk` (`deporte_iddeporte`),
  ADD KEY `docente_id_docente` (`docente_id_docente`);

--
-- Indices de la tabla `grupodetalle`
--
ALTER TABLE `grupodetalle`
  ADD KEY `grupodetalle_estudiante_fk` (`estudiante_identestudiante`),
  ADD KEY `grupo_idgrupo` (`grupo_idgrupo`) USING BTREE;

--
-- Indices de la tabla `prefactura`
--
ALTER TABLE `prefactura`
  ADD PRIMARY KEY (`nro_prefactura`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`Numero_servicio`);

--
-- Indices de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD PRIMARY KEY (`idtipodocumento`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`correousuario`),
  ADD KEY `usuario_docente_fk` (`docente_identdocente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `numero_factura` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `prefactura`
--
ALTER TABLE `prefactura`
  MODIFY `nro_prefactura` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acudiente`
--
ALTER TABLE `acudiente`
  ADD CONSTRAINT `acudiente_genero_fk` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`),
  ADD CONSTRAINT `acudiente_tipodocumento_fk` FOREIGN KEY (`tipodocumento_idtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`);

--
-- Filtros para la tabla `controlasistencia`
--
ALTER TABLE `controlasistencia`
  ADD CONSTRAINT `controlasistencia_estudiante_fk` FOREIGN KEY (`estudiante_identestudiante`) REFERENCES `estudiante` (`identestudiante`),
  ADD CONSTRAINT `controlasistencia_grupo_fk` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`);

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `docente_estado_fk` FOREIGN KEY (`estado_idestado`) REFERENCES `estado` (`idestado`),
  ADD CONSTRAINT `docente_genero_fk` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`),
  ADD CONSTRAINT `docente_tipodocumento_fk` FOREIGN KEY (`tipodocumento_idtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`);

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `estudiante_acudiente_fk` FOREIGN KEY (`acudiente_identacudiente`) REFERENCES `acudiente` (`identacudiente`),
  ADD CONSTRAINT `estudiante_estado_fk` FOREIGN KEY (`estado_idestado`) REFERENCES `estado` (`idestado`),
  ADD CONSTRAINT `estudiante_genero_fk` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`),
  ADD CONSTRAINT `estudiante_tipodocumento_fk` FOREIGN KEY (`tipodocumento_idtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `grupo_categoria_fk` FOREIGN KEY (`categoria_idcategoria`) REFERENCES `categoria` (`idcategoria`),
  ADD CONSTRAINT `grupo_deporte_fk` FOREIGN KEY (`deporte_iddeporte`) REFERENCES `deporte` (`iddeporte`),
  ADD CONSTRAINT `grupo_ibfk_1` FOREIGN KEY (`docente_id_docente`) REFERENCES `docente` (`identdocente`);

--
-- Filtros para la tabla `grupodetalle`
--
ALTER TABLE `grupodetalle`
  ADD CONSTRAINT `grupodetalle_estudiante_fk` FOREIGN KEY (`estudiante_identestudiante`) REFERENCES `estudiante` (`identestudiante`),
  ADD CONSTRAINT `grupodetalle_grupo_fk` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_docente_fk` FOREIGN KEY (`docente_identdocente`) REFERENCES `docente` (`identdocente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
