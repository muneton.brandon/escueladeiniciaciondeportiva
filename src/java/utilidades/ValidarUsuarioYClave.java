
package utilidades;

import modelo.Usuario;

/**
* La clase interface sirve como intermediario entre el formulario login y la clase UsuarioDAO
* para realizar la validación de si el usuario puede pasar o no
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
public interface ValidarUsuarioYClave {
    public String validar(Usuario u);
}
