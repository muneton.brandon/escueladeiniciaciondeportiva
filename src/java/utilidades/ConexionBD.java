
package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;


/**
* La clase conexión es quien realiza la conexión con la bd
* @since marzo 2020
* @version 1
* @author ecardona
*/ 
public class ConexionBD {
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "root";
    private static final String password = "";
    private static final String url = "jdbc:mysql://localhost:3306/proyectocsweb";
    
    Connection con;
    
    public Connection getConection(){
        try{
            Class.forName(driver);
            con = DriverManager.getConnection(url,user,password);
        }catch (Exception e){
        
        }
        return con;
    }
}
