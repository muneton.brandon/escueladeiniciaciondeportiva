
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Acudiente;
import modelo.DetalleGrupo;
import modelo.Docente;
import modelo.Estudiante;
import modelo.Grupo;
import modelo.informes;
import modelo.seguimientoClaseAClase;
import persistencia.AcudienteDAO;
import persistencia.DetalleGrupoDAO;
import persistencia.DocenteDAO;
import persistencia.EstudianteDAO;
import persistencia.GrupoDAO;
import persistencia.informesDAO;
import persistencia.seguimientoClaseAClaseDAO;

/**
 *  esta clase contiene los informes que se descargan en Excel, recibe
 * el informe que desea descargar, lo proceso y lo entrega
 * @author ecardona
 * @param opcionInforme
 * @return Excel
 * @since abril 2020
 * @version 1
 */
@WebServlet(name = "DescargaInformes", urlPatterns = {"/DescargaInformes"})
public class DescargaInformes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String opcionInforme = request.getParameter("informe");
        informesDAO inf;
        List<informes> lista;
        PrintWriter out;
        
        if ("ResumenGeneral".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=ResumenGeneral.xls");

            inf = new informesDAO();
            lista = inf.ConsultarResumenGrupos();

            out = response.getWriter();
            try{
                out.println("Nombre Categoria\tNombre Deporte\tId Grupo\tObjetibo Grupo\tNombre Docente\tIdent Estudiante"
                        + "\tNombre Estudiante\tNota Final Estudiante");
                for(informes rg:lista)
                    {  
                        out.println(rg.getNombreCategoria() + "\t" + rg.getNombreDeporte() + "\t" + rg.getIdGrupo()
                        + "\t" + rg.getObjetivoGrupo() + "\t" + rg.getNombDocente() + "\t" + rg.getIdentificacionEstudiante()
                        + "\t" + rg.getNombEstudiante() + "\t" + rg.getNotaFinalEstudiante());
                    }

            }finally{
                out.close();
            }
        }
        if ("EstudiantesTOP".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=EstudiantesTOP.xls");
            
            inf = new informesDAO();
            lista = inf.ConsultarEstudiantesTop();

            out = response.getWriter();
            try{
                out.println("Nombre Categoria\tNombre Deporte\tId Grupo\tIdent Estudiante"
                        + "\tNombre Estudiante\tNota Final Estudiante");
                for(informes rg:lista)
                    {  
                        out.println(rg.getNombreCategoria() + "\t" + rg.getNombreDeporte() + "\t" + rg.getIdGrupo()
                        + "\t" + rg.getIdentificacionEstudiante() + "\t" + rg.getNombEstudiante() + "\t" 
                        + rg.getNotaFinalEstudiante());
                    }

            }finally{
                out.close();
            }
        }
        if ("PazYSallvoEstudiantes".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=PazYSallvoEstudiantes.xls");
            
            inf = new informesDAO();
            lista = inf.ConsultarPazYSalvoEstudiantes();

            out = response.getWriter();
            try{
                out.println("Ident Estudiante\tNombre Estudiante\tEstado Paz y Salvo (S=Si, N=No)");
                for(informes rg:lista)
                    {  
                        out.println(rg.getIdentificacionEstudiante() + "\t" + rg.getNombEstudiante() + "\t" 
                        + rg.getPazYSalvo());
                    }

            }finally{
                out.close();
            }
        }
        if ("ExportarGrupos".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=Grupos.xls");
            
            GrupoDAO gpDAO = new GrupoDAO();
            Grupo gp = new Grupo();
            List<Grupo> listaGrupos = gpDAO.ConsultarGrupos();

            out = response.getWriter();
            try{
                out.println("Id Grupo\tId Deporte\tId Categoria\tHorario Inicio\tHorario Fin\tObjetivo\tId Docente");
                for(Grupo rg:listaGrupos)
                    {  
                        out.println(rg.getIdGrupo() + "\t" + rg.getIdDeporte() + "\t" + rg.getIdCategoria() + "\t"
                        + rg.getHorarioInicio() + "\t" + rg.getHorarioFin() + "\t" + rg.getObjetivo() + "\t" + 
                        rg.getIdDocente());
                    }

            }finally{
                out.close();
            }
        }
        
        if ("ExportarSeguimientoClases".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=SeguimientoClases.xls");
            
            seguimientoClaseAClaseDAO segDAO = new seguimientoClaseAClaseDAO();
            seguimientoClaseAClase seg = new seguimientoClaseAClase();
            List<seguimientoClaseAClase> listaSeguimiento = segDAO.ConsultarSeguimiento();

            out = response.getWriter();
            try{
                out.println("Id Grupo\tId Estudiante\tFecha Clase\tObjetivo clase\tNota Clase\tObservación Clase");
                for(seguimientoClaseAClase rg:listaSeguimiento)
                    {  
                        out.println(rg.getIdGrupo() + "\t" + rg.getIdEstudiante() + "\t" + rg.getFechaClaseControl() + "\t"
                        + rg.getObjetivosClaseControl() + "\t" + rg.getNotaClaseControl() + "\t" + rg.getObervClaseControl());
                    }

            }finally{
                out.close();
            }
        }
        
        if ("ExportarDetalleGrupos".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=DetalleGrupos.xls");
            
            DetalleGrupoDAO DetGDAO = new DetalleGrupoDAO();
            DetalleGrupo DetG = new DetalleGrupo();
            List<DetalleGrupo> listaDetalle = DetGDAO.ConsultarDetGrupo();

            out = response.getWriter();
            try{
                out.println("Id Grupo\tId Estudiante\tNota Final");
                for(DetalleGrupo rg:listaDetalle)
                    {  
                        out.println(rg.getIdGrupo() + "\t" + rg.getIdEstudiante() + "\t" + rg.getNotaFinalEst());
                    }

            }finally{
                out.close();
            }
        }
        
        if ("ExportarAcudientes".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=Acudientes.xls");
            
            AcudienteDAO aDAO = new AcudienteDAO();
            Acudiente a = new Acudiente();
            List<Acudiente> listaAcudientes = aDAO.ConsultarAcudientes();

            out = response.getWriter();
            try{
                out.println("Tipo identificación\tNro. Identif\tnombre\tFecha Nacimiento\tDirección\tTeléfono\t"
                        + "Género\tVínculo Instit");
                for(Acudiente rg:listaAcudientes)
                    {  
                        out.println(rg.getTipoDocumentoAcudiente() + "\t" + rg.getIdentificacionAcudiente() + 
                        "\t" + rg.getNombreAcudiente() + "\t" + rg.getFechaNacimientoAcudiente() + "\t" + 
                        rg.getDireccionAcudiente() + "\t" + rg.getTelefonoAcudiente() + "\t" + rg.getGeneroAcudiente() 
                        + "\t" + rg.getVinculoInstitucionAcudiente());
                    }

            }finally{
                out.close();
            }
        }
        
        if ("ExportarEstudiantes".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=Estudiantes.xls");
            
            EstudianteDAO eDAO = new EstudianteDAO();
            Estudiante e = new Estudiante();
            List<Estudiante> listaEstudiante = eDAO.ConsultarEstudiantes();

            out = response.getWriter();
            try{
                out.println("Tipo identificación\tNro. Identif\tnombre\tFecha Nacimiento\tDirección\tTeléfono"
                        + "\tGénero\tEstado\tAcudiente\tPaz y Salvo");
                for(Estudiante rg:listaEstudiante)
                    {  
                        out.println(rg.getTipoIdentificacionEstudiante() + "\t" + rg.getIdentificacionEstudiante() + 
                        "\t" + rg.getNombreEstudiante() + "\t" + rg.getFechaNacimientoEstudiante() + "\t" + 
                        rg.getDireccionEstudiante() + "\t" + rg.getTelefonoEstudiante() + "\t" + rg.getGeneroEstudiante()
                        + "\t" + rg.getEstadoEstudiante() + "\t" + rg.getAcudienteEstudiante() +"\t" + rg.getPazYSalvoEstudiante());
                    }

            }finally{
                out.close();
            }
        }
        
        if ("ExportarDocentes".equals(opcionInforme)){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition","filename=Docentes.xls");
            
            DocenteDAO dDAO = new DocenteDAO();
            Docente d = new Docente();
            List<Docente> listaDocente = dDAO.ConsultarDocentes();

            out = response.getWriter();
            try{
                out.println("Tipo identificación\tNro. Identif\tnombre\tFecha Nacimiento\tDirección\tTelefono"
                        + "\tCorreo\tEstudios\tGénero\tEstado");
                for(Docente rg:listaDocente)
                    {  
                        out.println(rg.getTipoDocumento() + "\t" + rg.getIdentificacionDocente() + 
                        "\t" + rg.getNombreDocente() + "\t" + rg.getFechaNacimientoDocente() + "\t" + 
                        rg.getDireccionDocente() + "\t" + rg.getTelefonoDocente() + "\t" + 
                        rg.getCorreoDocente() + "\t" + rg.getEstudiosDocente() + "\t" 
                        + rg.getGeneroDocente() +"\t" + rg.getEstadoDocente());
                    }

            }finally{
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
