package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Acudiente;
import persistencia.AcudienteDAO;
import modelo.Categoria;
import persistencia.CategoriaDAO;
import modelo.seguimientoClaseAClase;
import persistencia.seguimientoClaseAClaseDAO;
import modelo.Deporte;
import persistencia.DeporteDAO;
import modelo.DetalleGrupo;
import persistencia.DetalleGrupoDAO;
import modelo.Docente;
import persistencia.DocenteDAO;
import modelo.Estado;
import persistencia.EstadoDAO;
import modelo.Estudiante;
import modelo.Factura;
import persistencia.EstudianteDAO;
import modelo.Genero;
import persistencia.GeneroDAO;
import modelo.Grupo;
import modelo.Prefactura;
import modelo.Servicio;
import persistencia.GrupoDAO;
import persistencia.UsuarioDAO;
import modelo.Usuario;
import modelo.tipoDocumento;
import persistencia.FacturaDAO;
import persistencia.PrefacturaDAO;
import persistencia.ServicioDAO;
import persistencia.informesDAO;
import persistencia.tipoDocumentoDAO;

/**
 * La clase controlador sirve como interfaz entre las vistas (Login, Menú,
 * formularios, ...) y la bd
 *
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Controlador extends HttpServlet {

    UsuarioDAO uDAO = new UsuarioDAO();
    Usuario u = new Usuario();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /**
         * En esta parte se reciben los campos de los formularios, el formulario
         * de donde viene, y la acción que va a realizar y dependiendo de los
         * datos recibidos ejecuta una acción. Enviando primero los datos
         * recibidos a un objeto de la clase adecuada (usuario, estudiante,
         * docente, acudiente, ...) y luego enviando ese objeto el método DAO de
         * ese objeto para que se ejecuta en la bd.
         *
         * @param botonProcedenciaFormulario String: indica de que formulario
         * viene
         * @param opciones String: indica si va a crear, consultar, modificar o
         * borrar
         * @param tipoUsuario String: indica que perfil tiene para saber qué
         * mostrarle
         * @return resultadoOperacion String: recibe el resultado de la opción
         * en la bd, para mostrarlo en el formulario
         * @since marzo 2020
         * @version 1
         * @author ecardona
         */
        response.setContentType("text/html;charset=UTF-8");
        String botonProcedenciaFormulario = request.getParameter("accion");
        String CorreoUsuario = "";
        String Clave = "";
        String identificacionDocente = "";
        String tipoUsuario = "";
        tipoUsuario = uDAO.validar(u);//le dice que permisos le va a mostrar
        String opciones = ""; //indica si va a crear, modificar, borrar o consultar
        String resultadoOperacion = "";

        //el siguiente if valida que venga del formulario Loging, y si el correo y la clave estan en la bd, los re direcciona al Menú
        if (botonProcedenciaFormulario.equals("Acceder al Sistema")) {
            CorreoUsuario = request.getParameter("email");
            Clave = request.getParameter("clave");
            u.setCorreoUsuario(CorreoUsuario);
            u.setClave(Clave);
            tipoUsuario = uDAO.validar(u);
            if (tipoUsuario.equals("Admin") || tipoUsuario.equals("Docente")) {
                request.getSession().setAttribute("correo", CorreoUsuario); //recibe el correo del formularo de donde viene
                request.getSession().setAttribute("tipoUsuario", tipoUsuario); //recibe el tipoUsuario del formularo de donde viene
                request.getRequestDispatcher("menu.jsp?perfil=" + tipoUsuario).forward(request, response); //redirecciona al menú y envía el parametro perfil
            } else {
                request.getRequestDispatcher("paginaDeErrores.jsp").forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Usuarios y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Usuario")) {
            opciones = request.getParameter("opciones");
            if (opciones.equals("Crear")) {
                CorreoUsuario = request.getParameter("email");
                Clave = request.getParameter("clave");
                identificacionDocente = request.getParameter("identificacionDocente");
                tipoUsuario = request.getParameter("Perfil");

                u.setCorreoUsuario(CorreoUsuario + "@elpoli.edu.co");
                u.setClave(Clave);
                u.setIdentificacionDocente(identificacionDocente);
                u.setTipoUsuario(tipoUsuario);
                resultadoOperacion = uDAO.GuardarUsuario(u);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=usuarios&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                CorreoUsuario = request.getParameter("email");
                Clave = request.getParameter("clave");
                identificacionDocente = request.getParameter("identificacionDocente");
                tipoUsuario = request.getParameter("Perfil");

                u.setCorreoUsuario(CorreoUsuario + "@elpoli.edu.co");
                u.setClave(Clave);
                u.setIdentificacionDocente(identificacionDocente);
                u.setTipoUsuario(tipoUsuario);
                resultadoOperacion = uDAO.ActualizarUsuario(u);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=usuarios&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                CorreoUsuario = request.getParameter("email");

                u.setCorreoUsuario(CorreoUsuario + "@elpoli.edu.co");

                resultadoOperacion = uDAO.BorrarUsuario(u);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=usuarios&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=usuarios&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de tipos de documentos y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar tipo Documento")) {
            tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
            tipoDocumento td = new tipoDocumento();
            opciones = request.getParameter("opciones");
            if (opciones.equals("Crear")) {
                String idTipoDocumento;
                idTipoDocumento = request.getParameter("idTipoDocumento");
                String nombreDocumento;
                nombreDocumento = request.getParameter("nombreDocumento");

                td.setIdDocumento(idTipoDocumento);
                td.setNombreDocumento(nombreDocumento);

                resultadoOperacion = tdDAO.GuardarTipoDocumento(td);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=tipoDocumento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                String idTipoDocumento;
                idTipoDocumento = request.getParameter("idTipoDocumento");
                String nombreDocumento;
                nombreDocumento = request.getParameter("nombreDocumento");

                td.setIdDocumento(idTipoDocumento);
                td.setNombreDocumento(nombreDocumento);
                resultadoOperacion = tdDAO.ActualizarTipoDocumento(td);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=tipoDocumento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                String idTipoDocumento;
                idTipoDocumento = request.getParameter("idTipoDocumento");
                td.setIdDocumento(idTipoDocumento);
                resultadoOperacion = tdDAO.BorrarTipoDocumento(td);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=tipoDocumento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=tipoDocumento&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Estados y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Estado")) {
            EstadoDAO esDAO = new EstadoDAO();
            Estado es = new Estado();
            String idEstado;
            String descripcionestado;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idEstado = request.getParameter("idEstado");
                descripcionestado = request.getParameter("DescripcionEstado");
                es.setId(Integer.parseInt(idEstado));
                es.setNombreEstado(descripcionestado);
                resultadoOperacion = esDAO.GuardarEstado(es);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estado&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idEstado = request.getParameter("idEstado");
                descripcionestado = request.getParameter("DescripcionEstado");
                es.setId(Integer.parseInt(idEstado));
                es.setNombreEstado(descripcionestado);
                resultadoOperacion = esDAO.ActualizarEstado(es);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estado&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idEstado = request.getParameter("idEstado");

                es.setId(Integer.parseInt(idEstado));

                resultadoOperacion = esDAO.BorrarEstado(es);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estado&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=estado&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Generos y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Genero")) {
            GeneroDAO gDAO = new GeneroDAO();
            Genero g = new Genero();
            String idGenero;
            idGenero = request.getParameter("idGenero");
            String DescripcionGenero;
            DescripcionGenero = request.getParameter("descripcionGenero");
            opciones = request.getParameter("opciones");
            g.setIdGenero(idGenero);
            g.setDescripcionGenero(DescripcionGenero);
            if (opciones.equals("Crear")) {
                resultadoOperacion = gDAO.GuardarGenero(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=genero&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                resultadoOperacion = gDAO.ActualizarGenero(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=genero&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                resultadoOperacion = gDAO.BorrarGenero(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=genero&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=genero&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Categorías y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Categoria")) {
            CategoriaDAO cDAO = new CategoriaDAO();
            Categoria c = new Categoria();
            String idCategoria;
            String DescripcionCategoria;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idCategoria = request.getParameter("idCategoria");
                DescripcionCategoria = request.getParameter("descripcionCategoria");
                c.setIdCategoria(Integer.parseInt(idCategoria));
                c.setDescripcionCategoria(DescripcionCategoria);
                resultadoOperacion = cDAO.GuardarCategoria(c);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=categoria&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idCategoria = request.getParameter("idCategoria");
                DescripcionCategoria = request.getParameter("descripcionCategoria");
                c.setIdCategoria(Integer.parseInt(idCategoria));
                c.setDescripcionCategoria(DescripcionCategoria);
                resultadoOperacion = cDAO.ActualizarCategoria(c);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=categoria&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idCategoria = request.getParameter("idCategoria");

                c.setIdCategoria(Integer.parseInt(idCategoria));

                resultadoOperacion = cDAO.BorrarCategoria(c);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=categoria&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=categoria&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Deportes y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Deporte")) {
            DeporteDAO dDAO = new DeporteDAO();
            Deporte d = new Deporte();
            String idDeporte;
            String DescripcionDeporte;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idDeporte = request.getParameter("idDeporte");
                DescripcionDeporte = request.getParameter("descripcionDeporte");
                d.setIdDeporte(Integer.parseInt(idDeporte));
                d.setDescripcionDeporte(DescripcionDeporte);
                resultadoOperacion = dDAO.GuardarDeporte(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=deporte&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idDeporte = request.getParameter("idDeporte");
                DescripcionDeporte = request.getParameter("descripcionDeporte");
                d.setIdDeporte(Integer.parseInt(idDeporte));
                d.setDescripcionDeporte(DescripcionDeporte);
                resultadoOperacion = dDAO.ActualizarDeporte(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=deporte&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idDeporte = request.getParameter("idDeporte");

                d.setIdDeporte(Integer.parseInt(idDeporte));

                resultadoOperacion = dDAO.BorrarDeporte(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=deporte&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=deporte&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Docentes y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Docente")) {
            DocenteDAO dDAO = new DocenteDAO();
            Docente d = new Docente();
            String tipoDocumento;
            String identDocente;
            String nombreDocente;
            String fechaNacimientoDocente;
            String direccionDocente;
            String telefonoDocente;
            String correoDocente;
            String estudiosDocente;
            String generoDocente;
            String estadoDocente;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                tipoDocumento = request.getParameter("tipoIdentificacion");
                identDocente = request.getParameter("identificacionDocente");
                nombreDocente = request.getParameter("nombreDocente");
                fechaNacimientoDocente = request.getParameter("fechaNacimientoDocente");
                Date fechaConvertida = dDAO.ConvertirFecha(fechaNacimientoDocente);
                direccionDocente = request.getParameter("direccionDocente");
                telefonoDocente = request.getParameter("telefonoDocente");
                correoDocente = request.getParameter("emailDocente");
                estudiosDocente = request.getParameter("estudiosDocente");
                generoDocente = request.getParameter("generoDocente");
                estadoDocente = request.getParameter("estadoDocente");

                d.setTipoDocumento(tipoDocumento);
                d.setIdentificacionDocente(Integer.parseInt(identDocente));
                d.setNombreDocente(nombreDocente);
                d.setFechaNacimientoDocente(fechaConvertida);
                d.setDireccionDocente(direccionDocente);
                d.setTelefonoDocente(telefonoDocente);
                d.setCorreoDocente(correoDocente);
                d.setEstudiosDocente(estudiosDocente);
                d.setGeneroDocente(generoDocente);
                d.setEstadoDocente(estadoDocente);

                resultadoOperacion = dDAO.GuardarDocente(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=docente&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                tipoDocumento = request.getParameter("tipoIdentificacion");
                identDocente = request.getParameter("identificacionDocente");
                nombreDocente = request.getParameter("nombreDocente");
                fechaNacimientoDocente = request.getParameter("fechaNacimientoDocente");
                Date fechaConvertida = dDAO.ConvertirFecha(fechaNacimientoDocente);
                direccionDocente = request.getParameter("direccionDocente");
                telefonoDocente = request.getParameter("telefonoDocente");
                correoDocente = request.getParameter("emailDocente");
                estudiosDocente = request.getParameter("estudiosDocente");
                generoDocente = request.getParameter("generoDocente");
                estadoDocente = request.getParameter("estadoDocente");

                d.setTipoDocumento(tipoDocumento);
                d.setIdentificacionDocente(Integer.parseInt(identDocente));
                d.setNombreDocente(nombreDocente);
                d.setFechaNacimientoDocente(fechaConvertida);
                d.setDireccionDocente(direccionDocente);
                d.setTelefonoDocente(telefonoDocente);
                d.setCorreoDocente(correoDocente);
                d.setEstudiosDocente(estudiosDocente);
                d.setGeneroDocente(generoDocente);
                d.setEstadoDocente(estadoDocente);

                resultadoOperacion = dDAO.ActualizarDocente(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=docente&perfil=" + tipoUsuario).forward(request, response);

            }
            if (opciones.equals("Borrar")) {

                identDocente = request.getParameter("identificacionDocente");

                d.setIdentificacionDocente(Integer.parseInt(identDocente));

                resultadoOperacion = dDAO.BorrarDocente(d);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=docente&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=docente&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Acudientes y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Acudiente")) {
            AcudienteDAO aDAO = new AcudienteDAO();
            Acudiente a = new Acudiente();
            String tipoDocumentoAcudiente;
            String identAcudiente;
            String nombreAcudiente;
            String fechaNacimientoAcudiente;
            String direccionAcudiente;
            String telefonoAcudiente;
            String generoAcudiente;
            String vinculoInstitucionAcudiente;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                tipoDocumentoAcudiente = request.getParameter("tipoIdentificacion");
                identAcudiente = request.getParameter("identificacionAcudiente");
                nombreAcudiente = request.getParameter("nombreAcudiente");
                fechaNacimientoAcudiente = request.getParameter("fechaNacimientoAcudiente");
                Date fechaConvertida = aDAO.ConvertirFecha(fechaNacimientoAcudiente);
                direccionAcudiente = request.getParameter("direccionAcudiente");
                telefonoAcudiente = request.getParameter("telefonoAcudiente");
                generoAcudiente = request.getParameter("generoAcudiente");
                vinculoInstitucionAcudiente = request.getParameter("Perfil");

                a.setTipoDocumentoAcudiente(tipoDocumentoAcudiente);
                a.setIdentificacionAcudiente(Integer.parseInt(identAcudiente));
                a.setNombreAcudiente(nombreAcudiente);
                a.setFechaNacimientoAcudiente(fechaConvertida);
                a.setDireccionAcudiente(direccionAcudiente);
                a.setTelefonoAcudiente(telefonoAcudiente);
                a.setGeneroAcudiente(generoAcudiente);
                a.setVinculoInstitucionAcudiente(vinculoInstitucionAcudiente);

                resultadoOperacion = aDAO.GuardarAcudiente(a);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=acudiente&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                tipoDocumentoAcudiente = request.getParameter("tipoIdentificacion");
                identAcudiente = request.getParameter("identificacionAcudiente");
                nombreAcudiente = request.getParameter("nombreAcudiente");
                fechaNacimientoAcudiente = request.getParameter("fechaNacimientoAcudiente");
                Date fechaConvertida = aDAO.ConvertirFecha(fechaNacimientoAcudiente);
                direccionAcudiente = request.getParameter("direccionAcudiente");
                telefonoAcudiente = request.getParameter("telefonoAcudiente");
                generoAcudiente = request.getParameter("generoAcudiente");
                vinculoInstitucionAcudiente = request.getParameter("Perfil");

                a.setTipoDocumentoAcudiente(tipoDocumentoAcudiente);
                a.setIdentificacionAcudiente(Integer.parseInt(identAcudiente));
                a.setNombreAcudiente(nombreAcudiente);
                a.setFechaNacimientoAcudiente(fechaConvertida);
                a.setDireccionAcudiente(direccionAcudiente);
                a.setTelefonoAcudiente(telefonoAcudiente);
                a.setGeneroAcudiente(generoAcudiente);
                a.setVinculoInstitucionAcudiente(vinculoInstitucionAcudiente);

                resultadoOperacion = aDAO.ActualizarAcudiente(a);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=acudiente&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {

                identAcudiente = request.getParameter("identificacionAcudiente");

                a.setIdentificacionAcudiente(Integer.parseInt(identAcudiente));

                resultadoOperacion = aDAO.BorrarAcudiente(a);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=acudiente&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=acudiente&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Estudiantes y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Estudiante")) {
            EstudianteDAO eDAO = new EstudianteDAO();
            Estudiante e = new Estudiante();
            String tipoDocumentoEstudiante;
            String identEstudiante;
            String nombreEstudiante;
            String fechaNacimientoEstudiante;
            String direccionEstudiante;
            String telefonoEstudiante;
            String generoEstudiante;
            String estadoEstudiante;
            String acudienteEstudiante;
            //String pazYSalvoEstudiante;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                tipoDocumentoEstudiante = request.getParameter("tipoIdentificacionEstudiante");
                identEstudiante = request.getParameter("identificacionEstudiante");
                nombreEstudiante = request.getParameter("nombreEstudiante");
                fechaNacimientoEstudiante = request.getParameter("fechaNacimientoEstudiante");
                Date fechaConvertida = eDAO.ConvertirFecha(fechaNacimientoEstudiante);
                direccionEstudiante = request.getParameter("direccionEstudiante");
                telefonoEstudiante = request.getParameter("telefonoEstudiante");
                generoEstudiante = request.getParameter("generoEstudiante");
                estadoEstudiante = request.getParameter("estadoAcudiente");
                acudienteEstudiante = request.getParameter("acudienteEstudiante");

                e.setTipoIdentificacionEstudiante(tipoDocumentoEstudiante);
                e.setIdentificacionEstudiante(Integer.parseInt(identEstudiante));
                e.setNombreEstudiante(nombreEstudiante);
                e.setFechaNacimientoEstudiante(fechaConvertida);
                e.setDireccionEstudiante(direccionEstudiante);
                e.setTelefonoEstudiante(telefonoEstudiante);
                e.setGeneroEstudiante(generoEstudiante);
                e.setEstadoEstudiante(estadoEstudiante);
                e.setAcudienteEstudiante(Integer.parseInt(acudienteEstudiante));

                resultadoOperacion = eDAO.GuardarEstudiante(e);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estudiante&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                tipoDocumentoEstudiante = request.getParameter("tipoIdentificacionEstudiante");
                identEstudiante = request.getParameter("identificacionEstudiante");
                nombreEstudiante = request.getParameter("nombreEstudiante");
                fechaNacimientoEstudiante = request.getParameter("fechaNacimientoEstudiante");
                Date fechaConvertida = eDAO.ConvertirFecha(fechaNacimientoEstudiante);
                direccionEstudiante = request.getParameter("direccionEstudiante");
                telefonoEstudiante = request.getParameter("telefonoEstudiante");
                generoEstudiante = request.getParameter("generoEstudiante");
                estadoEstudiante = request.getParameter("estadoAcudiente");
                acudienteEstudiante = request.getParameter("acudienteEstudiante");

                e.setTipoIdentificacionEstudiante(tipoDocumentoEstudiante);
                e.setIdentificacionEstudiante(Integer.parseInt(identEstudiante));
                e.setNombreEstudiante(nombreEstudiante);
                e.setFechaNacimientoEstudiante(fechaConvertida);
                e.setDireccionEstudiante(direccionEstudiante);
                e.setTelefonoEstudiante(telefonoEstudiante);
                e.setGeneroEstudiante(generoEstudiante);
                e.setEstadoEstudiante(estadoEstudiante);
                e.setAcudienteEstudiante(Integer.parseInt(acudienteEstudiante));

                resultadoOperacion = eDAO.ActualizarEstudiante(e);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estudiante&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {

                identEstudiante = request.getParameter("identificacionEstudiante");

                e.setIdentificacionEstudiante(Integer.parseInt(identEstudiante));

                resultadoOperacion = eDAO.BorrarEstudiante(e);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=estudiante&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=estudiante&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Grupo y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Grupo")) {
            GrupoDAO gDAO = new GrupoDAO();
            Grupo g = new Grupo();
            String idGrupo;
            String idDeporte;
            String idCategoria;
            String horarioInicio;
            String horarioFin;
            String objetivoGrupo;
            String identDocente;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idGrupo = request.getParameter("idGrupo");
                idDeporte = request.getParameter("idDeporte");
                idCategoria = request.getParameter("idCategoria");
                horarioInicio = request.getParameter("horaInicioGrupo");
                horarioFin = request.getParameter("horaFinGrupo");
                objetivoGrupo = request.getParameter("objetivoGrupo");
                identDocente = request.getParameter("identificacionDocente");

                g.setIdGrupo(Integer.parseInt(idGrupo));
                g.setIdDeporte(Integer.parseInt(idDeporte));
                g.setIdCategoria(Integer.parseInt(idCategoria));
                g.setHorarioInicio(horarioInicio);
                g.setHorarioFin(horarioFin);
                g.setObjetivo(objetivoGrupo);
                g.setIdDocente(Integer.parseInt(identDocente));

                resultadoOperacion = gDAO.GuardarGrupo(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=grupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idGrupo = request.getParameter("idGrupo");
                idDeporte = request.getParameter("idDeporte");
                idCategoria = request.getParameter("idCategoria");
                horarioInicio = request.getParameter("horaInicioGrupo");
                horarioFin = request.getParameter("horaFinGrupo");
                objetivoGrupo = request.getParameter("objetivoGrupo");
                identDocente = request.getParameter("identificacionDocente");

                g.setIdGrupo(Integer.parseInt(idGrupo));
                g.setIdDeporte(Integer.parseInt(idDeporte));
                g.setIdCategoria(Integer.parseInt(idCategoria));
                g.setHorarioInicio(horarioInicio);
                g.setHorarioFin(horarioFin);
                g.setObjetivo(objetivoGrupo);
                g.setIdDocente(Integer.parseInt(identDocente));

                resultadoOperacion = gDAO.ActualizarGrupo(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=grupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idGrupo = request.getParameter("idGrupo");
                idDeporte = request.getParameter("idDeporte");
                idCategoria = request.getParameter("idCategoria");

                g.setIdGrupo(Integer.parseInt(idGrupo));
                g.setIdDeporte(Integer.parseInt(idDeporte));
                g.setIdCategoria(Integer.parseInt(idCategoria));

                resultadoOperacion = gDAO.BorrarGrupo(g);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=grupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=grupo&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de Detalle Grupo y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Detalle Grupo")) {
            DetalleGrupoDAO dgDAO = new DetalleGrupoDAO();
            DetalleGrupo dg = new DetalleGrupo();
            String idGrupo;
            String idEstudiante;
            String notaFinalEst;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");
                //notaFinalEst = request.getParameter("notaFinal");

                dg.setIdGrupo(Integer.parseInt(idGrupo));
                dg.setIdEstudiante(Integer.parseInt(idEstudiante));
                //dg.setNotaFinalEst(Float.parseFloat(notaFinalEst));

                resultadoOperacion = dgDAO.GuardarDetGrupo(dg);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=detalleGrupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");
                notaFinalEst = request.getParameter("notaFinal");

                dg.setIdGrupo(Integer.parseInt(idGrupo));
                dg.setIdEstudiante(Integer.parseInt(idEstudiante));
                dg.setNotaFinalEst(Float.parseFloat(notaFinalEst));

                resultadoOperacion = dgDAO.ActualizarDetGrupo(dg);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=detalleGrupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");

                dg.setIdGrupo(Integer.parseInt(idGrupo));
                dg.setIdEstudiante(Integer.parseInt(idEstudiante));

                resultadoOperacion = dgDAO.BorrarDetGrupo(dg);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=detalleGrupo&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=detalleGrupo&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario de seguimiento y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("Procesar Seguimiento")) {
            seguimientoClaseAClaseDAO caDAO = new seguimientoClaseAClaseDAO();
            seguimientoClaseAClase ca = new seguimientoClaseAClase();
            String idGrupo;
            String idEstudiante;
            String fechaClaseControl;
            String objetivosClaseControl;
            String notaClaseControl;
            String obervClaseControl;

            opciones = request.getParameter("opciones");

            if (opciones.equals("Crear")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");
                fechaClaseControl = request.getParameter("fechaClase");
                Date fechaConvertida = caDAO.ConvertirFecha(fechaClaseControl);
                objetivosClaseControl = request.getParameter("objetivoClase");
                notaClaseControl = request.getParameter("notaClase");
                obervClaseControl = request.getParameter("observacionClase");

                ca.setIdGrupo(Integer.parseInt(idGrupo));
                ca.setIdEstudiante(Integer.parseInt(idEstudiante));
                ca.setFechaClaseControl(fechaConvertida);
                ca.setObjetivosClaseControl(objetivosClaseControl);
                ca.setNotaClaseControl(Float.parseFloat(notaClaseControl));
                ca.setObervClaseControl(obervClaseControl);

                resultadoOperacion = caDAO.GuardarControlAsistencia(ca);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=seguimiento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Actualizar")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");
                fechaClaseControl = request.getParameter("fechaClase");
                Date fechaConvertida = caDAO.ConvertirFecha(fechaClaseControl);
                objetivosClaseControl = request.getParameter("objetivoClase");
                notaClaseControl = request.getParameter("notaClase");
                obervClaseControl = request.getParameter("observacionClase");

                ca.setIdGrupo(Integer.parseInt(idGrupo));
                ca.setIdEstudiante(Integer.parseInt(idEstudiante));
                ca.setFechaClaseControl(fechaConvertida);
                ca.setObjetivosClaseControl(objetivosClaseControl);
                ca.setNotaClaseControl(Float.parseFloat(notaClaseControl));
                ca.setObervClaseControl(obervClaseControl);

                resultadoOperacion = caDAO.ActualizarControlAsistencia(ca);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=seguimiento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Borrar")) {
                idGrupo = request.getParameter("idGrupo");
                idEstudiante = request.getParameter("idEstudiante");
                fechaClaseControl = request.getParameter("fechaClase");
                Date fechaConvertida = caDAO.ConvertirFecha(fechaClaseControl);

                ca.setIdGrupo(Integer.parseInt(idGrupo));
                ca.setIdEstudiante(Integer.parseInt(idEstudiante));
                ca.setFechaClaseControl(fechaConvertida);

                resultadoOperacion = caDAO.BorrarControlAsistencia(ca);
                request.getRequestDispatcher("formulario.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=seguimiento&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=seguimiento&perfil=" + tipoUsuario).forward(request, response);
            }
        }
        

        //el siguiente if valida que venga del formulario de facturas y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("procesar factura")) {
            FacturaDAO fDAO = new FacturaDAO();
            Factura f = new Factura();
            ServicioDAO sDAO = new ServicioDAO();
            Servicio s = new Servicio();
            
            String nroFactura;
            String fechaExp;
            String fechaVenc;
            String tipoDoc;
            String nroDoc;
            String acudiente;
            String estudiante;
            String idServicio;
            double subtotal=0;
            double porcIva =0.19;
            double iva=0;
            double porcDesc=0;
            double descuento=0;
            double valorTotal=0;
            String estado;
            String observaciones;
            String vinculo;
            opciones = request.getParameter("opciones");
            if (opciones.equals("Crear")) {
                //nroFactura = request.getParameter("NroFactura");
                fechaExp = request.getParameter("fechaExp");
                fechaVenc = request.getParameter("fechaVenc");
                tipoDoc = request.getParameter("tipodoc");
                nroDoc = request.getParameter("identificacion");
                acudiente = request.getParameter("acudiente");
                estudiante = request.getParameter("estudiante");
                idServicio = request.getParameter("servicio");
                //subtotal = request.getParameter("subtotal");
                //porcIva = request.getParameter("porcIva");
                //iva = request.getParameter("iva");
                //porcDesc = request.getParameter("porcDesc");
                //descuento = request.getParameter("descuento");
                //valorTotal = request.getParameter("valortotal");
                estado = request.getParameter("estado");
                observaciones = request.getParameter("observaciones");
                
                //llena el sub total de acuerdo al servicio elegido
                List<Servicio> listaServicio = sDAO.ConsultarServicio(Integer.parseInt(idServicio));
                    for(Servicio ser:listaServicio)
                {
                    subtotal= ser.getValor();
                }
                
                //Asigna el descuento de acuerdo a si es empleado, estudiante, ...
                vinculo = request.getParameter("vinculo");
                if (vinculo.equals("Empleado")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Estudiante")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Egresado")) {
                    porcDesc = 0.15;
                }
                if (vinculo.equals("Externo")) {
                    porcDesc = 0;
                }
                descuento = subtotal * porcDesc;
                iva = subtotal * porcIva;
                valorTotal = subtotal - descuento + iva;
               // f.setNroFactura(Integer.parseInt(nroFactura));
                f.setFechaExp(fechaExp);
                f.setFechaVenc(fechaVenc);
                f.setTipoDocumento(tipoDoc);
                f.setIdentificacion(Integer.parseInt(nroDoc));
                f.setAcudiente(acudiente);
                f.setAlumno(estudiante);
                f.setDescripcion(idServicio);
                f.setSubtotal(subtotal);
                f.setPorcIva(porcIva);
                f.setIva(iva);
                f.setPorcDescuento(porcDesc);
                f.setDescuento(descuento);
                f.setValorTotal(valorTotal);
                f.setEstado(estado);
                f.setObservaciones(observaciones);
                
                resultadoOperacion = fDAO.GuardarFactura(f);
                request.getRequestDispatcher("consultas.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=Factura&perfil=" + tipoUsuario).forward(request, response);
            }

            if (opciones.equals("Actualizar")) {
                nroFactura = request.getParameter("NroFactura");
                fechaExp = request.getParameter("fechaExp");
                fechaVenc = request.getParameter("fechaVenc");
                tipoDoc = request.getParameter("tipodoc");
                nroDoc = request.getParameter("identificacion");
                acudiente = request.getParameter("acudiente");
                estudiante = request.getParameter("estudiante");
                idServicio = request.getParameter("servicio");
                subtotal = Double.parseDouble(request.getParameter("subtotal"));
                porcIva = Double.parseDouble(request.getParameter("porcIva"));
                iva = Double.parseDouble(request.getParameter("iva"));
                porcDesc = Double.parseDouble(request.getParameter("porcDesc"));
                descuento = Double.parseDouble(request.getParameter("descuento"));
                valorTotal = Double.parseDouble(request.getParameter("valortotal"));
                estado = request.getParameter("estado");
                observaciones = request.getParameter("observaciones");
                
                //llena el sub total de acuerdo al servicio elegido
                List<Servicio> listaServicio = sDAO.ConsultarServicio(Integer.parseInt(idServicio));
                    for(Servicio ser:listaServicio)
                {
                    subtotal= ser.getValor();
                }
                
                //Asigna el descuento de acuerdo a si es empleado, estudiante, ...
                vinculo = request.getParameter("vinculo");
                if (vinculo.equals("Empleado")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Estudiante")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Egresado")) {
                    porcDesc = 0.15;
                }
                if (vinculo.equals("Externo")) {
                    porcDesc = 0;
                }
                descuento = subtotal * porcDesc;
                iva = subtotal * porcIva;
                valorTotal = subtotal - descuento + iva;
                f.setNroFactura(Integer.parseInt(nroFactura));
                f.setFechaExp(fechaExp);
                f.setFechaVenc(fechaVenc);
                f.setTipoDocumento(tipoDoc);
                f.setIdentificacion(Integer.parseInt(nroDoc));
                f.setAcudiente(acudiente);
                f.setAlumno(estudiante);
                f.setDescripcion(idServicio);
                f.setSubtotal(subtotal);
                f.setPorcIva(porcIva);
                f.setIva(iva);
                f.setPorcDescuento(porcDesc);
                f.setDescuento(descuento);
                f.setValorTotal(valorTotal);
                f.setEstado(estado);
                f.setObservaciones(observaciones);
                
                resultadoOperacion = fDAO.ActualizarFactura(f);
                request.getRequestDispatcher("consultas.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=Factura&perfil=" + tipoUsuario).forward(request, response);
                }
            if (opciones.equals("Anular")) {
                nroFactura = request.getParameter("NroFactura");
                observaciones = request.getParameter("observaciones");
                f.setNroFactura(Integer.parseInt(nroFactura));
                f.setEstado("Anulado");
                f.setObservaciones(observaciones);
                resultadoOperacion = fDAO.AnularFactura(f);
                request.getRequestDispatcher("consultas.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=Factura&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=factura&perfil=" + tipoUsuario).forward(request, response);
            }
        }
        
        //el siguiente if valida que venga del formulario de prefacturas y ejecuta la opción elegida en la bd
        if (botonProcedenciaFormulario.equals("procesar pre-factura")) {
            PrefacturaDAO pfDAO = new PrefacturaDAO();
            Prefactura pf = new Prefactura();
            ServicioDAO sDAO = new ServicioDAO();
            Servicio s = new Servicio();
            
            String nroPreFactura;
            String fechaExp;
            String fechaVenc;
            String tipoDoc;
            String nroDoc;
            String acudiente;
            String estudiante;
            String idServicio;
            double subtotal=0;
            double porcIva =0.19;
            double iva=0;
            double porcDesc=0;
            double descuento=0;
            double valorTotal=0;
            String estado;
            String observaciones;
            String vinculo;
            opciones = request.getParameter("opciones");
            if (opciones.equals("Crear")) {
                //nroFactura = request.getParameter("NroPreFactura");
                fechaExp = request.getParameter("fechaExp");
                fechaVenc = request.getParameter("fechaVenc");
                tipoDoc = request.getParameter("tipodoc");
                nroDoc = request.getParameter("identificacion");
                acudiente = request.getParameter("acudiente");
                estudiante = request.getParameter("estudiante");
                idServicio = request.getParameter("servicio");
                //subtotal = request.getParameter("subtotal");
                //porcIva = request.getParameter("porcIva");
                //iva = request.getParameter("iva");
                //porcDesc = request.getParameter("porcDesc");
                //descuento = request.getParameter("descuento");
                //valorTotal = request.getParameter("valortotal");
                estado = request.getParameter("estado");
                observaciones = request.getParameter("observaciones");
                
                //llena el sub total de acuerdo al servicio elegido
                List<Servicio> listaServicio = sDAO.ConsultarServicio(Integer.parseInt(idServicio));
                    for(Servicio ser:listaServicio)
                {
                    subtotal= ser.getValor();
                }
                
                //Asigna el descuento de acuerdo a si es empleado, estudiante, ...
                vinculo = request.getParameter("vinculo");
                if (vinculo.equals("Empleado")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Estudiante")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Egresado")) {
                    porcDesc = 0.15;
                }
                if (vinculo.equals("Externo")) {
                    porcDesc = 0;
                }
                descuento = subtotal * porcDesc;
                iva = subtotal * porcIva;
                valorTotal = subtotal - descuento + iva;
               // f.setNroFactura(Integer.parseInt(nroFactura));
                pf.setFechaExp(fechaExp);
                pf.setFechaVenc(fechaVenc);
                pf.setTipoDocumento(tipoDoc);
                pf.setIdentificacion(Integer.parseInt(nroDoc));
                pf.setAcudiente(acudiente);
                pf.setAlumno(estudiante);
                pf.setDescripcion(idServicio);
                pf.setSubtotal(subtotal);
                pf.setPorcIva(porcIva);
                pf.setIva(iva);
                pf.setPorcDescuento(porcDesc);
                pf.setDescuento(descuento);
                pf.setValorTotal(valorTotal);
                pf.setEstado(estado);
                pf.setObservaciones(observaciones);
                
                resultadoOperacion = pfDAO.GuardarPreactura(pf);
                request.getRequestDispatcher("consultas.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=PreFactura&perfil=" + tipoUsuario).forward(request, response);
            }
            
            if (opciones.equals("Actualizar")) {
                
                }
            if (opciones.equals("Convertir")) {
                FacturaDAO fDAO = new FacturaDAO();
                Factura f = new Factura();
                nroPreFactura = request.getParameter("NroFactura");
                fechaExp = request.getParameter("fechaExp");
                fechaVenc = request.getParameter("fechaVenc");
                tipoDoc = request.getParameter("tipodoc");
                nroDoc = request.getParameter("identificacion");
                acudiente = request.getParameter("acudiente");
                estudiante = request.getParameter("estudiante");
                idServicio = request.getParameter("servicio");
                //subtotal = request.getParameter("subtotal");
                //porcIva = request.getParameter("porcIva");
                //iva = request.getParameter("iva");
                //porcDesc = request.getParameter("porcDesc");
                //descuento = request.getParameter("descuento");
                //valorTotal = request.getParameter("valortotal");
                estado = request.getParameter("estado");
                observaciones = request.getParameter("observaciones");
                
                //llena el sub total de acuerdo al servicio elegido
                List<Servicio> listaServicio = sDAO.ConsultarServicio(Integer.parseInt(idServicio));
                    for(Servicio ser:listaServicio)
                {
                    subtotal= ser.getValor();
                }
                
                //Asigna el descuento de acuerdo a si es empleado, estudiante, ...
                vinculo = request.getParameter("vinculo");
                if (vinculo.equals("Empleado")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Estudiante")) {
                    porcDesc = 0.1;
                }
                if (vinculo.equals("Egresado")) {
                    porcDesc = 0.15;
                }
                if (vinculo.equals("Externo")) {
                    porcDesc = 0;
                }
                descuento = subtotal * porcDesc;
                iva = subtotal * porcIva;
                valorTotal = subtotal - descuento + iva;
               // f.setNroFactura(Integer.parseInt(nroFactura));
                f.setFechaExp(fechaExp);
                f.setFechaVenc(fechaVenc);
                f.setTipoDocumento(tipoDoc);
                f.setIdentificacion(Integer.parseInt(nroDoc));
                f.setAcudiente(acudiente);
                f.setAlumno(estudiante);
                f.setDescripcion(idServicio);
                f.setSubtotal(subtotal);
                f.setPorcIva(porcIva);
                f.setIva(iva);
                f.setPorcDescuento(porcDesc);
                f.setDescuento(descuento);
                f.setValorTotal(valorTotal);
                f.setEstado(estado);
                f.setObservaciones(observaciones);
                
                resultadoOperacion = fDAO.GuardarFactura(f);
                
                nroPreFactura = request.getParameter("NroPreFactura");
                pf.setNroPrefactura(Integer.parseInt(nroPreFactura));
                pf.setEstado("Aprobado");
                
                pfDAO.AprobarPreFactura(pf);
                
                
                request.getRequestDispatcher("consultas.jsp?resultadoOperacion=" + resultadoOperacion + "&procedencia=Factura&perfil=" + tipoUsuario).forward(request, response);
            }
            if (opciones.equals("Consultar")) {
                request.getRequestDispatcher("consultas.jsp?procedencia=PreFactura&perfil=" + tipoUsuario).forward(request, response);
            }
        }

        //el siguiente if valida que venga del formulario Recuperación Clave
        if (botonProcedenciaFormulario.equals("Recuperar Clave")) {
            EnvioCorreo envio = new EnvioCorreo();
            CorreoUsuario = request.getParameter("email");
            u.setCorreoUsuario(CorreoUsuario);
            Clave = uDAO.RecuperarClave(u);
            envio.RecuperarClave(CorreoUsuario, Clave);
            if (Clave.equals("")) {
                request.getRequestDispatcher("paginaDeErrores.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("formularioRecuperacion.jsp?resultadoOperacion=Correo Enviado&perfil=" + tipoUsuario).forward(request, response);
            }
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
