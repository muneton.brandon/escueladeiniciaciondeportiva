package controlador;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Docente;
import modelo.Estudiante;
import modelo.Factura;
import modelo.Prefactura;
import modelo.informes;
import persistencia.DocenteDAO;
import persistencia.EstudianteDAO;
import persistencia.FacturaDAO;
import persistencia.PrefacturaDAO;
import persistencia.informesDAO;

/**
 * esta clase contiene los certificados que se descargan en pdf, recibe el
 * reporte que desea descargar, lo procesa y lo entrega
 *
 * @author ecardona
 * @param botonProcedenciaFormulario
 * @return Excel
 * @since junio 2020
 * @version 1
 */
public class DescargaPDF extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String botonProcedenciaFormulario = request.getParameter("accion");
        String numeroFactura = request.getParameter("NroFactura");
        String numeroPreFactura = request.getParameter("NroPrefactura");
        OutputStream out = response.getOutputStream();
        java.util.Date fecha = new Date();
        String strDateFormat = "dd-MM-YYYY"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // La cadena de formato de fecha se pasa como un argumento al objeto 
        objSDF.format(fecha); // El formato de fecha se aplica a la fecha actual
        informesDAO inf;
        List<informes> lista;

        try {
            try {
                if (botonProcedenciaFormulario.equals("Generar Certificado Alumno")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=CertificadoAlumno.pdf");

                    String IdEstudiante = request.getParameter("idEstudiante");
                    inf = new informesDAO();
                    lista = inf.ConsultarEstudiante(IdEstudiante);

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    for (informes rg : lista) {
                        //Agregando imagen
                        Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                        imagenes.setAlignment(Element.ALIGN_CENTER);
                        imagenes.scaleToFit(300, 300);
                        documento.add(imagenes);

                        Paragraph par1 = new Paragraph();
                        //tipo de fuente y color para el título
                        Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                        //agregando frase al documento
                        par1.add(new Phrase(Chunk.NEWLINE));
                        par1.add(new Phrase(Chunk.NEWLINE));
                        par1.add(new Phrase(Chunk.NEWLINE));
                        par1.add(new Phrase("EL POLITÉCNICO COLOMBIANO JAIME ISAZA CADAVID", fontTitulo));
                        //centrando título
                        par1.setAlignment(Element.ALIGN_CENTER);
                        //salto de línea
                        par1.add(new Phrase(Chunk.NEWLINE));
                        par1.add(new Phrase(Chunk.NEWLINE));
                        par1.add(new Phrase(Chunk.NEWLINE));
                        //agregando parte 1 al documento
                        documento.add(par1);

                        Paragraph par2 = new Paragraph();
                        //tipo de fuente y color
                        Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                        //agregando frase al documento
                        par2.add(new Phrase("Institución de Educación Superior de caracter pública y departamental sujeta "
                                + "a inspección y vigilancia por parte del Ministerio de educación\n\n\n CERTIFICA QUE:\n\n\n\n\n"
                                + rg.getNombEstudiante() + " identificado con número de cédula " + rg.getIdentificacionEstudiante()
                                + "\n\n\n\n\na la fecha se encuentra matriculado en la Escuela de Iniciación Deportiva bajo la "
                                + "modalidad de " + rg.getNombreDeporte() + "-" + rg.getNombreCategoria() + "\n\n\n\n\n"
                                + "La anterior certificación se expide en la ciudad de Medellín el día: " + objSDF.format(fecha)
                                + "\n\n\n\n\n\n", fontDescripcion));

                        //centrando título
                        par2.setAlignment(Element.ALIGN_CENTER);
                        //salto de línea
                        par2.add(new Phrase(Chunk.NEWLINE));
                        par2.add(new Phrase(Chunk.NEWLINE));
                        par2.add(new Phrase(Chunk.NEWLINE));
                        par2.add(new Phrase(Chunk.NEWLINE));
                        par2.add(new Phrase(Chunk.NEWLINE));

                        //agregando parte 2 al documento
                        documento.add(par2);

                        Paragraph par3 = new Paragraph();
                        //tipo de fuente y color
                        Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                        //agregando frase al documento
                        par3.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                                + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                                + "NIT: 890980136-6\n"
                                + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                                + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                        //centrando título
                        par3.setAlignment(Element.ALIGN_CENTER);
                        //agregando parte 3 al documento
                        documento.add(par3);
                    }
                    //cerrando documento
                    documento.close();
                }

                //Factura ppal
                if (botonProcedenciaFormulario.equals("Imprimir Factura")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=Factura.pdf");

                    //Solicitamos la factura a FacturaDAO
                    FacturaDAO fDAO = new FacturaDAO();
                    Factura f = new Factura();
                    List<Factura> listaf = fDAO.ConsultarFactura(Integer.parseInt(numeroFactura));

                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase("FACTURA DE VENTA NRO. " + numeroFactura, fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 1 al documento
                    documento.add(par1);
                    
                    Paragraph par5 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescrip2 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par5.add(new Phrase("Autorizacion Numeracion de Facturacion "
                            + "No.18763004019942 de 2020/2/5 Rango de 1 A 8000. ", fontDescrip2));
                    //centrando título
                    par5.setAlignment(Element.ALIGN_CENTER);
                    par5.add(new Phrase(Chunk.NEWLINE));
                    par5.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 3 al documento
                    documento.add(par5);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(7);

                    //For de extraccion de datos de la lista de la base de datos de la factura especificada
                    String nroFactura = "";
                    String fechaExp = "";
                    String fechaVenc = "";
                    String identificacion = "";
                    String tipoDocumento = "";
                    String acudiente = "";
                    String descripcion = "";
                    String valorTotal = "";
                    String nombreEstudiante = "";
                    String estadoFactura = "";
                    String subtotal = "";
                    double iva=0;
                    String ivaValor = "";
                    double descuento = 0;
                    String descuentoValor = "";
                    String Observaciones = "";

                    for (Factura lf : listaf) {
                        nroFactura = String.valueOf(lf.getNroFactura());
                        fechaExp = lf.getFechaExp();
                        fechaVenc = lf.getFechaVenc();
                        identificacion = String.valueOf(lf.getIdentificacion());
                        acudiente = lf.getAcudiente();
                        descripcion = lf.getDescripcion();
                        valorTotal = String.valueOf(lf.getValorTotal());
                        tipoDocumento = lf.getTipoDocumento();
                        estadoFactura = lf.getEstado();
                        subtotal = String.valueOf(lf.getSubtotal());
                        iva = Math.round(lf.getPorcIva()*100);
                        ivaValor = String.valueOf(lf.getIva());
                        descuento = Math.round(lf.getPorcDescuento()*100);
                        descuentoValor = String.valueOf(lf.getDescuento());
                        Observaciones = lf.getObservaciones();
//                        nombreEstudiante = fDAO.consultarNombreEstudiante(Integer.parseInt(lf.getAlumno()));
//                        lf.setNombreAlumno(nombreEstudiante);
//                        nombreEstudiante = lf.getAlumno();

                    }

                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda0 = new PdfPCell(new Paragraph("Datos de Factura", FontFactory.getFont(//Numero de factura, el campo que muestra el dato es el la celda 7
                            "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda1 = new PdfPCell(new Paragraph("Numero de Factura", FontFactory.getFont(//Numero de factura, el campo que muestra el dato es el la celda 7
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Fecha de Vencimiento", FontFactory.getFont(//Fecha de Vencimiento, el campo que muestra el dato es la celda 9
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda4 = new PdfPCell(new Paragraph("Identificacion", FontFactory.getFont(//Identificacion, el campo que muestra el dato es la celda 10
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda7 = new PdfPCell(new Paragraph(nroFactura, FontFactory.getFont(//Celda 7 muestra en el pdf el numero de factura indicado en la celda 1
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));// Valor asociado a la aidentificacion
                    PdfPCell celda9 = new PdfPCell(new Paragraph(fechaVenc, FontFactory.getFont(//Celda 9 muestra en el pdf la fecha de vencimiento indicada en la celda 3
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda10 = new PdfPCell(new Paragraph(tipoDocumento + " " + identificacion, FontFactory.getFont(//Celda 10 muestra en el pdf la identifiion indicada en la celda 4
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda13 = new PdfPCell(new Paragraph("Fecha de Expedicion", FontFactory.getFont(//Fecha de expedicion, el campo que muestra el dato es el la celda 14
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda14 = new PdfPCell(new Paragraph(fechaExp, FontFactory.getFont(//Celda 14 muestra en el pdf el numero de factura indicado en la celda 13
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda15 = new PdfPCell(new Paragraph("Nombre Acudiente", FontFactory.getFont(//Nombre de Acudiente, el campo que muestra el dato es el la celda 16
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda16 = new PdfPCell(new Paragraph(acudiente, FontFactory.getFont(//Celda 16 muestra en el pdf el nombre de acudiente indicado en la celda 15
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));

                    PdfPCell celda21 = new PdfPCell(new Paragraph("Estado", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda22 = new PdfPCell(new Paragraph(estadoFactura, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    celda0.setColspan(7);
                    celda1.setColspan(2);
                    celda7.setColspan(2);
                    celda13.setColspan(2);
                    celda14.setColspan(1);
                    celda15.setColspan(2);
                    celda16.setColspan(2);
                    celda3.setColspan(2);
                    celda9.setColspan(1);
                    celda4.setColspan(2);
                    celda10.setColspan(2);
                    celda21.setColspan(1);
                    celda22.setColspan(2);
//                    celda11.setColspan(1);

                    //agregando celdas a la tabla       
                    tabla.addCell(celda0);
                    tabla.addCell(celda1); //Numero de factura
                    tabla.addCell(celda7);
                    tabla.addCell(celda13); //Fecha de Expedicion
                    tabla.addCell(celda14);
                    tabla.addCell(celda15); //Nombre Acudiente
                    tabla.addCell(celda16);
                    tabla.addCell(celda3); //Nombre Acudiente
                    tabla.addCell(celda9);
                    tabla.addCell(celda4); //Docuemnto de Identidad
                    tabla.addCell(celda10);
                    tabla.addCell(celda21); //referencia
                    tabla.addCell(celda22);

                    //agregando tabla al documento
                    documento.add(tabla);

                    Paragraph par2 = new Paragraph();
                    par2.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla2 = new PdfPTable(7);

                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda20 = new PdfPCell(new Paragraph("Datos Finacnieros de la Factura", FontFactory.getFont(
                            "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda23 = new PdfPCell(new Paragraph("Descripción ", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda24 = new PdfPCell(new Paragraph("Valor", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda25 = new PdfPCell(new Paragraph(descripcion, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda26 = new PdfPCell(new Paragraph(subtotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda27 = new PdfPCell(new Paragraph("Observaciones", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda28 = new PdfPCell(new Paragraph("Subtotal", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda29 = new PdfPCell(new Paragraph(subtotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda30 = new PdfPCell(new Paragraph(Observaciones, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda31 = new PdfPCell(new Paragraph("Descuento: %" + descuento, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda32 = new PdfPCell(new Paragraph(descuentoValor, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda33 = new PdfPCell(new Paragraph("Iva %" + iva, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda34 = new PdfPCell(new Paragraph(ivaValor, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda35 = new PdfPCell(new Paragraph("Total Factura", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda36 = new PdfPCell(new Paragraph(valorTotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));

                    celda20.setColspan(7);
                    celda23.setColspan(6);
                    celda25.setColspan(6);
                    celda27.setColspan(4);
                    celda28.setColspan(2);
                    celda29.setColspan(1);
                    celda30.setColspan(4);
                    celda30.setRowspan(4);
                    celda31.setColspan(2);
                    celda32.setColspan(1);
                    celda33.setColspan(2);
                    celda34.setColspan(1);
                    celda35.setColspan(2);
                    celda36.setColspan(1);

                    //agregando celdas a la tabla 
                    tabla2.addCell(celda20);
                    tabla2.addCell(celda23); //Mensaje Descripcion
                    tabla2.addCell(celda24); //Mensje de valor
                    tabla2.addCell(celda25);
                    tabla2.addCell(celda26);
                    tabla2.addCell(celda27);
                    tabla2.addCell(celda28);
                    tabla2.addCell(celda29);
                    tabla2.addCell(celda30);
                    tabla2.addCell(celda31);
                    tabla2.addCell(celda32);
                    tabla2.addCell(celda33);
                    tabla2.addCell(celda34);
                    tabla2.addCell(celda35);
                    tabla2.addCell(celda36);

                    //agregando tabla al documento
                    documento.add(tabla2);

                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescrip = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    par3.add(new Phrase(Chunk.NEWLINE));
                    //agregando frase al documento
                    par3.add(new Phrase("IVA Régimen común (Responsables del IVA), entidad educativa exenta de Electronica"
                            + "retención en la fuente, actividad económica xxx. CUFE: "
                            + "ca0185a392ea94fe7c95fbb19a77ac0006f101de6288d903dc18c70d6dce4632a37e40097dbf558701628b650595b8e8 "
                            + "Favor pagar con cheque a nombre de Politécnico Colombiano Jaime Isaza Cadavid; transferencia, "
                            + "consignación Bancolombia Ahorros xxxxxxxxxx o en nuestras oficinas. Favor informar su pago al "
                            + "teléfono (+57 4) 444 76 54 - (+57 4) 319 79 00 o al mail john_cardona82141@elpoli.edu.co "
                            + "GENERADA EL: 31/05/2020 10:39:19 p.m. EXPEDIDA DIAN: 1/06/2020 7:43:45 a.m. ", fontDescrip));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_JUSTIFIED);
                    par3.add(new Phrase(Chunk.NEWLINE));
                    par3.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 3 al documento
                    documento.add(par3);

                    Paragraph par4 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    par4.add(new Phrase(Chunk.NEWLINE));
                    //agregando frase al documento
                    par4.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par4.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par4);

                    //cerrando documento
                    documento.close();
                }

                //Impresión pre factura princpial
                if (botonProcedenciaFormulario.equals("Imprimir Pre-factura")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=PreFactura.pdf");

                    //Solicitamos la factura a FacturaDAO
                    PrefacturaDAO pfDAO = new PrefacturaDAO();
                    Prefactura pf = new Prefactura();
                    List<Prefactura> listapf = pfDAO.ConsultarPreFactura(Integer.parseInt(numeroPreFactura));

                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase("PRE-FACTURA NRO. " + numeroPreFactura, fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 1 al documento
                    documento.add(par1);
                    
                    Paragraph par5 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescrip2 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par5.add(new Phrase("Autorizacion Numeracion de Pre-Facturacion "
                            + "No.18763004019942 de 2020/2/5 Rango de 1 A 8000. ", fontDescrip2));
                    //centrando título
                    par5.setAlignment(Element.ALIGN_CENTER);
                    par5.add(new Phrase(Chunk.NEWLINE));
                    par5.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 3 al documento
                    documento.add(par5);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(7);

                    //For de extraccion de datos de la lista de la base de datos de la factura especificada
                    String nroPreFactura = "";
                    String fechaExp = "";
                    String fechaVenc = "";
                    String identificacion = "";
                    String tipoDocumento = "";
                    String acudiente = "";
                    String descripcion = "";
                    String valorTotal = "";
                    String nombreEstudiante = "";
                    String estadoFactura = "";
                    String subtotal = "";
                    double iva=0;
                    String ivaValor = "";
                    double descuento = 0;
                    String descuentoValor = "";
                    String Observaciones = "";

                    for (Prefactura lpf : listapf) {
                        nroPreFactura = String.valueOf(lpf.getNroPrefactura());
                        fechaExp = lpf.getFechaExp();
                        fechaVenc = lpf.getFechaVenc();
                        identificacion = String.valueOf(lpf.getIdentificacion());
                        acudiente = lpf.getAcudiente();
                        descripcion = lpf.getDescripcion();
                        valorTotal = String.valueOf(lpf.getValorTotal());
                        tipoDocumento = lpf.getTipoDocumento();
                        estadoFactura = lpf.getEstado();
                        subtotal = String.valueOf(lpf.getSubtotal());
                        iva = Math.round(lpf.getPorcIva()*100);
                        ivaValor = String.valueOf(lpf.getIva());
                        descuento = Math.round(lpf.getPorcDescuento()*100);
                        descuentoValor = String.valueOf(lpf.getDescuento());
                        Observaciones = lpf.getObservaciones();
//                        nombreEstudiante = fDAO.consultarNombreEstudiante(Integer.parseInt(lf.getAlumno()));
//                        lf.setNombreAlumno(nombreEstudiante);
//                        nombreEstudiante = lf.getAlumno();

                    }

                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda0 = new PdfPCell(new Paragraph("Datos de Pre-Factura", FontFactory.getFont(//Numero de factura, el campo que muestra el dato es el la celda 7
                            "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda1 = new PdfPCell(new Paragraph("Numero de Pre-Factura", FontFactory.getFont(//Numero de factura, el campo que muestra el dato es el la celda 7
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Fecha de Vencimiento", FontFactory.getFont(//Fecha de Vencimiento, el campo que muestra el dato es la celda 9
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda4 = new PdfPCell(new Paragraph("Identificacion", FontFactory.getFont(//Identificacion, el campo que muestra el dato es la celda 10
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda7 = new PdfPCell(new Paragraph(nroPreFactura, FontFactory.getFont(//Celda 7 muestra en el pdf el numero de factura indicado en la celda 1
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));// Valor asociado a la aidentificacion
                    PdfPCell celda9 = new PdfPCell(new Paragraph(fechaVenc, FontFactory.getFont(//Celda 9 muestra en el pdf la fecha de vencimiento indicada en la celda 3
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda10 = new PdfPCell(new Paragraph(tipoDocumento + " " + identificacion, FontFactory.getFont(//Celda 10 muestra en el pdf la identifiion indicada en la celda 4
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda13 = new PdfPCell(new Paragraph("Fecha de Expedicion", FontFactory.getFont(//Fecha de expedicion, el campo que muestra el dato es el la celda 14
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda14 = new PdfPCell(new Paragraph(fechaExp, FontFactory.getFont(//Celda 14 muestra en el pdf el numero de factura indicado en la celda 13
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda15 = new PdfPCell(new Paragraph("Nombre Acudiente", FontFactory.getFont(//Nombre de Acudiente, el campo que muestra el dato es el la celda 16
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda16 = new PdfPCell(new Paragraph(acudiente, FontFactory.getFont(//Celda 16 muestra en el pdf el nombre de acudiente indicado en la celda 15
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));

                    PdfPCell celda21 = new PdfPCell(new Paragraph("Estado", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda22 = new PdfPCell(new Paragraph(estadoFactura, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    celda0.setColspan(7);
                    celda1.setColspan(2);
                    celda7.setColspan(2);
                    celda13.setColspan(2);
                    celda14.setColspan(1);
                    celda15.setColspan(2);
                    celda16.setColspan(2);
                    celda3.setColspan(2);
                    celda9.setColspan(1);
                    celda4.setColspan(2);
                    celda10.setColspan(2);
                    celda21.setColspan(1);
                    celda22.setColspan(2);
//                    celda11.setColspan(1);

                    //agregando celdas a la tabla       
                    tabla.addCell(celda0);
                    tabla.addCell(celda1); //Numero de factura
                    tabla.addCell(celda7);
                    tabla.addCell(celda13); //Fecha de Expedicion
                    tabla.addCell(celda14);
                    tabla.addCell(celda15); //Nombre Acudiente
                    tabla.addCell(celda16);
                    tabla.addCell(celda3); //Nombre Acudiente
                    tabla.addCell(celda9);
                    tabla.addCell(celda4); //Docuemnto de Identidad
                    tabla.addCell(celda10);
                    tabla.addCell(celda21); //referencia
                    tabla.addCell(celda22);

                    //agregando tabla al documento
                    documento.add(tabla);

                    Paragraph par2 = new Paragraph();
                    par2.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla2 = new PdfPTable(7);

                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda20 = new PdfPCell(new Paragraph("Datos Finacnieros de la Pre-Factura", FontFactory.getFont(
                            "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda23 = new PdfPCell(new Paragraph("Descripción ", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda24 = new PdfPCell(new Paragraph("Valor", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda25 = new PdfPCell(new Paragraph(descripcion, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda26 = new PdfPCell(new Paragraph(subtotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda27 = new PdfPCell(new Paragraph("Observaciones", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda28 = new PdfPCell(new Paragraph("Subtotal", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda29 = new PdfPCell(new Paragraph(subtotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda30 = new PdfPCell(new Paragraph(Observaciones, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda31 = new PdfPCell(new Paragraph("Descuento: %" + descuento, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda32 = new PdfPCell(new Paragraph(descuentoValor, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda33 = new PdfPCell(new Paragraph("Iva %" + iva, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda34 = new PdfPCell(new Paragraph(ivaValor, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));
                    PdfPCell celda35 = new PdfPCell(new Paragraph("Total Factura", FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda36 = new PdfPCell(new Paragraph(valorTotal, FontFactory.getFont(
                            "TIMES_ROMAN", 10, Font.NORMAL, BaseColor.BLACK)));

                    celda20.setColspan(7);
                    celda23.setColspan(6);
                    celda25.setColspan(6);
                    celda27.setColspan(4);
                    celda28.setColspan(2);
                    celda29.setColspan(1);
                    celda30.setColspan(4);
                    celda30.setRowspan(4);
                    celda31.setColspan(2);
                    celda32.setColspan(1);
                    celda33.setColspan(2);
                    celda34.setColspan(1);
                    celda35.setColspan(2);
                    celda36.setColspan(1);

                    //agregando celdas a la tabla 
                    tabla2.addCell(celda20);
                    tabla2.addCell(celda23); //Mensaje Descripcion
                    tabla2.addCell(celda24); //Mensje de valor
                    tabla2.addCell(celda25);
                    tabla2.addCell(celda26);
                    tabla2.addCell(celda27);
                    tabla2.addCell(celda28);
                    tabla2.addCell(celda29);
                    tabla2.addCell(celda30);
                    tabla2.addCell(celda31);
                    tabla2.addCell(celda32);
                    tabla2.addCell(celda33);
                    tabla2.addCell(celda34);
                    tabla2.addCell(celda35);
                    tabla2.addCell(celda36);

                    //agregando tabla al documento
                    documento.add(tabla2);

                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescrip = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    par3.add(new Phrase(Chunk.NEWLINE));
                    //agregando frase al documento
                    par3.add(new Phrase("IVA Régimen común (Responsables del IVA), entidad educativa exenta de Electronica"
                            + "retención en la fuente, actividad económica xxx. CUFE: "
                            + "ca0185a392ea94fe7c95fbb19a77ac0006f101de6288d903dc18c70d6dce4632a37e40097dbf558701628b650595b8e8 "
                            + "Favor pagar con cheque a nombre de Politécnico Colombiano Jaime Isaza Cadavid; transferencia, "
                            + "consignación Bancolombia Ahorros xxxxxxxxxx o en nuestras oficinas. Favor informar su pago al "
                            + "teléfono (+57 4) 444 76 54 - (+57 4) 319 79 00 o al mail john_cardona82141@elpoli.edu.co "
                            + "GENERADA EL: 31/05/2020 10:39:19 p.m. EXPEDIDA DIAN: 1/06/2020 7:43:45 a.m. ", fontDescrip));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_JUSTIFIED);
                    par3.add(new Phrase(Chunk.NEWLINE));
                    par3.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 3 al documento
                    documento.add(par3);

                    Paragraph par4 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    par4.add(new Phrase(Chunk.NEWLINE));
                    //agregando frase al documento
                    par4.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par4.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par4);

                    //cerrando documento
                    documento.close();
                }

                //Listado de estudiantes
                if (botonProcedenciaFormulario.equals("ListadoEstudiantes")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=ListadoEstudiantes.pdf");

                    EstudianteDAO eDAO = new EstudianteDAO();
                    Estudiante e = new Estudiante();
                    List<Estudiante> listaEst = eDAO.ConsultarEstudiantes();

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase("LISTADO DE ESTUDIANTES", fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 1 al documento
                    documento.add(par1);

                    Paragraph par2 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par2.add(new Phrase("A continuación se relaciona el listado de estudiantes registrados en la BD"
                            + " a la fecha:", fontDescripcion));

                    //centrando título
                    par2.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));

                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(6);
                    
                    
                        
                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda1 = new PdfPCell(new Paragraph("Tipo de Ident", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda2 = new PdfPCell(new Paragraph("Nro. Doc", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Nombre Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda4 = new PdfPCell(new Paragraph("Fecha Nac", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda5 = new PdfPCell(new Paragraph("Acudiente", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda6 = new PdfPCell(new Paragraph("Paz y Salvo", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    
                    tabla.addCell(celda1);
                    tabla.addCell(celda2);
                    tabla.addCell(celda3);
                    tabla.addCell(celda4);
                    tabla.addCell(celda5);
                    tabla.addCell(celda6);
                    for(Estudiante es:listaEst) {
                        tabla.addCell(es.getTipoIdentificacionEstudiante());
                        tabla.addCell(String.valueOf(es.getIdentificacionEstudiante()));
                        tabla.addCell(es.getNombreEstudiante());
                        tabla.addCell(String.valueOf(es.getFechaNacimientoEstudiante()));
                        tabla.addCell(String.valueOf(es.getAcudienteEstudiante()));
                        tabla.addCell(es.getPazYSalvoEstudiante());
                     }
                    //agregando tabla al documento
                    documento.add(tabla);

                    
                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    //agregando frase al documento
                    par3.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par3);

                    //cerrando documento
                    documento.close();
                }
                
                //Listado de Docentes
                if (botonProcedenciaFormulario.equals("ListadoDocentes")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=ListadoDocentes.pdf");

                    DocenteDAO dDAO = new DocenteDAO();
                    Docente d = new Docente();
                    List<Docente> listaDoc = dDAO.ConsultarDocentes();

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase("LISTADO DE DOCENTES", fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 1 al documento
                    documento.add(par1);

                    Paragraph par2 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par2.add(new Phrase("A continuación se relaciona el listado de estudiantes registrados en la BD"
                            + " a la fecha:", fontDescripcion));

                    //centrando título
                    par2.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));

                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(6);
                    
                    
                        
                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda1 = new PdfPCell(new Paragraph("Tipo de Ident", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda2 = new PdfPCell(new Paragraph("Nro. Doc", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Nombre Docente", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda4 = new PdfPCell(new Paragraph("Fecha Nac", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda5 = new PdfPCell(new Paragraph("Estudios", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda6 = new PdfPCell(new Paragraph("Estado", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    
                    tabla.addCell(celda1);
                    tabla.addCell(celda2);
                    tabla.addCell(celda3);
                    tabla.addCell(celda4);
                    tabla.addCell(celda5);
                    tabla.addCell(celda6);
                    
                    for(Docente doc:listaDoc) {
                        tabla.addCell(doc.getTipoDocumento());
                        tabla.addCell(String.valueOf(doc.getIdentificacionDocente()));
                        tabla.addCell(doc.getNombreDocente());
                        tabla.addCell(String.valueOf(doc.getFechaNacimientoDocente()));
                        tabla.addCell(doc.getEstudiosDocente());
                        tabla.addCell(doc.getEstadoDocente());
                     }
                    //agregando tabla al documento
                    documento.add(tabla);

                    
                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    //agregando frase al documento
                    par3.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par3);

                    //cerrando documento
                    documento.close();
                }
                
                
                //Impresión notas finales en pdf
                if (botonProcedenciaFormulario.equals("Imprimir Notas Finales")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=ReporteNotas.pdf");
                    
                    String idGrupo = request.getParameter("idGrupo");
                    informesDAO iDAO = new informesDAO();
                    informes info = new informes();
                    List<informes> listaGrupos = iDAO.ConsultarGrupoNotas(idGrupo);

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase("REPORTE DE NOTAS GRUPO " + idGrupo, fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 1 al documento
                    documento.add(par1);

                    Paragraph par2 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par2.add(new Phrase("A continuación, el reporte de notas...", fontDescripcion));

                    //centrando título
                    par2.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));

                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(3);
                    
                    
                        
                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda1 = new PdfPCell(new Paragraph("id Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda2 = new PdfPCell(new Paragraph("Nombre Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Nota final", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    
                    tabla.addCell(celda1);
                    tabla.addCell(celda2);
                    tabla.addCell(celda3);

                    
                    for(informes infor:listaGrupos) {
                        tabla.addCell(String.valueOf(infor.getIdentificacionEstudiante()));
                        tabla.addCell(infor.getNombEstudiante());
                        tabla.addCell(String.valueOf(infor.getNotaFinalEstudiante()));
                     }
                    //agregando tabla al documento
                    documento.add(tabla);

                    
                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    //agregando frase al documento
                    par3.add(new Phrase("Politécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par3);

                    //cerrando documento
                    documento.close();
                }
                
                
                //Impresión Estudiantes que Ganaron
                if (botonProcedenciaFormulario.equals("Imprimir Notas Estudiantes que Ganaron")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=ReporteNotas.pdf");
                    
                    String idGrupo = request.getParameter("idGrupo");
                    informesDAO iDAO = new informesDAO();
                    informes info = new informes();
                    List<informes> listaGrupos = iDAO.ConsultarEstudiantesGanaron(idGrupo);

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase("REPORTE DE ESTUDIANTES QUE GANARON DEL GRUPO " + idGrupo, fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 1 al documento
                    documento.add(par1);

                    Paragraph par2 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par2.add(new Phrase("A continuación, el reporte de estudiantes que ganaron...", fontDescripcion));

                    //centrando título
                    par2.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));

                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(3);
                    
                    
                        
                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda1 = new PdfPCell(new Paragraph("id Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda2 = new PdfPCell(new Paragraph("Nombre Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Nota final", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    
                    tabla.addCell(celda1);
                    tabla.addCell(celda2);
                    tabla.addCell(celda3);

                    
                    for(informes infor:listaGrupos) {
                        tabla.addCell(String.valueOf(infor.getIdentificacionEstudiante()));
                        tabla.addCell(infor.getNombEstudiante());
                        tabla.addCell(String.valueOf(infor.getNotaFinalEstudiante()));
                     }
                    //agregando tabla al documento
                    documento.add(tabla);

                    
                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    //agregando frase al documento
                    par3.add(new Phrase("\n\nPolitécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par3);

                    //cerrando documento
                    documento.close();
                }
                
                
                //Impresión Estudiantes que Perdienron
                if (botonProcedenciaFormulario.equals("Imprimir Notas Estudiantes que Perdieron")) {
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "filename=ReporteNotas.pdf");
                    
                    String idGrupo = request.getParameter("idGrupo");
                    informesDAO iDAO = new informesDAO();
                    informes info = new informes();
                    List<informes> listaGrupos = iDAO.ConsultarEstudiantesPerdieron(idGrupo);

                    //creando documento
                    Document documento = new Document();
                    PdfWriter.getInstance(documento, out);

                    //abrimos documento y llenamos de contenido
                    documento.open();

                    
                    //Agregando imagen
                    Image imagenes = Image.getInstance("https://www.politecnicojic.edu.co/images/logo/logo-negro.png");
                    imagenes.setAlignment(Element.ALIGN_CENTER);
                    imagenes.scaleToFit(300, 300);
                    documento.add(imagenes);

                    Paragraph par1 = new Paragraph();
                    //tipo de fuente y color para el título
                    Font fontTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);
                    //agregando frase al documento
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase("REPORTE DE ESTUDIANTES QUE PERDIERON DEL GRUPO " + idGrupo, fontTitulo));
                    //centrando título
                    par1.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    par1.add(new Phrase(Chunk.NEWLINE));
                    //agregando parte 1 al documento
                    documento.add(par1);

                    Paragraph par2 = new Paragraph();
                    //tipo de fuente y color
                    Font fontDescripcion = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.BLACK);
                    //agregando frase al documento
                    par2.add(new Phrase("A continuación, el reporte de estudiantes que perdieron...", fontDescripcion));

                    //centrando título
                    par2.setAlignment(Element.ALIGN_CENTER);
                    //salto de línea
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));
                    par2.add(new Phrase(Chunk.NEWLINE));

                    //agregando parte 2 al documento
                    documento.add(par2);

                    //Creando tabla y definiendo el número de columnas
                    PdfPTable tabla = new PdfPTable(3);
                    
                    
                        
                    //Creando celda y definiendo nombre de la celda
                    PdfPCell celda1 = new PdfPCell(new Paragraph("id Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda2 = new PdfPCell(new Paragraph("Nombre Estudiante", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    PdfPCell celda3 = new PdfPCell(new Paragraph("Nota final", FontFactory.getFont(
                        "TIMES_ROMAN", 12, Font.BOLD, BaseColor.BLACK)));
                    
                    tabla.addCell(celda1);
                    tabla.addCell(celda2);
                    tabla.addCell(celda3);

                    
                    for(informes infor:listaGrupos) {
                        tabla.addCell(String.valueOf(infor.getIdentificacionEstudiante()));
                        tabla.addCell(infor.getNombEstudiante());
                        tabla.addCell(String.valueOf(infor.getNotaFinalEstudiante()));
                     }
                    //agregando tabla al documento
                    documento.add(tabla);

                    
                    Paragraph par3 = new Paragraph();
                    //tipo de fuente y color
                    Font fontPiePagina = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.ITALIC, BaseColor.BLACK);
                    //agregando frase al documento
                    par3.add(new Phrase("\n\nPolitécnico Colombiano Jaime Isaza Cadavid © 2020 \n"
                            + "Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |\n"
                            + "NIT: 890980136-6\n"
                            + "Institución de Educación Superior de caracter pública y departamental sujeta a"
                            + "inspección y vigilancia por parte del Ministerio de Educación.", fontPiePagina));
                    //centrando título
                    par3.setAlignment(Element.ALIGN_CENTER);
                    //agregando parte 3 al documento
                    documento.add(par3);

                    //cerrando documento
                    documento.close();
                }
                
                

            } catch (Exception ex) {
                ex.getMessage();
            }

        } finally {
            out.close();
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
