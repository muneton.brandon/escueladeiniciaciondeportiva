
package controlador;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//import javax.swing.JOptionPane;

/**
* el método EnvioCorreo recibe el correo y la clave
* y los envíe mediante un correo de GMAIL.
* @param CorreoDestinario
* @param Clave
* @since Abril 2020
* @version 1
* @author ecardona
*/ 
public class EnvioCorreo {
    
    public void RecuperarClave(String correoDestinatario, String Clave){
        Properties propiedad = new Properties();
        propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
        propiedad.setProperty("mail.smtp.starttls.enable", "true");
        propiedad.setProperty("mail.smtp.port", "587");
        propiedad.setProperty("mail.smtp.auth", "true");
        
        Session sesion = Session.getDefaultInstance(propiedad);
        
        String correoEnvia = "proyectocsweb@gmail.com";
        String contrasena = "3ZSjCYMI66pF";
        String destinatario = correoDestinatario;
        String asunto = "Recordatorio de clave: Escuela de iniciación deportiva P.C.J.I.C.";
        String mensaje = "Su clave es: " + Clave;
        
        MimeMessage mail = new MimeMessage(sesion);
        
        try {
            mail.setFrom(new InternetAddress (correoEnvia));
            mail.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            mail.setSubject(asunto);
            mail.setText(mensaje);
            
            Transport transporte = sesion.getTransport("smtp");
            transporte.connect(correoEnvia,contrasena);
            transporte.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
            transporte.close();
            
//            JOptionPane.showMessageDialog(null, "Correo enviado");

        } catch (AddressException ex) {
            Logger.getLogger(EnvioCorreo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(EnvioCorreo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
