
package controlador;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistencia.EstudianteDAO;


/**
 * La clase CargueArchivo recibe un archivo, lo lee y lo procesa en la bd) 
 * @param Archivo
 * @since abril 2020
 * @version 1
 * @author ecardona
 */
public class CargueArchivo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String contentType = request.getContentType();
        
        if ((contentType != null) && (contentType.contains("multipart/form-data"))) {
            DataInputStream in = new DataInputStream(request.getInputStream());
            
            /**
             * Obtenermos la longitud y el contenido del tipo de datos (Content type)
             */
            int formDataLength = request.getContentLength(); //Longitud de datos desde el formulario
            byte dataBytes[] = new byte[formDataLength]; //Guardar los bytes obtenidos
            int byteRead = 0; //Contador
            int totalBytesRead = 0; //Contador
            
            /**
             * Convertir el archivo cargado a bytes
             */
            while (totalBytesRead < formDataLength) {
                byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                totalBytesRead += byteRead;
            }
            
            String file = new String(dataBytes); //Convertir los bytes leidos a cadena
            String contenido = file.substring(file.indexOf("text/plain") + 12); //Obtener el contenido sin cabeceras
            String[] lineas = contenido.split("\\t"); //Quitar las tabulaciones y guardar los contenidos en un arreglo
            
            
            try (PrintWriter out = response.getWriter()) {
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("</head>");
                    out.println("<body>");
                    String resultadoOperacion="";
                    
                    EstudianteDAO eDAO = new EstudianteDAO();
                for(int i=1;i<lineas.length-1;i=i+2){
 
                    resultadoOperacion = eDAO.ActualizarPazYSalvoEstudiante(lineas[i], lineas[i+1]);

                    out.println("<p>id Estu: " + lineas[i]);
                    out.println("<p>Paz y Salvo: " + lineas[i+1]);
                    out.println("<p>Resultado: " + resultadoOperacion);
                    
                }
                    out.println("</body>");
                    out.println("</html>");
            }
            //resultadoOperacion = eDAO.ActualizarEstudiante(e);
            //request.getRequestDispatcher("formulario.jsp?resultadoOperacion="+resultadoOperacion+"&procedencia=estudiante").forward(request, response);
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
