
package modelo;

import java.sql.Date;

/**
 * La clase seguimientoClaseAClase administra los atributos de los 
 * seguimientos de las clases, tales como fecha de la clase, nota
 * de la clase, objetivo y observaciones de la clase, entre otros.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class seguimientoClaseAClase {
    int idGrupo;
    int idEstudiante;
    Date fechaClaseControl;
    String objetivosClaseControl;
    float notaClaseControl;
    String obervClaseControl;

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Date getFechaClaseControl() {
        return fechaClaseControl;
    }

    public void setFechaClaseControl(Date fechaClaseControl) {
        this.fechaClaseControl = fechaClaseControl;
    }

    public String getObjetivosClaseControl() {
        return objetivosClaseControl;
    }

    public void setObjetivosClaseControl(String objetivosClaseControl) {
        this.objetivosClaseControl = objetivosClaseControl;
    }

    public float getNotaClaseControl() {
        return notaClaseControl;
    }

    public void setNotaClaseControl(float notaClaseControl) {
        this.notaClaseControl = notaClaseControl;
    }

    public String getObervClaseControl() {
        return obervClaseControl;
    }

    public void setObervClaseControl(String obervClaseControl) {
        this.obervClaseControl = obervClaseControl;
    }
    
    
    
    
}
