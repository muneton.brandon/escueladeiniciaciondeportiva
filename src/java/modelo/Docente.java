
package modelo;

import java.sql.Date;

/**
 * La clase Docente administra los atributos de los docentes,
 * tales como su nombre, direccion, telefono, etc.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Docente {
    String tipoDocumento;
    int identificacionDocente;
    String nombreDocente;
    Date fechaNacimientoDocente;
    String direccionDocente;
    String telefonoDocente;
    String correoDocente;
    String estudiosDocente;
    String generoDocente;
    String estadoDocente;

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getIdentificacionDocente() {
        return identificacionDocente;
    }

    public void setIdentificacionDocente(int identificacionDocente) {
        this.identificacionDocente = identificacionDocente;
    }

    public String getNombreDocente() {
        return nombreDocente;
    }

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public Date getFechaNacimientoDocente() {
        return fechaNacimientoDocente;
    }

    public void setFechaNacimientoDocente(Date fechaNacimientoDocente) {
        this.fechaNacimientoDocente = fechaNacimientoDocente;
    }

    public String getDireccionDocente() {
        return direccionDocente;
    }

    public void setDireccionDocente(String direccionDocente) {
        this.direccionDocente = direccionDocente;
    }

    public String getTelefonoDocente() {
        return telefonoDocente;
    }

    public void setTelefonoDocente(String telefonoDocente) {
        this.telefonoDocente = telefonoDocente;
    }

    public String getCorreoDocente() {
        return correoDocente;
    }

    public void setCorreoDocente(String correoDocente) {
        this.correoDocente = correoDocente;
    }

    public String getEstudiosDocente() {
        return estudiosDocente;
    }

    public void setEstudiosDocente(String estudiosDocente) {
        this.estudiosDocente = estudiosDocente;
    }

    public String getGeneroDocente() {
        return generoDocente;
    }

    public void setGeneroDocente(String generoDocente) {
        this.generoDocente = generoDocente;
    }

    public String getEstadoDocente() {
        return estadoDocente;
    }

    public void setEstadoDocente(String estadoDocente) {
        this.estadoDocente = estadoDocente;
    }
    
    
}
