
package modelo;

/**
 * La clase Usuario administra los atributos de los usuarios,
 * tales como correo, clave, identificacion y tipo de usuario
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Usuario {
    String CorreoUsuario;
    String Clave;
    String identificacionDocente;
    String TipoUsuario;

    public String getCorreoUsuario() {
        return CorreoUsuario;
    }

    public String getIdentificacionDocente() {
        return identificacionDocente;
    }

    public void setIdentificacionDocente(String identificacionDocente) {
        this.identificacionDocente = identificacionDocente;
    }

    public void setCorreoUsuario(String CorreoUsuario) {
        this.CorreoUsuario = CorreoUsuario;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getTipoUsuario() {
        return TipoUsuario;
    }

    public void setTipoUsuario(String TipoUsuario) {
        this.TipoUsuario = TipoUsuario;
    }

    
    
}
