
package modelo;

import java.sql.Date;

/**
 * La clase Estudiante administra los atributos de los estudiantes,
 * tales como su nombre, direccion, telefono, etc.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Estudiante {
    String tipoIdentificacionEstudiante;
    int identificacionEstudiante;
    String nombreEstudiante;
    Date fechaNacimientoEstudiante;
    String direccionEstudiante;
    String telefonoEstudiante;
    String generoEstudiante;
    String estadoEstudiante;
    int acudienteEstudiante;
    String pazYSalvoEstudiante;

    public String getTipoIdentificacionEstudiante() {
        return tipoIdentificacionEstudiante;
    }

    public void setTipoIdentificacionEstudiante(String tipoIdentificacionEstudiante) {
        this.tipoIdentificacionEstudiante = tipoIdentificacionEstudiante;
    }

    public int getIdentificacionEstudiante() {
        return identificacionEstudiante;
    }

    public void setIdentificacionEstudiante(int identificacionEstudiante) {
        this.identificacionEstudiante = identificacionEstudiante;
    }

    public String getNombreEstudiante() {
        return nombreEstudiante;
    }

    public void setNombreEstudiante(String nombreEstudiante) {
        this.nombreEstudiante = nombreEstudiante;
    }

    public Date getFechaNacimientoEstudiante() {
        return fechaNacimientoEstudiante;
    }

    public void setFechaNacimientoEstudiante(Date fechaNacimientoEstudiante) {
        this.fechaNacimientoEstudiante = fechaNacimientoEstudiante;
    }

    public String getDireccionEstudiante() {
        return direccionEstudiante;
    }

    public void setDireccionEstudiante(String direccionEstudiante) {
        this.direccionEstudiante = direccionEstudiante;
    }

    public String getTelefonoEstudiante() {
        return telefonoEstudiante;
    }

    public void setTelefonoEstudiante(String telefonoEstudiante) {
        this.telefonoEstudiante = telefonoEstudiante;
    }

    public String getGeneroEstudiante() {
        return generoEstudiante;
    }

    public void setGeneroEstudiante(String generoEstudiante) {
        this.generoEstudiante = generoEstudiante;
    }

    public String getEstadoEstudiante() {
        return estadoEstudiante;
    }

    public void setEstadoEstudiante(String estadoEstudiante) {
        this.estadoEstudiante = estadoEstudiante;
    }

    public int getAcudienteEstudiante() {
        return acudienteEstudiante;
    }

    public void setAcudienteEstudiante(int acudienteEstudiante) {
        this.acudienteEstudiante = acudienteEstudiante;
    }

    public String getPazYSalvoEstudiante() {
        return pazYSalvoEstudiante;
    }

    public void setPazYSalvoEstudiante(String pazYSalvoEstudiante) {
        this.pazYSalvoEstudiante = pazYSalvoEstudiante;
    }
    
    
}
