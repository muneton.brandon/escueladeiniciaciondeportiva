/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ecardona
 */
public class informes {
    String nombreCategoria;
    String nombreDeporte;
    int idGrupo;
    String objetivoGrupo;
    String nombDocente;
    int identificacionEstudiante;
    String nombEstudiante;
    float notaFinalEstudiante;
    String pazYSalvo;
    int idCategoria;
    int idDocente;
    int iddeporte;

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public int getIddeporte() {
        return iddeporte;
    }

    public void setIddeporte(int iddeporte) {
        this.iddeporte = iddeporte;
    }

    public String getPazYSalvo() {
        return pazYSalvo;
    }

    public void setPazYSalvo(String pazYSalvo) {
        this.pazYSalvo = pazYSalvo;
    }

    public String getNombEstudiante() {
        return nombEstudiante;
    }

    public void setNombEstudiante(String nombEstudiante) {
        this.nombEstudiante = nombEstudiante;
    }

    public String getNombDocente() {
        return nombDocente;
    }

    public void setNombDocente(String nombDocente) {
        this.nombDocente = nombDocente;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getNombreDeporte() {
        return nombreDeporte;
    }

    public void setNombreDeporte(String nombreDeporte) {
        this.nombreDeporte = nombreDeporte;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getObjetivoGrupo() {
        return objetivoGrupo;
    }

    public void setObjetivoGrupo(String objetivoGrupo) {
        this.objetivoGrupo = objetivoGrupo;
    }

    public int getIdentificacionEstudiante() {
        return identificacionEstudiante;
    }

    public void setIdentificacionEstudiante(int identificacionEstudiante) {
        this.identificacionEstudiante = identificacionEstudiante;
    }

    public float getNotaFinalEstudiante() {
        return notaFinalEstudiante;
    }

    public void setNotaFinalEstudiante(float notaFinalEstudiante) {
        this.notaFinalEstudiante = notaFinalEstudiante;
    }   
}
