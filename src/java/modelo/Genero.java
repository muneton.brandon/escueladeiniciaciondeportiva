
package modelo;

/**
 * La clase Genero administra los atributos de los generos,
 * tales como id del genero y descripcion del estado
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Genero {
    String idGenero;
    String descripcionGenero;

    public String getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(String idGenero) {
        this.idGenero = idGenero;
    }

    public String getDescripcionGenero() {
        return descripcionGenero;
    }

    public void setDescripcionGenero(String descripcionGenero) {
        this.descripcionGenero = descripcionGenero;
    }
}
