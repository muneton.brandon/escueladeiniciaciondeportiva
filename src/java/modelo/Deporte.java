
package modelo;

/**
 * La clase Deporte administra los atributos de los deportes,
 * tales como id del deporte y descripcion del deporte.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Deporte {
    int idDeporte;
    String descripcionDeporte;

    public int getIdDeporte() {
        return idDeporte;
    }

    public void setIdDeporte(int idDeporte) {
        this.idDeporte = idDeporte;
    }

    public String getDescripcionDeporte() {
        return descripcionDeporte;
    }

    public void setDescripcionDeporte(String descripcionDeporte) {
        this.descripcionDeporte = descripcionDeporte;
    }
    
    
}
