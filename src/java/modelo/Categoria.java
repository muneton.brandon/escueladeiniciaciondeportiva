
package modelo;

/**
 * La clase Categoria administra los atributos de las categorias,
 * tales como id de la categoria y descripcion de la categoria
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Categoria {
    int idCategoria;
    String descripcionCategoria;

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }
}
