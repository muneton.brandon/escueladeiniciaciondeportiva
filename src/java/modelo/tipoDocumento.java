
package modelo;

/**
 * La clase tipoDocumento administra los atributos de los tipos de Documentos
 * de las personas (estudiantes, docentes, acudientes, ...), tales como 
 * id del documento y nombre del documento.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class tipoDocumento {
    String idDocumento;
    String nombreDocumento;

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }
}
