
package modelo;

/**
 * La clase DetalleGrupo administra los atributosl detalle de los grupos, 
 * es decir, los estudiantes que estan matriculados en ese curso
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class DetalleGrupo {
    int idGrupo;
    int idEstudiante;
    float notaFinalEst;

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public float getNotaFinalEst() {
        return notaFinalEst;
    }

    public void setNotaFinalEst(float notaFinalEst) {
        this.notaFinalEst = notaFinalEst;
    }
    
}
