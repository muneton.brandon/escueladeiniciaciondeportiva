
package modelo;

/**
 * La clase Estado administra los atributos de los estados,
 * tales como id del estado y nombre del estado
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Estado {
    int id;
    String nombreEstado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
}
