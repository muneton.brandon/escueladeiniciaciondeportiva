
package modelo;

import java.sql.Date;

/**
 * La clase Acudiente administra los atributos de los Acudientes,
 * tales como nombre, direccion, telefono, ...
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Acudiente {
    String tipoDocumentoAcudiente;
    int identificacionAcudiente;
    String nombreAcudiente;
    Date fechaNacimientoAcudiente;
    String direccionAcudiente;
    String telefonoAcudiente;
    String generoAcudiente;
    String vinculoInstitucionAcudiente;

    public String getTipoDocumentoAcudiente() {
        return tipoDocumentoAcudiente;
    }

    public void setTipoDocumentoAcudiente(String tipoDocumentoAcudiente) {
        this.tipoDocumentoAcudiente = tipoDocumentoAcudiente;
    }

    public int getIdentificacionAcudiente() {
        return identificacionAcudiente;
    }

    public void setIdentificacionAcudiente(int identificacionAcudiente) {
        this.identificacionAcudiente = identificacionAcudiente;
    }

    public String getNombreAcudiente() {
        return nombreAcudiente;
    }

    public void setNombreAcudiente(String nombreAcudiente) {
        this.nombreAcudiente = nombreAcudiente;
    }

    public Date getFechaNacimientoAcudiente() {
        return fechaNacimientoAcudiente;
    }

    public void setFechaNacimientoAcudiente(Date fechaNacimientoAcudiente) {
        this.fechaNacimientoAcudiente = fechaNacimientoAcudiente;
    }

    public String getDireccionAcudiente() {
        return direccionAcudiente;
    }

    public void setDireccionAcudiente(String direccionAcudiente) {
        this.direccionAcudiente = direccionAcudiente;
    }

    public String getTelefonoAcudiente() {
        return telefonoAcudiente;
    }

    public void setTelefonoAcudiente(String telefonoAcudiente) {
        this.telefonoAcudiente = telefonoAcudiente;
    }

    public String getGeneroAcudiente() {
        return generoAcudiente;
    }

    public void setGeneroAcudiente(String generoAcudiente) {
        this.generoAcudiente = generoAcudiente;
    }

    public String getVinculoInstitucionAcudiente() {
        return vinculoInstitucionAcudiente;
    }

    public void setVinculoInstitucionAcudiente(String vinculoInstitucionAcudiente) {
        this.vinculoInstitucionAcudiente = vinculoInstitucionAcudiente;
    }
}
