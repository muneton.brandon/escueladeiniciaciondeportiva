
package modelo;


import java.sql.Date;
import java.sql.Time;
/**
 * La clase Grupo administra los atributos de los Grupos,
 * tales como id del grupo, deporte y categoria de ese grupo,
 * horario inicio, fin y objetivo de éste.
 * @since marzo 2020
 * @version 1
 * @author ecardona
 */
public class Grupo {
    int idGrupo;
    int idDeporte;
    int idCategoria;
    String horarioInicio;
    String horarioFin;
    String objetivo;
    int idDocente;

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdDeporte() {
        return idDeporte;
    }

    public void setIdDeporte(int idDeporte) {
        this.idDeporte = idDeporte;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getHorarioInicio() {
        return horarioInicio;
    }

    public void setHorarioInicio(String horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public String getHorarioFin() {
        return horarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        this.horarioFin = horarioFin;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }
    
    
}
