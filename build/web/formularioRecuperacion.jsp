<%-- 
    Document   : formularioRecuperacion
    Created on : 15/03/2020, 03:42:10 PM
    Author     : ecardona
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--<link href="css/estilosFormularios.css" rel="stylesheet" type="text/css"/>-->
        <title>Recuperación de clave</title>
    </head>
    <body>
        <%String resultadoOperacion=request.getParameter("resultadoOperacion");%>
        <div class=" container" >
        <div class="row">
            <div class="col-12 badge-success fixed-top ">
                <p></p>
            </div>
            <div class="col-12 badge-success fixed-bottom ">
                <p></p>
            </div>
            <div class="container " >
                <div class="row justify-content-center ">
                    <div class="col-4 border ">
                        <div class="text-center "><br>
                            <img class=" w-75 justify-content-center " src="img/logo.png"><br><br>
                            <h5>ESCUELA DE INICIACION DEPORTIVA</h5> <br>
                            <h7>Recuperar Clave</h7>
                        </div>
                    <form class="form" name="Recuperacion" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Bienvenidos al sistema</strong></p>
                        </div>
                       <div class="form-group">
                            <label>Correo:</label>
                            <input class="form-control" type="email" name="email" placeholder="Ingrese su correo" required="true">
                       </div>  
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Recuperar Clave">    
                    </form>
                        <a href="index.jsp" style="color: gray"> <font size="2"><i>Regresar</font> </a>
                    </div>
                </div>
            <p class="mt-2 mb-3 text-muted text-center">&copy;
                    Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                    Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4) 319 79 00 |
                    NIT: 890980136-6<br>
                    Institución de Educación Superior de caracter pública y departamental sujeta a inspección y
                    vigilancia
                    por parte del Ministerio de Educación.
                <img src="img/icontec.png" width="70px">
            </p>
            </div>
        </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
