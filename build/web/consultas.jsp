<%-- 
    Document   : consultas
    Created on : 12/03/2020, 11:10:25 AM
    Author     : ecardona
--%>
<%@page import="modelo.Prefactura"%>
<%@page import="persistencia.PrefacturaDAO"%>
<%@page import="modelo.Factura"%>
<%@page import="persistencia.FacturaDAO"%>
<%@page import="modelo.seguimientoClaseAClase"%>
<%@page import="persistencia.seguimientoClaseAClaseDAO"%>
<%@page import="modelo.DetalleGrupo"%>
<%@page import="persistencia.DetalleGrupoDAO"%>
<%@page import="java.sql.Time"%>
<%@page import="modelo.Estudiante"%>
<%@page import="persistencia.EstudianteDAO"%>
<%@page import="modelo.Acudiente"%>
<%@page import="persistencia.AcudienteDAO"%>
<%@page import="java.sql.Date"%>
<%@page import="modelo.Docente"%>
<%@page import="persistencia.DocenteDAO"%>
<%@page import="modelo.Deporte"%>
<%@page import="persistencia.DeporteDAO"%>
<%@page import="modelo.Categoria"%>
<%@page import="persistencia.CategoriaDAO"%>
<%@page import="modelo.Genero"%>
<%@page import="persistencia.GeneroDAO"%>
<%@page import="modelo.Grupo"%>
<%@page import="persistencia.GrupoDAO"%>
<%@page import="modelo.Estado"%>
<%@page import="persistencia.EstadoDAO"%>
<%@page import="modelo.tipoDocumento"%>
<%@page import="persistencia.tipoDocumentoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import='persistencia.UsuarioDAO'%>
<%@ page import='modelo.Usuario'%>
<%@page import="java.util.List"%>
<%
    String procedencia=request.getParameter("procedencia");
    String perfil=request.getParameter("perfil");
%>
<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://kit.fontawesome.com/f9c76b2b8b.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
              crossorigin="anonymous">
        <title>Consulta de datos</title>
    </head>
    <body>
        
<%if ("Admin".equals(perfil)){%>
  
        <nav class="navbar navbar-dark bg-success">
        <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
        <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
        <ul class="nav nav-pills">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Administración</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=usuarios&perfil=${tipoUsuario}" style="color: black">
                    Usuarios</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">
                    Acudientes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">
                    Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=docente&perfil=${tipoUsuario}" style="color: black">
                    Docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?resultadoOperacion=En linea&procedencia=pazYSalvo&perfil=
                   ${tipoUsuario}" style="color: black">Cargue Masivo Paz y Salvo</a>
                </div>
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Matriculas</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">
                      Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">
                      Detalle Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">
                      Seguimiento</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Informes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">
                    Resumen general de grupos</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">
                    Top 10 mejores estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">
                    Paz y salvo Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 4</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 5</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Reportes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=Cerficado&perfil=${tipoUsuario}" style="color: black">Certificado de Matrícula estudiante</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoEstudiantes" style="color: black">Listado de estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoDocentes" style="color: black">Listado de docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupo&perfil=${tipoUsuario}" style="color: black">Reporte de notas final</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoGanaron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Ganaron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoPerdieron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Perdieron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirFactura&perfil=${tipoUsuario}" style="color: black">Imprimir factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirPrefactura&perfil=${tipoUsuario}" style="color: black">Imprimir pre-factura</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Facturación</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=Factura&perfil=${tipoUsuario}" style="color: black">Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=FacturaAnulada&perfil=${tipoUsuario}" style="color: black">Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFactura&perfil=${tipoUsuario}" style="color: black">Pre Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAnulada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAprobada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Aprobadas</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Tablas</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=deporte&perfil=${tipoUsuario}" style="color: black">
                    Deportes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=categoria&perfil=${tipoUsuario}" style="color: black">
                    Categorias</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=tipoDocumento&perfil=${tipoUsuario}" style="color: black">
                    Tipos de documentos</a>
                <div class="dropdown-divider"></div>      
                <a class="dropdown-item" href="consultas.jsp?procedencia=estado&perfil=${tipoUsuario}" style="color: black">
                    Estado</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=genero&perfil=${tipoUsuario}" style="color: black">
                    Genero</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Acerca de</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">
                    Ver información</a>
                </div>
            </div>
        </ul>
        <!--
        <div class="row justify-content-end ">
            <div class="col-1"></div>
            <div class="">
                <form action="" method="post" class="form-inline">
                    <!--class="form-inline= en la misma linea
                    <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                    <button class="btn btn-warning " type="reset" >Buscar</button>
                </form>
            </div>    
        </div>
        -->
        <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
            </div>
        </nav>     
        <%}%>
        
        <%if ("Docente".equals(perfil)){%>
        <nav class="navbar navbar-dark bg-success">
        <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
        <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
        <ul class="nav nav-pills">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Administración</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">Acudientes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">Estudiantes</a>
                </div>
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Matriculas</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">Detalle Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">Seguimiento</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Informes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">Resumen general de grupos</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">Top 10 mejores estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">Paz y salvo Estudiantes</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Acerca de</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">Ver información</a>
                </div>
            </div>
        </ul>
        <div class="row justify-content-end ">
            <div class="col-1"></div>
            <div class="">
                <form action="#" method="post" class="form-inline">
                    <!--class="form-inline= en la misma linea-->
                    <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                    <button class="btn btn-warning " type="reset" >Buscar</button>
                </form>
            </div>    
        </div>
        <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
        </div>
        </nav>
        <%}%>
        
        <div class="container">            
        <%if ("usuarios".equals(procedencia)){
            UsuarioDAO uDAO = new UsuarioDAO();
            Usuario usr = new Usuario();
            List<Usuario> lista=uDAO.ConsultarUsuarios();
            DocenteDAO dDAO = new DocenteDAO();
            Docente d = new Docente();
            List<Docente> listaDocentes = dDAO.ConsultarDocentes();
        %>
        
        <p><center><h3>Usuarios registrados</h3></center>
        <div class="container " style="margin-top:10px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=usuarios&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                            <tr class="bg-success" style="color: white">
                                <td>Correo</td>
                                <td>Clave</td>
                                <td>Identificación Docente</td>
                                <td>Perfil</td>
                                <td>Acciones</td>
                            </tr>
                            <tr>
                                <td>
                                <input class="form-control" type="text" id="email" name="email" placeholder=
                                       "Ingrese su correo sin el dominio (@dominio.com)" >
                                </td>
                                <td>
                                    <input class="form-control" type="password" id="clave" name="clave" placeholder=
                                           "Ingrese su clave" >
                                </td>
                                <td>
                                    <select class="form-control" name="identificacionDocente" id="identificacionDocente" 
                                            required="true">
                                        <%  String idDocente="";
                                            String nombreDocente="";
                                                for(Docente doc:listaDocentes)
                                            {
                                                idDocente=String.valueOf(doc.getIdentificacionDocente());
                                                nombreDocente=doc.getNombreDocente();
                                        %>
                                        <option value="<%=idDocente%>"><%=nombreDocente%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="Perfil" required="true">
                                    <option>Admin</option>
                                    <option>Docente</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            <%  String correo="";
                            String clave="";
                            String identificacionDicente="";
                            String tipoUsuario="";
                            for(Usuario u:lista)
                            {
                            correo=u.getCorreoUsuario();
                            clave=u.getClave();
                            identificacionDicente=u.getIdentificacionDocente();
                            tipoUsuario=u.getTipoUsuario();                            
                            %>
                                <td>
                                    <%=correo%>
                                </td>
                                <td>
                                    <%=clave%>
                                </td>
                                <td>
                                     <%=identificacionDicente%>
                                </td>
                                <td>
                                     <%=tipoUsuario%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=usuarios&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                                                     
                        </table>

                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        
        <%}%>
        
        
        <%if ("tipoDocumento".equals(procedencia)){
            tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
            tipoDocumento td = new tipoDocumento();
            List<tipoDocumento> lista=tdDAO.ConsultarTipoDocumentos();%>  
            <p><center><h3>Tipos de documentos registrados</h3></center>
        <div class="container " style="margin-top:10px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=tipoDocumento&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Tipo Documento</th>
                                <th scope="col">Nombre Tipo Documento</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idTipoDocumento="";
                                String nombreDocumento="";
                                for(tipoDocumento tdo:lista)
                            {
                                idTipoDocumento=tdo.getIdDocumento();
                                nombreDocumento=tdo.getNombreDocumento();%>
                            <tr>
                                <td>
                                    <%=idTipoDocumento%>
                                </td>
                                <td>
                                    <%=nombreDocumento%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=tipoDocumento&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("estado".equals(procedencia)){
            EstadoDAO esDAO = new EstadoDAO();
            Estado es = new Estado();
            List<Estado> lista=esDAO.ConsultarEstados();%>  
        <p><center><h3>Estados registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=tipoDocumento&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                    <table
                        class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Estado</th>
                                <th scope="col">Descripcion Estado</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idEstado="";
                                String DescripcionEstado="";
                                for(Estado est:lista)
                            {
                                idEstado=String.valueOf(est.getId());
                                DescripcionEstado=est.getNombreEstado();%>
                            <tr>
                                <td>
                                    <%=idEstado%>
                                </td>
                                <td>
                                    <%=DescripcionEstado%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=estado&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("genero".equals(procedencia)){
            GeneroDAO gDAO = new GeneroDAO();
            Genero g = new Genero();
            List<Genero> lista=gDAO.ConsultarGeneros();%>  
        <p><center><h3>Géneros registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=genero&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Genero</th>
                                <th scope="col">Descripción del Género</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idGenero="";
                                String DescripcionGenero="";
                                for(Genero ge:lista)
                            {
                                idGenero=ge.getIdGenero();
                                DescripcionGenero=ge.getDescripcionGenero();%>
                            <tr>
                                <td>
                                    <%=idGenero%>
                                </td>
                                <td>
                                    <%=DescripcionGenero%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=genero&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("categoria".equals(procedencia)){
            CategoriaDAO cDAO = new CategoriaDAO();
            Categoria c = new Categoria();
            List<Categoria> lista = cDAO.ConsultarCategorias();%>  
        <p><center><h3>Categorías registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=categoria&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Categoria</th>
                                <th scope="col">Descripción de la Categoría</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idCategoria="";
                                String DescripcionCategoria="";
                                for(Categoria ca:lista)
                            {
                                idCategoria = String.valueOf(ca.getIdCategoria());
                                DescripcionCategoria = ca.getDescripcionCategoria();%>
                            <tr>
                                <td>
                                    <%=idCategoria%>
                                </td>
                                <td>
                                    <%=DescripcionCategoria%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=categoria&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("deporte".equals(procedencia)){
            DeporteDAO dDAO = new DeporteDAO();
            Deporte d = new Deporte();
            List<Deporte> lista = dDAO.ConsultarDeportes();%>  
        <p><center><h3>Deportes registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=deporte&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="#">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Deporte</th>
                                <th scope="col">Descripción del Deporte</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idDeporte="";
                                String DescripcionDeporte="";
                                for(Deporte de:lista)
                            {
                                idDeporte = String.valueOf(de.getIdDeporte());
                                DescripcionDeporte = de.getDescripcionDeporte();%>
                            <tr>
                                <td>
                                    <%=idDeporte%>
                                </td>
                                <td>
                                    <%=DescripcionDeporte%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=deporte&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("docente".equals(procedencia)){
            DocenteDAO dDAO = new DocenteDAO();
            Docente d = new Docente();
            List<Docente> lista = dDAO.ConsultarDocentes();%>  
        <p><center><h3>Docentes registrados</h3></center>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=docente&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarDocentes">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <center>
                    <table class="table table-hover">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Tipo identificación</th>
                                <th scope="col">Nro. de identificación</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha de Nacimiento</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Estudios</th>
                                <th scope="col">Genero</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String tipoDocumento;
                                String identDocente;
                                String nombreDocente;
                                Date fechaNacimientoDocente;
                                String direccionDocente;
                                String telefonoDocente;
                                String correoDocente;
                                String estudiosDocente;
                                String generoDocente;
                                String estadoDocente;
                                for(Docente de:lista)
                            {
                                tipoDocumento = de.getTipoDocumento();
                                identDocente = String.valueOf(de.getIdentificacionDocente());
                                nombreDocente = de.getNombreDocente();
                                fechaNacimientoDocente = de.getFechaNacimientoDocente();
                                direccionDocente = de.getDireccionDocente();
                                telefonoDocente = de.getTelefonoDocente();
                                correoDocente = de.getCorreoDocente();
                                estudiosDocente = de.getEstudiosDocente();
                                generoDocente = de.getGeneroDocente();
                                estadoDocente = de.getEstadoDocente();%>
                            <tr>
                                <td>
                                    <%=tipoDocumento%>
                                </td>
                                <td>
                                    <%=identDocente%>
                                </td>
                                <td>
                                    <%=nombreDocente%>
                                </td>
                                <td>
                                    <%=fechaNacimientoDocente%>
                                </td>
                                <td>
                                    <%=direccionDocente%>
                                </td>
                                <td>
                                    <%=telefonoDocente%>
                                </td>
                                <td>
                                    <%=correoDocente%>
                                </td>
                                <td>
                                    <%=estudiosDocente%>
                                </td>
                                <td>
                                    <%=generoDocente%>
                                </td>
                                <td>
                                    <%=estadoDocente%>
                                </td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                    </center>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("acudiente".equals(procedencia)){
            AcudienteDAO aDAO = new AcudienteDAO();
            Acudiente a = new Acudiente();
            List<Acudiente> lista = aDAO.ConsultarAcudientes();%>  
        <p><center><h3>Acudientes registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=acudiente&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarAcudientes">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Tipo identificación</th>
                                <th scope="col">Nro. de identificación</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha de Nacimiento</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Genero</th>
                                <th scope="col">Vínculo con la Institución</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String tipoDocumentoAcudiente;
                                String identAcudiente;
                                String nombreAcudiente;
                                Date fechaNacimientoAcudiente;
                                String direccionAcudiente;
                                String telefonoAcudiente;
                                String generoAcudiente;
                                String vinculoInstitucionAcudiente;
                                for(Acudiente ac:lista)
                            {
                                tipoDocumentoAcudiente = ac.getTipoDocumentoAcudiente();
                                identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                nombreAcudiente = ac.getNombreAcudiente();
                                fechaNacimientoAcudiente = ac.getFechaNacimientoAcudiente();
                                direccionAcudiente = ac.getDireccionAcudiente();
                                telefonoAcudiente = ac.getTelefonoAcudiente();
                                generoAcudiente = ac.getGeneroAcudiente();
                                vinculoInstitucionAcudiente = ac.getVinculoInstitucionAcudiente();%>
                            <tr>
                                <td>
                                    <%=tipoDocumentoAcudiente%>
                                </td>
                                <td>
                                    <%=identAcudiente%>
                                </td>
                                <td>
                                    <%=nombreAcudiente%>
                                </td>
                                <td>
                                    <%=fechaNacimientoAcudiente%>
                                </td>
                                <td>
                                    <%=direccionAcudiente%>
                                </td>
                                <td>
                                    <%=telefonoAcudiente%>
                                </td>
                                <td>
                                    <%=generoAcudiente%>
                                </td>
                                <td>
                                    <%=vinculoInstitucionAcudiente%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=acudiente&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("estudiante".equals(procedencia)){
            EstudianteDAO eDAO = new EstudianteDAO();
            Estudiante e = new Estudiante();
            List<Estudiante> lista = eDAO.ConsultarEstudiantes();%>  
        <p><center><h3>Estudiantes registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=estudiante&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarEstudiantes">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Tipo identificación</th>
                                <th scope="col">Nro. de identificación</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha de Nacimiento</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Genero</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Acudiente</th>
                                <th scope="col">Paz y Salvo</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String tipoDocumentoEstudiante;
                                String identEstudiante;
                                String nombreEstudiante;
                                Date fechaNacimientoEstudiante;
                                String direccionEstudiante;
                                String telefonoEstudiante;
                                String generoEstudiante;
                                String estadoAcudiente;
                                String acudienteEstudiante;
                                String pazYSalvoEstudiante;
                                
                                for(Estudiante es:lista)
                            {
                                tipoDocumentoEstudiante = es.getTipoIdentificacionEstudiante();
                                identEstudiante = String.valueOf(es.getIdentificacionEstudiante());
                                nombreEstudiante = es.getNombreEstudiante();
                                fechaNacimientoEstudiante = es.getFechaNacimientoEstudiante();
                                direccionEstudiante = es.getDireccionEstudiante();
                                telefonoEstudiante = es.getTelefonoEstudiante();
                                generoEstudiante = es.getGeneroEstudiante();
                                estadoAcudiente = es.getEstadoEstudiante();
                                acudienteEstudiante = String.valueOf(es.getAcudienteEstudiante());
                                pazYSalvoEstudiante = es.getPazYSalvoEstudiante();%>
                            <tr>
                                <td>
                                    <%=tipoDocumentoEstudiante%>
                                </td>
                                <td>
                                    <%=identEstudiante%>
                                </td>
                                <td>
                                    <%=nombreEstudiante%>
                                </td>
                                <td>
                                    <%=fechaNacimientoEstudiante%>
                                </td>
                                <td>
                                    <%=direccionEstudiante%>
                                </td>
                                <td>
                                    <%=telefonoEstudiante%>
                                </td>
                                <td>
                                    <%=generoEstudiante%>
                                </td>
                                <td>
                                    <%=estadoAcudiente%>
                                </td>
                                <td>
                                    <%=acudienteEstudiante%>
                                </td>
                                <td>
                                    <%=pazYSalvoEstudiante%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=estudiante&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("grupo".equals(procedencia)){
            GrupoDAO gpDAO = new GrupoDAO();
            Grupo gp = new Grupo();
            List<Grupo> lista = gpDAO.ConsultarGrupos();%>  
        <p><center><h3>Grupos registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=grupo&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarGrupos">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Grupo</th>
                                <th scope="col">Id Deporte</th>
                                <th scope="col">Id Categoria</th>
                                <th scope="col">Horario Inicio</th>
                                <th scope="col">Horario Fin</th>
                                <th scope="col">Objetivo</th>
                                <th scope="col">Id Docente</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idGrupo="";
                                String idDeporte="";
                                String idCategoria="";
                                String horarioInicio;
                                String horarioFin;
                                String objetivo="";
                                String idDocente="";
                                
                                for(Grupo g:lista)
                                {
                                    idGrupo = String.valueOf(g.getIdGrupo());
                                    idDeporte = String.valueOf(g.getIdDeporte());
                                    idCategoria = String.valueOf(g.getIdCategoria());
                                    horarioInicio = g.getHorarioInicio();
                                    horarioFin = g.getHorarioFin();
                                    objetivo = g.getObjetivo();
                                    idDocente = String.valueOf(g.getIdDocente());%>
                            <tr>
                                <td>
                                    <%=idGrupo%>
                                </td>
                                <td>
                                    <%=idDeporte%>
                                </td>
                                <td>
                                    <%=idCategoria%>
                                </td>
                                <td>
                                    <%=horarioInicio%>
                                </td>
                                <td>
                                    <%=horarioFin%>
                                </td>
                                <td>
                                    <%=objetivo%>
                                </td>
                                <td>
                                    <%=idDocente%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=grupo&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("detalleGrupo".equals(procedencia)){
            DetalleGrupoDAO DetGDAO = new DetalleGrupoDAO();
            DetalleGrupo DetG = new DetalleGrupo();
            List<DetalleGrupo> lista = DetGDAO.ConsultarDetGrupo();%>  
        <p><center><h3>Detalle Grupos registrados</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=detalleGrupo&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarDetalleGrupos">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Grupo</th>
                                <th scope="col">Id Estudiante</th>
                                <th scope="col">Nota Final Estudiante</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idGrupo="";
                                String idEstudiante="";
                                String notaFinalEst;
                                for(DetalleGrupo dg:lista)
                                {
                                    idGrupo = String.valueOf(dg.getIdGrupo());
                                    idEstudiante = String.valueOf(dg.getIdEstudiante());
                                    notaFinalEst = String.valueOf(dg.getNotaFinalEst());%>
                            <tr>
                                <td>
                                    <%=idGrupo%>
                                </td>
                                <td>
                                    <%=idEstudiante%>
                                </td>
                                <td>
                                    <%=notaFinalEst%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=detalleGrupo&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("seguimiento".equals(procedencia)){
            seguimientoClaseAClaseDAO segDAO = new seguimientoClaseAClaseDAO();
            seguimientoClaseAClase seg = new seguimientoClaseAClase();
            List<seguimientoClaseAClase> listaSeguimiento = segDAO.ConsultarSeguimiento();%>  
        <p><center><h3>Seguimiento clase a clase</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=seguimiento&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="DescargaInformes?informe=ExportarSeguimientoClases">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Exportar
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Id Grupo</th>
                                <th scope="col">Id Estudiante</th>
                                <th scope="col">Fecha de la Clase</th>  
                                <th scope="col">Objetivo de la Clase</th>
                                <th scope="col">Nota de Clase</th>
                                <th scope="col">Observacion de la Clase</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String idGrupo="";
                                String idEstudiante="";
                                String fechaClaseControl="";
                                String objetivoClaseControl="";
                                String notaClaseControl="";
                                String observClasecontrol="";
                                for(seguimientoClaseAClase segui:listaSeguimiento)
                                {
                                    idGrupo= String.valueOf(segui.getIdGrupo());
                                    idEstudiante= String.valueOf(segui.getIdEstudiante());
                                    fechaClaseControl=String.valueOf(segui.getFechaClaseControl());
                                    objetivoClaseControl= segui.getObjetivosClaseControl();
                                    notaClaseControl= String.valueOf(segui.getNotaClaseControl());
                                    observClasecontrol= segui.getObervClaseControl();%>
                            <tr>
                                <td>
                                    <%=idGrupo%>
                                </td>
                                <td>
                                    <%=idEstudiante%>
                                </td>
                                <td>
                                    <%=fechaClaseControl%>
                                </td>
                                <td>
                                    <%=objetivoClaseControl%>
                                </td>
                                <td>
                                    <%=notaClaseControl%>
                                </td>
                                <td>
                                    <%=observClasecontrol%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=seguimiento&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
                  
        <%if ("Factura".equals(procedencia)){
            FacturaDAO fDAO = new FacturaDAO();
            Factura f = new Factura();
            List<Factura> listaf = fDAO.ConsultarFacturas();
            String resultadoOperacion = request.getParameter("resultadoOperacion");%>  
        <p><center><h3>Facturas registradas</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=Factura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ImprimirFactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-pdf"></i>Imprimir
                            </button></a>
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Nro Factura</th>
                                <th scope="col">Fecha de Expedición</th>
                                <th scope="col">Fecha de Vencimiento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Nombre Acudiente</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Valor Total</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String nroFactura="";
                                String fechaExp="";
                                String fechaVenc="";
                                String identificacion="";
                                String acudiente="";
                                String descripcion="";
                                String valorTotal="";
                                String estado="";
                                String estado1="";
                                
                                for(Factura lf:listaf)
                                {
                                    if (("Pagado".equals(lf.getEstado()))||("Sin Pagar".equals(lf.getEstado()))){
                                    nroFactura = String.valueOf(lf.getNroFactura());
                                    fechaExp = lf.getFechaExp();
                                    fechaVenc = lf.getFechaVenc();
                                    identificacion = String.valueOf(lf.getIdentificacion());
                                    acudiente = lf.getAcudiente();
                                    descripcion = lf.getDescripcion();
                                    valorTotal = String.valueOf(lf.getValorTotal());
                                    estado = lf.getEstado();
                                ;%>
                            <tr>
                                <td>
                                    <%=nroFactura%>
                                </td>
                                <td>
                                    <%=fechaExp%>
                                </td>
                                <td>
                                    <%=fechaVenc%>
                                </td>
                                <td>
                                    <%=identificacion%>
                                </td>
                                <td>
                                    <%=acudiente%>
                                </td>
                                <td>
                                    <%=descripcion%>
                                </td>
                                <td>
                                    <%=valorTotal%>
                                </td>
                                <td>
                                    <%=estado%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ActFactura&NroFactura=<%=nroFactura%>&perfil=${tipoUsuario}"><i class="fas fa-edit "></i></a>
                                    <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=AnularFactura&NroFactura=<%=nroFactura%>&perfil=${tipoUsuario}"><i class="fas fa-times-circle "></i></a>
                                    <a class="" href="DescargaPDF?accion=Imprimir Factura&NroFactura=<%=nroFactura%>"><i class="fa fa-file-pdf-o"></i></a>
                                </td>
                            </tr>
                            <%}}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div>
                <center><p><%=resultadoOperacion%></p></center>
            </div>
        </div>
        <%}%>
        
        <%if ("FacturaAnulada".equals(procedencia)){
            FacturaDAO fDAO = new FacturaDAO();
            Factura f = new Factura();
            List<Factura> listaf = fDAO.ConsultarFacturas();%>  
        <p><center><h3>Facturas registradas</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=Factura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a> 
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ImprimirFactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Imprimir
                            </button></a>                      
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Nro Factura</th>
                                <th scope="col">Fecha de Expedición</th>
                                <th scope="col">Fecha de Vencimiento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Nombre Acudiente</th>
                                <th scope="col">Motivo anulación</th>
                                <th scope="col">Valor Total</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String nroFactura="";
                                String fechaExp="";
                                String fechaVenc="";
                                String identificacion="";
                                String acudiente="";
                                String observacion="";
                                String valorTotal="";
                                String estado="";
                                
                                for(Factura lf:listaf)
                                {
                                    if ("Anulado".equals(lf.getEstado())){
                                    nroFactura = String.valueOf(lf.getNroFactura());
                                    fechaExp = lf.getFechaExp();
                                    fechaVenc = lf.getFechaVenc();
                                    identificacion = String.valueOf(lf.getIdentificacion());
                                    acudiente = lf.getAcudiente();
                                    observacion = lf.getObservaciones();
                                    valorTotal = String.valueOf(lf.getValorTotal());
                                    estado = lf.getEstado();
                                    %>
                            <tr>
                                <td>
                                    <%=nroFactura%>
                                </td>
                                <td>
                                    <%=fechaExp%>
                                </td>
                                <td>
                                    <%=fechaVenc%>
                                </td>
                                <td>
                                    <%=identificacion%>
                                </td>
                                <td>
                                    <%=acudiente%>
                                </td>
                                <td>
                                    <%=observacion%>
                                </td>
                                <td>
                                    <%=valorTotal%>
                                </td>
                                <td> <%=estado%></td>
                            </tr>
                            <%}}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("PreFactura".equals(procedencia)){
            PrefacturaDAO pfDAO = new PrefacturaDAO();
            Prefactura pf = new Prefactura();
            List<Prefactura> listapf = pfDAO.ConsultarPrefacturas();%>  
        <p><center><h3>Prefacturas registradas</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                        <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=PreFactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-plus"></i>Crear
                            </button></a>
                       <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ImprimirPrefactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Imprimir
                            </button></a>                        
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Nro Prefactura</th>
                                <th scope="col">Fecha de Expedición</th>
                                <th scope="col">Fecha de Vencimiento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Nombre Acudiente</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Valor Total</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String nroPrefactura="";
                                String fechaExp="";
                                String fechaVenc="";
                                String identificacion;
                                String acudiente;
                                String descripcion="";
                                String valorTotal="";
                                String estado="";
                                
                                for(Prefactura pfac:listapf){
                                    
                                    nroPrefactura = String.valueOf(pfac.getNroPrefactura());
                                    fechaExp = pfac.getFechaExp();
                                    fechaVenc = pfac.getFechaVenc();
                                    identificacion = String.valueOf(pfac.getIdentificacion());
                                    acudiente = pfac.getAcudiente();
                                    descripcion = pfac.getDescripcion();
                                    valorTotal = String.valueOf(pfac.getValorTotal());
                                    estado = pfac.getEstado();
                                    %>
                            <tr>
                                <td>
                                    <%=nroPrefactura%>
                                </td>
                                <td>
                                    <%=fechaExp%>
                                </td>
                                <td>
                                    <%=fechaVenc%>
                                </td>
                                <td>
                                    <%=identificacion%>
                                </td>
                                <td>
                                    <%=acudiente%>
                                </td>
                                <td>
                                    <%=descripcion%>
                                </td>
                                <td>
                                    <%=valorTotal%>
                                </td>
                                <td>
                                    <%=estado%>
                                </td>
                                <td> <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ActPreFactura&NroPreFactura=<%=nroPrefactura%>&perfil=${tipoUsuario}"><i class="fas fa-check-circle "></i></a>
                                    <a class="" href="DescargaPDF?accion=Imprimir Pre-factura&NroPrefactura=<%=nroPrefactura%>"><i class="fa fa-file-pdf-o"></i></a>
                                </td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        
        <%if ("PrefacturaAnulada".equals(procedencia)){
            PrefacturaDAO pfDAO = new PrefacturaDAO();
            Prefactura pf = new Prefactura();
            List<Prefactura> listapf = pfDAO.ConsultarPrefacturas();%>  
        <p><center><h3>Prefacturas anuladas</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                       <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ImprimirPrefactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Imprimir
                            </button></a>                        
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Nro Prefactura</th>
                                <th scope="col">Fecha de Expedición</th>
                                <th scope="col">Fecha de Vencimiento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Nombre Acudiente</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Valor Total</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String nroPrefactura="";
                                String fechaExp="";
                                String fechaVenc="";
                                String identificacion;
                                String acudiente;
                                String descripcion="";
                                String valorTotal="";
                                String estado="Anulado";
                                
                                for(Prefactura fac:listapf)
                                {
                                    if (pf.getEstado().equalsIgnoreCase(estado)){
                                    nroPrefactura = String.valueOf(pf.getNroPrefactura());
                                    fechaExp = pf.getFechaExp();
                                    fechaVenc = pf.getFechaVenc();
                                    identificacion = String.valueOf(pf.getIdentificacion());
                                    acudiente = pf.getAcudiente();
                                    descripcion = pf.getDescripcion();
                                    valorTotal = String.valueOf(pf.getValorTotal());%>
                            <tr>
                                <td>
                                    <%=nroPrefactura%>
                                </td>
                                <td>
                                    <%=fechaExp%>
                                </td>
                                <td>
                                    <%=fechaVenc%>
                                </td>
                                <td>
                                    <%=identificacion%>
                                </td>
                                <td>
                                    <%=acudiente%>
                                </td>
                                <td>
                                    <%=descripcion%>
                                </td>
                                <td>
                                    <%=valorTotal%>
                                </td>
                                <td> <%=estado%></td>
                            </tr>
                            <%}}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <%if ("PrefacturaAprobada".equals(procedencia)){
            PrefacturaDAO pfDAO = new PrefacturaDAO();
            Prefactura pf = new Prefactura();
            List<Prefactura> listapf = pfDAO.ConsultarPrefacturas();%>  
        <p><center><h3>Prefacturas anuladas</h3></center>
        <div class="container " style="margin-top:50px">
            <div id=""> 
                <div class="card">
                    <div class="card-header align-self-end">
                       <a class="" href="formulario.jsp?resultadoOperacion=En linea&procedencia=ImprimirPrefactura&perfil=${tipoUsuario}">
                            <button type="button"
                            class="btn btn-outline-dark"><i class="fas fa-file-export"></i>Imprimir
                            </button></a>                        
                    </div>
                    <div id="uno" class="collapse show">
                        <table
                            class="table table-hover text-center table table-striped table-bordered table-responsive-xs">
                        <thead>
                            <tr class="bg-success" style="color: white">
                                <th scope="col">Nro Prefactura</th>
                                <th scope="col">Fecha de Expedición</th>
                                <th scope="col">Fecha de Vencimiento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Nombre Acudiente</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Valor Total</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  String nroPrefactura="";
                                String fechaExp="";
                                String fechaVenc="";
                                String identificacion;
                                String acudiente;
                                String descripcion="";
                                String valorTotal="";
                                String estado="Aprobado";
                                
                                for(Prefactura fac:listapf)
                                {
                                    if (pf.getEstado().equalsIgnoreCase(estado)){
                                    nroPrefactura = String.valueOf(pf.getNroPrefactura());
                                    fechaExp = pf.getFechaExp();
                                    fechaVenc = pf.getFechaVenc();
                                    identificacion = String.valueOf(pf.getIdentificacion());
                                    acudiente = pf.getAcudiente();
                                    descripcion = pf.getDescripcion();
                                    valorTotal = String.valueOf(pf.getValorTotal());%>
                            <tr>
                                <td>
                                    <%=nroPrefactura%>
                                </td>
                                <td>
                                    <%=fechaExp%>
                                </td>
                                <td>
                                    <%=fechaVenc%>
                                </td>
                                <td>
                                    <%=identificacion%>
                                </td>
                                <td>
                                    <%=acudiente%>
                                </td>
                                <td>
                                    <%=descripcion%>
                                </td>
                                <td>
                                    <%=valorTotal%>
                                </td>
                                <td> <%=estado%></td>
                            </tr>
                            <%}}%>
                        </tbody>
                    </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        
        <%if ("pazYSalvo".equals(procedencia)){
            
        %>
            <h1>Resultado de carga</h1>
            <ul>
            <li>fileName:${fileName}</li>
            <li>contentType:${contentType}</li>
            <li>size:${size/1024.0} KB</li>
            <li>líneas:${lineas}</li>
            </ul>
        <%}%>
        
        <br>
        <div class="col-12">
            <p class="mt-2 mb-3 text-muted text-center ">&copy;
                Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4)
                319
                79 00 |
                NIT: 890980136-6<br>
                Institución de Educación Superior de caracter pública y departamental sujeta a
                inspección y
                vigilancia
                por parte del Ministerio de Educación.
                <img src="img/icontec.png" width="70px">           
            </p>
        </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
