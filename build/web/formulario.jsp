<%-- 
    Document   : formulario
    Created on : 12/03/2020, 09:03:08 AM
    Author     : ecardona
    Descripcion: Contiene todos los formularios del sistema, recibe la información de donde que opción viene del Menú
                 y le muestra el formulario de acuerdo a de donde vino
--%>

<%@page import="modelo.Factura"%>
<%@page import="persistencia.FacturaDAO"%>
<%@page import="modelo.Servicio"%>
<%@page import="persistencia.ServicioDAO"%>
<%@page import="modelo.Prefactura"%>
<%@page import="persistencia.PrefacturaDAO"%>
<%@page import="modelo.tipoDocumento"%>
<%@page import="persistencia.tipoDocumentoDAO"%>
<%@page import="modelo.Docente"%>
<%@page import="persistencia.DocenteDAO"%>
<%@page import="modelo.Estudiante"%>
<%@page import="persistencia.EstudianteDAO"%>
<%@page import="modelo.Grupo"%>
<%@page import="persistencia.GrupoDAO"%>
<%@page import="modelo.Categoria"%>
<%@page import="persistencia.CategoriaDAO"%>
<%@page import="modelo.Deporte"%>
<%@page import="persistencia.DeporteDAO"%>
<%@page import="modelo.Acudiente"%>
<%@page import="persistencia.AcudienteDAO"%>
<%@page import="modelo.Estado"%>
<%@page import="persistencia.EstadoDAO"%>
<%@page import="modelo.Genero"%>
<%@page import="persistencia.GeneroDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html>
    <head>
        <link  rel="icon"   href="imagenes/logoPoli.png" type="image/png" />
        <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">
        <!--<link href="css/estilosFormularios.css" rel="stylesheet" type="text/css"/>-->
        <title>Formulario</title>
    </head>
    <body>
        <%String resultadoOperacion = request.getParameter("resultadoOperacion");
            String procedencia = request.getParameter("procedencia");
            String perfil = request.getParameter("perfil");%>

        <%if ("Admin".equals(perfil)) {%>

        <nav class="navbar navbar-dark bg-success">
        <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
        <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
        <ul class="nav nav-pills">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Administración</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=usuarios&perfil=${tipoUsuario}" style="color: black">
                    Usuarios</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">
                    Acudientes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">
                    Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=docente&perfil=${tipoUsuario}" style="color: black">
                    Docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?resultadoOperacion=En linea&procedencia=pazYSalvo&perfil=
                   ${tipoUsuario}" style="color: black">Cargue Masivo Paz y Salvo</a>
                </div>
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Matriculas</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">
                      Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">
                      Detalle Grupos</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">
                      Seguimiento</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Informes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">
                    Resumen general de grupos</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">
                    Top 10 mejores estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">
                    Paz y salvo Estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 4</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" style="color: black">Informe 5</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Reportes</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=Cerficado&perfil=${tipoUsuario}" style="color: black">Certificado de Matrícula estudiante</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoEstudiantes" style="color: black">Listado de estudiantes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="DescargaPDF?accion=ListadoDocentes" style="color: black">Listado de docentes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupo&perfil=${tipoUsuario}" style="color: black">Reporte de notas final</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoGanaron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Ganaron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirNotasGrupoPerdieron&perfil=${tipoUsuario}" style="color: black">Reporte de Alumnos que Perdieron</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirFactura&perfil=${tipoUsuario}" style="color: black">Imprimir factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="formulario.jsp?procedencia=ImprimirPrefactura&perfil=${tipoUsuario}" style="color: black">Imprimir pre-factura</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Facturación</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=Factura&perfil=${tipoUsuario}" style="color: black">Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=FacturaAnulada&perfil=${tipoUsuario}" style="color: black">Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFactura&perfil=${tipoUsuario}" style="color: black">Pre Factura</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAnulada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Anuladas</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=PreFacturaAprobada&perfil=${tipoUsuario}" style="color: black">Pre Facturas Aprobadas</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Tablas</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="consultas.jsp?procedencia=deporte&perfil=${tipoUsuario}" style="color: black">
                    Deportes</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=categoria&perfil=${tipoUsuario}" style="color: black">
                    Categorias</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=tipoDocumento&perfil=${tipoUsuario}" style="color: black">
                    Tipos de documentos</a>
                <div class="dropdown-divider"></div>      
                <a class="dropdown-item" href="consultas.jsp?procedencia=estado&perfil=${tipoUsuario}" style="color: black">
                    Estado</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="consultas.jsp?procedencia=genero&perfil=${tipoUsuario}" style="color: black">
                    Genero</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
                   aria-expanded="false" style="color: white">Acerca de</a>
                <div class="dropdown-menu">
                <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">
                    Ver información</a>
                </div>
            </div>
        </ul>
        <!--
        <div class="row justify-content-end ">
            <div class="col-1"></div>
            <div class="">
                <form action="" method="post" class="form-inline">
                    <!--class="form-inline= en la misma linea
                    <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                    <button class="btn btn-warning " type="reset" >Buscar</button>
                </form>
            </div>    
        </div>
        -->
        <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
            </div>
        </nav>
        <%}%>

        <%if ("Docente".equals(perfil)) {%>
        <nav class="navbar navbar-dark bg-success">
            <!--<a href="menu.jsp" style="color: white" class="navbar-toggler"><span class="navbar-toggler-icon"></span> Home </a>-->
            <a href="menu.jsp" class="navbar-brand"><img src="img/logoBlanco.png" style="width:250px;"></a>
            <ul class="nav nav-pills">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Administración</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="consultas.jsp?procedencia=acudiente&perfil=${tipoUsuario}" style="color: black">Acudientes</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="consultas.jsp?procedencia=estudiante&perfil=${tipoUsuario}" style="color: black">Estudiantes</a>
                    </div>
                </div>
                <div class="dropdown">
                    <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Matriculas</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="consultas.jsp?procedencia=grupo&perfil=${tipoUsuario}" style="color: black">Grupos</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="consultas.jsp?procedencia=detalleGrupo&perfil=${tipoUsuario}" style="color: black">Detalle Grupos</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="consultas.jsp?procedencia=seguimiento&perfil=${tipoUsuario}" style="color: black">Seguimiento</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Informes</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="DescargaInformes?informe=ResumenGeneral" style="color: black">Resumen general de grupos</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="DescargaInformes?informe=EstudiantesTOP" style="color: black">Top 10 mejores estudiantes</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="DescargaInformes?informe=PazYSallvoEstudiantes" style="color: black">Paz y salvo Estudiantes</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white">Acerca de</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="formulario.jsp?procedencia=acercaDe&perfil=${tipoUsuario}" style="color: black">Ver información</a>
                    </div>
                </div>
            </ul>
            <div class="row justify-content-end ">
                <div class="col-1"></div>
                <div class="">
                    <form action="#" method="post" class="form-inline">
                        <!--class="form-inline= en la misma linea-->
                        <input type="text" placeholder="Buscar" class="form-control mr-sm2">
                        <button class="btn btn-warning " type="reset" >Buscar</button>
                    </form>
                </div>    
            </div>
            <div class="dropdown">
                <a style="color: white" href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cerrar sesión</a>
                <div class="dropdown-menu text-center">
                    <a><img src="imagenes/login.png" width="80"/></a><br>
                    <a>User: ${correo}</a><br>
                    <a>Perfil: ${tipoUsuario}</a>
                    <div class="dropdown-divider"></div>
                    <a href="controlador?accion=Salir" class="dropdown-item">Salir</a>
                </div>
            </div>
        </nav>
        <%}%>

        <%if ("usuarios".equals(procedencia)) {
                DocenteDAO dDAO = new DocenteDAO();
                Docente d = new Docente();
                List<Docente> listaDocentes = dDAO.ConsultarDocentes();
        %>    
        <div class=" container" >
            <div class="row">
                <div class="col-12 badge-success fixed-top ">
                    <p></p>
                </div>
                <div class="col-12 badge-success fixed-bottom ">
                    <p></p>
                </div>
                <div class="container " >
                    <div class="row justify-content-center ">
                        <div class="col-4 border ">
                            <div class="text-center "><br>
                                <img class=" w-75 justify-content-center " src="img/logo.png"><br><br>
                                <h5>ESCUELA DE INICIACION DEPORTIVA</h5> <br>
                                <h7>Usuarios</h7>
                            </div>
                            <form class="form" name="Usuarios" action="controlador" method="post">
                                <div class="form-group text-center">
                                    <img src="imagenes/login.png" width="80"/>
                                </div>
                                <div class="form-group">
                                    <label>Correo (sin @elpoli.edu.co):</label>
                                    <input class="form-control" type="text" id="email" name="email" 
                                           placeholder="Ingrese su correo sin el dominio (@dominio.com)" required >
                                </div>


                                <div class="form-group">
                                    <label>Clave:</label>
                                    <input class="form-control" type="password" id="clave" name="clave" placeholder="Ingrese su clave" >
                                </div>
                                <div class="form-group">
                                    <label>Docente:</label>
                                    <select class="form-control" name="identificacionDocente" id="identificacionDocente" required="true">
                                        <%  String idDocente = "";
                                            String nombreDocente = "";
                                            for (Docente doc : listaDocentes) {
                                                idDocente = String.valueOf(doc.getIdentificacionDocente());
                                                nombreDocente = doc.getNombreDocente();
                                        %>
                                        <option value="<%=idDocente%>"><%=nombreDocente%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Perfil:</label>
                                    <select class="form-control" name="Perfil" required="true">
                                        <option>Admin</option>
                                        <option>Docente</option>
                                    </select>
                                </div>
                                <center>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                        </label>
                                        <label>
                                            <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                        </label>
                                        <label>
                                            <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                        </label>
                                        <label>
                                            <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                        </label>
                                    </div>
                                </center>
                                <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Usuario">
                            </form>
                        </div>
                    </div>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("tipoDocumento".equals(procedencia)) {%>
                <div class="container col-lg-3">
                    <form name="FormularioTipoDocumento" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Tipos de documentos</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id del documento:</label>
                            <input class="form-control" type="text" maxlength="2" id="idTipoDocumento" name="idTipoDocumento" 
                                   placeholder="Ingrese el Id para el tipo de documento" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre del documento:</label>
                            <input class="form-control" type="text" id="nombreDocumento" name="nombreDocumento" 
                                   placeholder="Ingrese el nombre del documento" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar tipo Documento">  
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>

                <%}%>

                <%if ("estado".equals(procedencia)) {%>
                <div class="container col-lg-3">
                    <form name="FormularioEstado" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Estados</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id estado:</label>
                            <input class="form-control" type="number" id="idEstado" name="idEstado" 
                                   placeholder="Ingrese el Id para el estado" required>
                        </div>
                        <div class="form-group">
                            <label>Descripción del estado:</label>
                            <input class="form-control" type="text" id="DescripcionEstado" name="DescripcionEstado" 
                                   placeholder="Ingrese la descripción del estado" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Estado">  
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("genero".equals(procedencia)) {%>
                <div class="container col-lg-3">
                    <form name="FormularioGenero" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Generos</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id del genero</label>
                            <input class="form-control" type="text" maxlength="1" id="idGenero" name="idGenero" 
                                   placeholder="Ingrese el Id para el genero" required>
                        </div>
                        <div class="form-group">
                            <label>Descripcion del Genero:</label>
                            <input class="form-control" type="text" id="descripcionGenero" name="descripcionGenero" 
                                   placeholder="Ingrese la descripción del genero" required>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Genero">  
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>

                <%}%>

                <%if ("categoria".equals(procedencia)) {%>
                <div class="container col-lg-3">
                    <form name="FormularioCategorias" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Categoría</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id Categoría:</label>
                            <input class="form-control" type="numeric" id="idCategoria" name="idCategoria" 
                                   placeholder="Ingrese el Id para la categoria" required>
                        </div>
                        <div class="form-group">
                            <label>Descripcion Categoría:</label>
                            <input class="form-control" type="text" id="descripcionCategoria" name="descripcionCategoria" 
                                   placeholder="Ingrese descripción para la categoria" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Categoria">  
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>

                <%}%>

                <%if ("deporte".equals(procedencia)) {%>
                <div class="container col-lg-3">
                    <form name="FormularioDeportes" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Deporte</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id Deporte</label>
                            <input class="form-control" type="numeric" id="idDeporte" name="idDeporte" 
                                   placeholder="Ingrese el Id para el deporte" required>
                        </div>
                        <div class="form-group">
                            <label>Descripcion Deporte</label>
                            <input class="form-control" type="text" id="descripcionDeporte" name="descripcionDeporte" 
                                   placeholder="Ingrese descripción para el deporte" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Deporte">  
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>

                <%}%>

                <%if ("docente".equals(procedencia)) {
                        tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                        tipoDocumento td = new tipoDocumento();
                        List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                        GeneroDAO gDAO = new GeneroDAO();
                        Genero g = new Genero();
                        List<Genero> listaGenero = gDAO.ConsultarGeneros();
                        EstadoDAO esDAO = new EstadoDAO();
                        Estado es = new Estado();
                        List<Estado> listaEstado = esDAO.ConsultarEstados();
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioDocentes" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/docente.png" width="80"/>
                            <p><strong>Docentes</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Tipo de identificación:</label>
                            <select class="form-control" name="tipoIdentificacion" id="tipoIdentificacion" >
                                <%  String idTipoDocumento = "";
                                    String nombreDocumento = "";
                                    for (tipoDocumento tdo : listaTtipoDocumento) {
                                        nombreDocumento = tdo.getNombreDocumento();
                                        idTipoDocumento = tdo.getIdDocumento();
                                %>
                                <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input class="form-control" type="numeric" id="identificacionDocente" name="identificacionDocente" 
                                   placeholder="Ingrese el número de identificación" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre Docente:</label>
                            <input class="form-control" type="text" id="nombreDocente" name="nombreDocente" 
                                   placeholder="Ingrese el nombre" >
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento:</label>
                            <input class="form-control" type="date" id="fechaNacimientoDocente" name="fechaNacimientoDocente" 
                                   placeholder="Ingrese la fecha de nacimiento" >
                        </div>
                        <div class="form-group">
                            <label>Dirección:</label>
                            <input class="form-control" type="text" id="direccionDocente" name="direccionDocente" 
                                   placeholder="Ingrese la dirección" >
                        </div>
                        <div class="form-group">
                            <label>Teléfono:</label>
                            <input class="form-control" type="text" id="telefonoDocente" name="telefonoDocente" 
                                   placeholder="Ingrese el telefono" >
                        </div>
                        <div class="form-group">
                            <label>Correo:</label>
                            <input class="form-control" type="email" id="emailDocente" name="emailDocente" 
                                   placeholder="Ingrese el correo" >
                        </div>
                        <div class="form-group">
                            <label>Estudios:</label>
                            <textarea class="form-control" name="estudiosDocente" id="estudiosDocente" rows="3" cols="30" 
                                      placeholder="Ingrese los estudios"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Género:</label>
                            <select class="form-control" name="generoDocente" required="true">
                                <%  String idGenero = "";
                                    String nombreGenero = "";
                                    for (Genero ge : listaGenero) {
                                        nombreGenero = ge.getDescripcionGenero();
                                        idGenero = ge.getIdGenero();
                                %>
                                <option value="<%=idGenero%>"><%=nombreGenero%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estado:</label>
                            <select class="form-control" name="estadoDocente" required="true">
                                <%  String idestado = "";
                                    String nombreEstado = "";
                                    for (Estado est : listaEstado) {
                                        nombreEstado = est.getNombreEstado();
                                        idestado = String.valueOf(est.getId());
                                %>
                                <option value="<%=idestado%>"><%=nombreEstado%></option>
                                <%}%>
                            </select>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Docente">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("acudiente".equals(procedencia)) {
                        tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                        tipoDocumento td = new tipoDocumento();
                        List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                        GeneroDAO gDAO = new GeneroDAO();
                        Genero g = new Genero();
                        List<Genero> listaGenero = gDAO.ConsultarGeneros();
                        EstadoDAO esDAO = new EstadoDAO();
                        Estado es = new Estado();
                        List<Estado> listaEstado = esDAO.ConsultarEstados();
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioAcudientes" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Acudientes</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Tipo de identificación:</label>
                            <select class="form-control" name="tipoIdentificacion" id="tipoIdentificacion" >
                                <%  String idTipoDocumento = "";
                                    String nombreDocumento = "";
                                    for (tipoDocumento tdo : listaTtipoDocumento) {
                                        nombreDocumento = tdo.getNombreDocumento();
                                        idTipoDocumento = tdo.getIdDocumento();
                                %>
                                <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input class="form-control" type="numeric" id="identificacionAcudiente" name="identificacionAcudiente" 
                                   placeholder="Ingrese el número de identificación" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre:</label>
                            <input class="form-control" type="text" id="nombreAcudiente" name="nombreAcudiente" 
                                   placeholder="Ingrese el nombre" >
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento:</label>
                            <input class="form-control" type="date" id="fechaNacimientoAcudiente" name="fechaNacimientoAcudiente" 
                                   placeholder="Ingrese la fecha de nacimiento" >
                        </div>
                        <div class="form-group">
                            <label>Dirección:</label>
                            <input class="form-control" type="text" id="direccionAcudiente" name="direccionAcudiente" 
                                   placeholder="Ingrese la dirección" >
                        </div>
                        <div class="form-group">
                            <label>Teléfono:</label>
                            <input class="form-control" type="text" id="telefonoAcudiente" name="telefonoAcudiente" 
                                   placeholder="Ingrese el telefono" >
                        </div>
                        <div class="form-group">
                            <label>Género:</label>
                            <select class="form-control" name="generoAcudiente" required>
                                <%  String idGenero = "";
                                    String descripcionGenero = "";
                                    for (Genero ge : listaGenero) {
                                        descripcionGenero = ge.getDescripcionGenero();
                                        idGenero = ge.getIdGenero();
                                %>
                                <option value="<%=idGenero%>"><%=descripcionGenero%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Vínculo:</label>
                            <select class="form-control" name="Perfil" required>
                                <option>Empleado</option>
                                <option>Estudiante</option>
                                <option>Graduado</option>
                                <option>Externo</option>
                            </select>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Acudiente">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("estudiante".equals(procedencia)) {
                        tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                        tipoDocumento td = new tipoDocumento();
                        List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                        GeneroDAO gDAO = new GeneroDAO();
                        Genero g = new Genero();
                        List<Genero> listaGenero = gDAO.ConsultarGeneros();
                        EstadoDAO esDAO = new EstadoDAO();
                        Estado es = new Estado();
                        List<Estado> listaEstado = esDAO.ConsultarEstados();
                        AcudienteDAO aDAO = new AcudienteDAO();
                        Acudiente a = new Acudiente();
                        List<Acudiente> listaAcudiente = aDAO.ConsultarAcudientes();
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioEstudiantes" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/estudiante.png" width="80"/>
                            <p><strong>Estudiantes</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Tipo de identificación:</label>
                            <select class="form-control" name="tipoIdentificacionEstudiante" id="tipoIdentificacionEstudiante" 
                                    required>
                                <%  String idTipoDocumento = "";
                                    String nombreDocumento = "";
                                    for (tipoDocumento tdo : listaTtipoDocumento) {
                                        nombreDocumento = tdo.getNombreDocumento();
                                        idTipoDocumento = tdo.getIdDocumento();
                                %>
                                <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input class="form-control" type="numeric" id="identificacionEstudiante" name="identificacionEstudiante" 
                                   placeholder="Ingrese el número de identificación" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre:</label>
                            <input class="form-control" type="text" id="nombreEstudiante" name="nombreEstudiante" 
                                   placeholder="Ingrese el nombre" >
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento:</label>
                            <input class="form-control" type="date" id="fechaNacimientoEstudiante" name="fechaNacimientoEstudiante" 
                                   placeholder="Ingrese la fecha de nacimiento" >
                        </div>
                        <div class="form-group">
                            <label>Dirección:</label>
                            <input class="form-control" type="text" id="direccionEstudiante" name="direccionEstudiante" 
                                   placeholder="Ingrese la dirección" >
                        </div>
                        <div class="form-group">
                            <label>Teléfono:</label>
                            <input class="form-control" type="text" id="telefonoEstudiante" name="telefonoEstudiante" 
                                   placeholder="Ingrese el telefono" >
                        </div>
                        <div class="form-group">
                            <label>Género:</label>
                            <select class="form-control" name="generoEstudiante" required="true">
                                <%  String idGenero = "";
                                    String descripcionGenero = "";
                                    for (Genero ge : listaGenero) {
                                        descripcionGenero = ge.getDescripcionGenero();
                                        idGenero = ge.getIdGenero();
                                %>
                                <option value="<%=idGenero%>"><%=descripcionGenero%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estado:</label>
                            <select class="form-control" name="estadoAcudiente" required="true">
                                <%  String idestado = "";
                                    String nombreEstado = "";
                                    for (Estado est : listaEstado) {
                                        idestado = String.valueOf(est.getId());
                                        nombreEstado = est.getNombreEstado();
                                %>
                                <option value="<%=idestado%>"><%=nombreEstado%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Acudiente</label>
                            <select class="form-control" name="acudienteEstudiante" required="true">
                                <%  String identAcudiente = "";
                                    String nombreAcudiente = "";
                                    for (Acudiente ac : listaAcudiente) {
                                        nombreAcudiente = String.valueOf(ac.getNombreAcudiente());
                                        identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                %>
                                <option value="<%=identAcudiente%>"><%=nombreAcudiente%></option>
                                <%}%>
                            </select>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Estudiante">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("grupo".equals(procedencia)) {
                        DeporteDAO dDAO = new DeporteDAO();
                        Deporte d = new Deporte();
                        List<Deporte> listaDeportes = dDAO.ConsultarDeportes();
                        CategoriaDAO cDAO = new CategoriaDAO();
                        Categoria c = new Categoria();
                        List<Categoria> listaCategorias = cDAO.ConsultarCategorias();
                        DocenteDAO doDAO = new DocenteDAO();
                        Docente doc = new Docente();
                        List<Docente> listaDocentes = doDAO.ConsultarDocentes();%>
                <div class="container col-lg-3">
                    <form name="FormularioGrupo" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Grupo</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Id Grupo</label>
                            <input class="form-control" type="numeric" id="idGrupo" name="idGrupo" placeholder="Ingrese el id" required>
                        </div>
                        <div class="form-group">
                            <label>id Deporte</label>
                            <select class="form-control" name="idDeporte" required="true">
                                <%  String idDeporte = "";
                                    String DescripcionDeporte = "";
                                    for (Deporte de : listaDeportes) {
                                        idDeporte = String.valueOf(de.getIdDeporte());
                                        DescripcionDeporte = de.getDescripcionDeporte();%>
                                <option value="<%=idDeporte%>"><%=DescripcionDeporte%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>id Categoria</label>
                            <select class="form-control" name="idCategoria" required="true">
                                <%  String idCategoria = "";
                                    String DescripcionCategoria = "";
                                    for (Categoria ca : listaCategorias) {
                                        idCategoria = String.valueOf(ca.getIdCategoria());
                                        DescripcionCategoria = ca.getDescripcionCategoria();%>
                                <option value="<%=idCategoria%>"><%=DescripcionCategoria%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Docente:</label>
                            <select class="form-control" name="identificacionDocente" id="identificacionDocente" required="true">
                                <%  String idDocente = "";
                                    String nombreDocente = "";
                                    for (Docente doce : listaDocentes) {
                                        idDocente = String.valueOf(doce.getIdentificacionDocente());
                                        nombreDocente = doce.getNombreDocente();
                                %>
                                <option value="<%=idDocente%>"><%=nombreDocente%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Hora inicio:</label>
                            <input class="form-control" type="time" id="horaInicioGrupo" name="horaInicioGrupo" 
                                   placeholder="Ingrese la hora de inicio" >
                        </div>
                        <div class="form-group">
                            <label>Hora fin:</label>
                            <input class="form-control" type="time" id="horaFinGrupo" name="horaFinGrupo" 
                                   placeholder="Ingrese la hora de fin" >
                        </div>
                        <div class="form-group">
                            <label>Objetivo Grupo</label>
                            <input class="form-control" type="text" id="objetivoGrupo" name="objetivoGrupo" 
                                   placeholder="Ingrese el objetivo" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Grupo">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("detalleGrupo".equals(procedencia)) {
                        GrupoDAO gpDAO = new GrupoDAO();
                        Grupo gp = new Grupo();
                        List<Grupo> listaGrupos = gpDAO.ConsultarGrupos();
                        EstudianteDAO eDAO = new EstudianteDAO();
                        Estudiante e = new Estudiante();
                        List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes();
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioDetalleGrupo" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Detalle Grupos</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Grupos</label>
                            <select class="form-control" name="idGrupo" required="true">
                                <%  String idGrupo = "";
                                    for (Grupo g : listaGrupos) {
                                        idGrupo = String.valueOf(g.getIdGrupo());%>
                                <option><%=idGrupo%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estudiantes</label>
                            <select class="form-control" name="idEstudiante" required="true">
                                <%  String idEstudiante = "";
                                    String nombreEstudiante = "";
                                    for (Estudiante est : listaEstudiantes) {
                                        idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                        nombreEstudiante = est.getNombreEstudiante();%>
                                <option value="<%=idEstudiante%>"><%=idEstudiante%>-<%=nombreEstudiante%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nota Final</label>
                            <input class="form-control" type="number" step="any" id="notaFinal" name="notaFinal" 
                                   placeholder="Ingrese la nota final" >
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Detalle Grupo">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("seguimiento".equals(procedencia)) {
                        GrupoDAO gpDAO = new GrupoDAO();
                        Grupo gp = new Grupo();
                        List<Grupo> listaGrupos = gpDAO.ConsultarGrupos();
                        EstudianteDAO eDAO = new EstudianteDAO();
                        Estudiante e = new Estudiante();
                        List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes();
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioSeguimiento" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Seguimiento Clase a Clase</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Grupos</label>
                            <select class="form-control" name="idGrupo" required="true">
                                <%  String idGrupo = "";
                                    for (Grupo g : listaGrupos) {
                                        idGrupo = String.valueOf(g.getIdGrupo());%>
                                <option><%=idGrupo%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estudiantes</label>
                            <select class="form-control" name="idEstudiante" required="true">
                                <%  String idEstudiante = "";
                                    String nombreEstudiante = "";
                                    for (Estudiante est : listaEstudiantes) {
                                        idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                        nombreEstudiante = est.getNombreEstudiante();%>
                                <option value="<%=idEstudiante%>"><%=idEstudiante%>-<%=nombreEstudiante%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fecha</label>
                            <input class="form-control" type="date" id="fechaClase" name="fechaClase" required>
                        </div>
                        <div class="form-group">
                            <label>Objetivo de la Clase</label>
                            <input class="form-control" type="text" id="objetivoClase" name="objetivoClase">
                        </div>
                        <div class="form-group">
                            <label>Nota de la clase</label>
                            <input class="form-control" type="number" step="any" id="notaClase" name="notaClase" 
                                   placeholder="Ingrese la nota" >
                        </div>
                        <div class="form-group">
                            <label>Observaciones de la clase</label>
                            <textarea class="form-control" name="observacionClase" id="observacionClase" rows="3" cols="30" 
                                      placeholder="Observaciones de la clase"></textarea>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear">Crear
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioActualizar" value="Actualizar">Actualizar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioBorrar" value="Borrar">Borrar
                                </label>
                                <label>
                                    <input type="radio" name="opciones" id="radioConsultar" value="Consultar" checked>Consultar
                                </label>
                            </div>
                        </center>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Seguimiento">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <!Pre factura generada para los espacio-!>

                <%if ("PreFactura".equals(procedencia)) {
                    tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                    tipoDocumento td = new tipoDocumento();
                    List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                    AcudienteDAO aDAO = new AcudienteDAO();
                    Acudiente a = new Acudiente();
                    List<Acudiente> listaAcudiente = aDAO.ConsultarAcudientes();
                    FacturaDAO fDAO = new FacturaDAO();
                    Factura f = new Factura();
                    List<Factura> listafac = fDAO.ConsultarFacturas();
                    ServicioDAO servDAO = new ServicioDAO();
                    Servicio serv = new Servicio();
                    List<Servicio> listaServicios = servDAO.ConsultarServicios();
                    EstudianteDAO eDAO = new EstudianteDAO();
                    Estudiante e = new Estudiante();
                    List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes(); 
                %>  
                <br>
                <div class="container col-lg-10 border">
                    <form class="form" name="frmPreFactura" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Guardar Pre-Factura</strong></p>
                        </div> 
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Numero de Factura</label>
                                <input readonly="true" class="form-control" type="number" name="NroPreFactura" placeholder="Ingrese el Numero de Factura" >
                            </div>
                            <div class = "col-3">
                                <label>Fecha Expedición</label>
                                <input class="form-control" type="date"  name="fechaExp" placeholder="Ingrese Fecha Expedición" required="true">
                            </div>
                            <div class = "col-3">
                                <label>Fecha de vencimiento</label>
                                <input class="form-control" type="date" name="fechaVenc" placeholder="Ingrese Fecha de vencimiento" required="true">
                            </div>
                            <div class="col-3"> <label for="estado"> Estado
                                </label>
                                <select class="custom-select" id="estado" name = "estado" required>
                                    <option selected disabled value=""> Seleccione...</option>
                                    <Option value="Pendiente"> Pendiente </Option>
                                    <Option value="Aprobado"> Aprobado </Option>
                                    <Option value="Anulado"> Anulado </Option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class="col-2"> <label for="tipodoc"> Tipo de documento</label>
                                <select class="form-control" name="tipodoc" id="tipodoc" required>
                                    <%  String idTipoDocumento = "";
                                        String nombreDocumento = "";
                                        for (tipoDocumento tdo : listaTtipoDocumento) {
                                            nombreDocumento = tdo.getNombreDocumento();
                                            idTipoDocumento = tdo.getIdDocumento();
                                    %>
                                    <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class = "col-3">
                                <label>Identificacion</label>
                                <input class="form-control" type="number" name="identificacion" placeholder="Ingrese la Identificacion" required="true">
                            </div>
                            <!--<div class = "col-4">
                                <label>Nombre Estudiante</label>
                                <input class="form-control" type="text" name="estudiante" placeholder="Ingrese Nombre" required="true">
                            </div>-->
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Nombre estudiante</label>
                                <select class="form-control" name="estudiante" required>
                                    <%  String idEstudiante = "";
                                        String nombreEstudiante = "";
                                        for (Estudiante est : listaEstudiantes) {
                                            idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                            nombreEstudiante = est.getNombreEstudiante();%>
                                    <option value="<%=nombreEstudiante%>"><%=idEstudiante%> - <%=nombreEstudiante%></option>
                                    <%}%>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label>Acudiente</label>
                            <select class="form-control" name="acudiente" required>
                                <%  String identAcudiente = "";
                                    String nombreAcudiente = "";
                                    for (Acudiente ac : listaAcudiente) {
                                        nombreAcudiente = String.valueOf(ac.getNombreAcudiente());
                                        identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                %>
                                <option value="<%=nombreAcudiente%>"><%=identAcudiente%> - <%=nombreAcudiente%></option>
                                <%}%>
                            </select>
                        </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Servicio a facturar</label>
                                <select class="form-control" name="servicio" required>
                                    <%  int idServicio = 0;
                                        String nombreServicio = "";
                                        
                                        for (Servicio s : listaServicios) {
                                            idServicio = s.getIdServicio();
                                            nombreServicio = s.getNombreServicio();
                                             %>
                                    <option value="<%=idServicio%>"><%=idServicio%> - <%=nombreServicio%></option>
                                    <%}%>
                                </select>
                            </div>
                            </div>
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Forma de pago</label>
                                <select class="form-control" name="formaPago" required>
                                    <option value="Contado">Contado</option>
                                    <option value="Tarjeta crédito">Tarjeta crédito</option>
                                    <option value="Tarjeta débito">Tarjeta débito</option>
                                </select>
                            </div>
                            </div> 
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Vínculo con la institución</label>
                                <select class="form-control" name="vinculo" required>
                                    <option value="Empleado">Empleado</option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Egresado">Egresado</option>
                                    <option value="Externo">Externo</option>
                                </select>
                            </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Sub total</label>
                                <input readonly class="form-control" type="number" name="subtotal" placeholder="">
                            </div> 
                            
                            <div class = "col-2">
                                <label>Iva Porcentaje</label>
                                <input readonly class="form-control" type="number" name="porcIva" placeholder="">
                            </div>
                            <div class = "col-2">
                                <label>Iva</label>
                                <input readonly class="form-control" type="number" name="iva" placeholder="" >
                            </div>
                            <div class = "col-2">
                                <label>Porc Descuento</label>
                                <input readonly class="form-control" type="number" name="porcDesc" placeholder="" >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Descuento</label>
                                <input readonly class="form-control" type="number" name="descuento" placeholder="" >
                            </div>
                            <div class = "col-2">
                                <label>Valor Total</label>
                                <input readonly class="form-control" type="number" name="valortotal" placeholder="" >
                            </div>
                            <div class = "col-11">
                                <div class="form-group">
                                    <label>Observaciones:</label>
                                    <textarea class="form-control" name="observaciones" 
                                              rows="3" cols="30" placeholder="Ingrese los estudios" value="">
                                    </textarea>
                                </div>
                            </div>
                            <br><br><br><br><br>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear" checked="true" ><!--Crear-->
                                </label>
                            </div>
                        </center>
                        <div>
                            <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                        </div>
                        <div class = "col-11">
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="procesar pre-factura">
                        </div>
                    </form>
                </div>
                <%}%>

                <%if ("Factura".equals(procedencia)) {
                    tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                    tipoDocumento td = new tipoDocumento();
                    List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                    AcudienteDAO aDAO = new AcudienteDAO();
                    Acudiente a = new Acudiente();
                    List<Acudiente> listaAcudiente = aDAO.ConsultarAcudientes();
                    FacturaDAO fDAO = new FacturaDAO();
                    Factura f = new Factura();
                    List<Factura> listafac = fDAO.ConsultarFacturas();
                    ServicioDAO servDAO = new ServicioDAO();
                    Servicio serv = new Servicio();
                    List<Servicio> listaServicios = servDAO.ConsultarServicios();
                    EstudianteDAO eDAO = new EstudianteDAO();
                    Estudiante e = new Estudiante();
                    List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes(); 
                %>  
                <br>
                <div class="container col-lg-10 border">
                    <form class="form" name="frmFactura" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Guardar Factura</strong></p>
                        </div> 
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Numero de Factura</label>
                                <input readonly="true" class="form-control" type="number" name="NroFactura" placeholder="Ingrese el Numero de Factura" >
                            </div>
                            <div class = "col-3">
                                <label>Fecha Expedición</label>
                                <input class="form-control" type="date"  name="fechaExp" placeholder="Ingrese Fecha Expedición" required="true">
                            </div>
                            <div class = "col-3">
                                <label>Fecha de vencimiento</label>
                                <input class="form-control" type="date" name="fechaVenc" placeholder="Ingrese Fecha de vencimiento" required="true">
                            </div>
                            <div class="col-3"> <label for="estado"> Estado
                                </label>
                                <select class="custom-select" id="estado" name = "estado" required>
                                    <option selected disabled value=""> Seleccione...</option>
                                    <Option value="Pagar"> Sin Pagar </Option>
                                    <Option value="Pagado"> Pagado </Option>
                                    <Option value="Anulado"> Anulado </Option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class="col-2"> <label for="tipodoc"> Tipo de documento</label>
                                <select class="form-control" name="tipodoc" id="tipodoc" required>
                                    <%  String idTipoDocumento = "";
                                        String nombreDocumento = "";
                                        for (tipoDocumento tdo : listaTtipoDocumento) {
                                            nombreDocumento = tdo.getNombreDocumento();
                                            idTipoDocumento = tdo.getIdDocumento();
                                    %>
                                    <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class = "col-3">
                                <label>Identificacion</label>
                                <input class="form-control" type="number" name="identificacion" placeholder="Ingrese la Identificacion" required="true">
                            </div>
                            <!--<div class = "col-4">
                                <label>Nombre Estudiante</label>
                                <input class="form-control" type="text" name="estudiante" placeholder="Ingrese Nombre" required="true">
                            </div>-->
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Nombre estudiante</label>
                                <select class="form-control" name="estudiante" required>
                                    <%  String idEstudiante = "";
                                        String nombreEstudiante = "";
                                        for (Estudiante est : listaEstudiantes) {
                                            idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                            nombreEstudiante = est.getNombreEstudiante();%>
                                    <option value="<%=nombreEstudiante%>"><%=idEstudiante%> - <%=nombreEstudiante%></option>
                                    <%}%>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label>Acudiente</label>
                            <select class="form-control" name="acudiente" required>
                                <%  String identAcudiente = "";
                                    String nombreAcudiente = "";
                                    for (Acudiente ac : listaAcudiente) {
                                        nombreAcudiente = String.valueOf(ac.getNombreAcudiente());
                                        identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                %>
                                <option value="<%=nombreAcudiente%>"><%=identAcudiente%> - <%=nombreAcudiente%></option>
                                <%}%>
                            </select>
                        </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Servicio a facturar</label>
                                <select class="form-control" name="servicio" required>
                                    <%  int idServicio = 0;
                                        String nombreServicio = "";
                                        
                                        for (Servicio s : listaServicios) {
                                            idServicio = s.getIdServicio();
                                            nombreServicio = s.getNombreServicio();
                                             %>
                                    <option value="<%=idServicio%>"><%=idServicio%> - <%=nombreServicio%></option>
                                    <%}%>
                                </select>
                            </div>
                            </div>
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Forma de pago</label>
                                <select class="form-control" name="formaPago" required>
                                    <option value="Contado">Contado</option>
                                    <option value="Tarjeta crédito">Tarjeta crédito</option>
                                    <option value="Tarjeta débito">Tarjeta débito</option>
                                </select>
                            </div>
                            </div> 
                            <div class = "col-3">
                            <div class="form-group">
                                <label>Vínculo con la institución</label>
                                <select class="form-control" name="vinculo" required>
                                    <option value="Empleado">Empleado</option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Egresado">Egresado</option>
                                    <option value="Externo">Externo</option>
                                </select>
                            </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Sub total</label>
                                <input readonly class="form-control" type="number" name="subtotal" placeholder="">
                            </div> 
                            
                            <div class = "col-2">
                                <label>Iva Porcentaje</label>
                                <input readonly class="form-control" type="number" name="porcIva" placeholder="">
                            </div>
                            <div class = "col-2">
                                <label>Iva</label>
                                <input readonly class="form-control" type="number" name="iva" placeholder="" >
                            </div>
                            <div class = "col-2">
                                <label>Porc Descuento</label>
                                <input readonly class="form-control" type="number" name="porcDesc" placeholder="" >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Descuento</label>
                                <input readonly class="form-control" type="number" name="descuento" placeholder="" >
                            </div>
                            <div class = "col-2">
                                <label>Valor Total</label>
                                <input readonly class="form-control" type="number" name="valortotal" placeholder="" >
                            </div>
                            <div class = "col-11">
                                <div class="form-group">
                                    <label>Observaciones:</label>
                                    <textarea class="form-control" name="observaciones" 
                                              rows="3" cols="30" placeholder="Ingrese los estudios" value="">
                                    </textarea>
                                </div>
                            </div>
                            <br><br><br><br><br>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Crear" checked="true" ><!--Crear-->
                                </label>
                            </div>
                        </center>
                        <div>
                            <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                        </div>
                        <div class = "col-11">
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="procesar factura">
                        </div>
                    </form>
                </div>
                <%}%>
                
                <%if ("ActFactura".equals(procedencia)) {
                    String nroFactura = request.getParameter("NroFactura");
                    tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                    tipoDocumento td = new tipoDocumento();
                    List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                    AcudienteDAO aDAO = new AcudienteDAO();
                    Acudiente a = new Acudiente();
                    List<Acudiente> listaAcudiente = aDAO.ConsultarAcudientes();
                    FacturaDAO fDAO = new FacturaDAO();
                    Factura f = new Factura();
                    List<Factura> listafac = fDAO.ConsultarFacturas();
                    List<Factura> ListaFactura = fDAO.ConsultarFactura(Integer.parseInt(nroFactura));
                    ServicioDAO servDAO = new ServicioDAO();
                    Servicio serv = new Servicio();
                    List<Servicio> listaServicios = servDAO.ConsultarServicios();
                    EstudianteDAO eDAO = new EstudianteDAO();
                    Estudiante e = new Estudiante();
                    List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes();
                    
                    String fechaExp="";
                    String fechaVenc="";
                    String tipoDoc="";
                    String identificacion="";
                    String estudiante ="";
                    String acudiente="";
                    String descripcion="";
                    double subtotal;
                    double porcIVA;
                    double iva;
                    double porcDesc;
                    double Descuento;
                    double valorTotal;
                    String estado="";
                    String observaciones="";
                    for(Factura fac:ListaFactura)
                        {
                        nroFactura = String.valueOf(fac.getNroFactura());
                        fechaExp = fac.getFechaExp();
                        estado = fac.getEstado();
                        tipoDoc = fac.getTipoDocumento();
                        fechaVenc = fac.getFechaVenc();
                        identificacion = String.valueOf(fac.getIdentificacion());
                        estudiante = fac.getAlumno();
                        acudiente = fac.getAcudiente();
                        descripcion = fac.getDescripcion();
                        subtotal = fac.getSubtotal();
                        porcIVA = fac.getPorcIva();
                        iva = fac.getIva();
                        porcDesc = fac.getPorcDescuento();
                        Descuento = fac.getDescuento();
                        valorTotal = fac.getValorTotal();
                        observaciones = fac.getObservaciones();
                %>  
                <br>
                <div class="container col-lg-10 border">
                    <form class="form" name="frmFactura" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Actualizar Factura</strong></p>
                        </div> 
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Numero de Factura</label>
                                <input readonly="true" class="form-control" type="number" name="NroFactura" 
                                       placeholder="Ingrese el Numero de Factura" value="<%=nroFactura%>">
                            </div>
                            <div class = "col-3">
                                <label>Fecha Expedición</label>
                                <input class="form-control" type="date"  name="fechaExp" 
                                       placeholder="Ingrese Fecha Expedición" value="<%=fechaExp%>">
                            </div>
                            <div class = "col-3">
                                <label>Fecha de vencimiento</label>
                                <input class="form-control" type="date" name="fechaVenc" 
                                       placeholder="Ingrese Fecha de vencimiento" value="<%=fechaVenc%>">
                            </div>
                            <div class="col-3"> <label for="estado"> Estado
                                </label>
                                <select class="custom-select" id="estado" name = "estado" required>
                                    <option selected disabled value=""> Seleccione...</option>
                                    <option value="Sin Pagar"> Sin Pagar </option>
                                    <option value="Pagado"> Pagado </option>
                                    <option value="Anulado"> Anulado </option>
                                    <option selected="true" value="<%=estado%>"> <%=estado%> </option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class="col-2"> <label for="tipodoc"> Tipo de documento</label>
                                <select class="form-control" name="tipodoc" id="tipodoc" required>
                                    <%  String idTipoDocumento = "";
                                        String nombreDocumento = "";
                                        for (tipoDocumento tdo : listaTtipoDocumento) {
                                            nombreDocumento = tdo.getNombreDocumento();
                                            idTipoDocumento = tdo.getIdDocumento();
                                    %>
                                    <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                    <%}%>
                                    <option selected="true" value="<%=tipoDoc%>"> <%=tipoDoc%> </option>
                                </select>
                            </div>
                            <div class = "col-3">
                                <label>Identificacion</label>
                                <input class="form-control" type="number" name="identificacion" placeholder="Ingrese la Identificacion" value="<%=identificacion%>">    
                            </div>
                                
                            <div class = "col-3">
                                <label>Nombre estudiante</label>
                                <select class="form-control" name="estudiante" required>
                                    <%  String idEstudiante = "";
                                        String nombreEstudiante = "";
                                        for (Estudiante est : listaEstudiantes) {
                                            idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                            nombreEstudiante = est.getNombreEstudiante();%>
                                    <option value="<%=idEstudiante%>"><%=idEstudiante%> - <%=nombreEstudiante%></option>
                                    <%}%>
                                    <option selected="true" value="<%=estudiante%>"> <%=estudiante%> </option>
                                </select>
                            </div>
                            
                            <div class = "col-3">
                                <label>Acudiente</label>
                                <select class="form-control" name="acudiente" required>
                                    <%  String identAcudiente = "";
                                        String nombreAcudiente = "";
                                        for (Acudiente ac : listaAcudiente) {
                                            nombreAcudiente = String.valueOf(ac.getNombreAcudiente());
                                            identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                    %>
                                    <option value="<%=identAcudiente%>"><%=identAcudiente%> - <%=nombreAcudiente%></option>
                                    <%}%>
                                    <option selected="true" value="<%=acudiente%>"> <%=acudiente%> </option>
                                </select>
                            </div>
                        <br><br>
                        <div class="form-row">
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Servicio a facturar</label>
                                <select class="form-control" name="servicio" required>
                                    <%  int idServicio = 0;
                                        String nombreServicio = "";
                                        
                                        for (Servicio s : listaServicios) {
                                            idServicio = s.getIdServicio();
                                            nombreServicio = s.getNombreServicio();
                                             %>
                                    <option value="<%=idServicio%>"><%=idServicio%> - <%=nombreServicio%></option>
                                    <%}%>
                                    <option selected="true" value="<%=descripcion%>"> <%=descripcion%> </option>
                                </select>
                            </div>
                            </div>
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Forma de pago</label>
                                <select class="form-control" name="formaPago" required>
                                    <option value="Contado">Contado</option>
                                    <option value="Tarjeta crédito">Tarjeta crédito</option>
                                    <option value="Tarjeta débito">Tarjeta débito</option>
                                </select>
                            </div>
                            </div> 
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Vínculo con la institución</label>
                                <select class="form-control" name="vinculo" required>
                                    <option value="Empleado">Empleado</option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Egresado">Egresado</option>
                                    <option value="Externo">Externo</option>
                                </select>
                            </div>
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class = "col-3">
                                <label>Sub total</label>
                                <input readonly class="form-control" type="number" name="subtotal" placeholder=""
                                    value="<%=subtotal%>">
                            </div> 
                            
                            <div class = "col-3">
                                <label>Iva Porcentaje</label>
                                <input readonly class="form-control" type="number" name="porcIva" placeholder=""
                                       value="<%=porcIVA%>">
                            </div>
                            <div class = "col-3">
                                <label>Iva</label>
                                <input readonly class="form-control" type="number" name="iva" placeholder=""
                                       value="<%=iva%>">
                            </div>
                            <div class = "col-3">
                                <label>Porc Descuento</label>
                                <input readonly class="form-control" type="number" name="porcDesc" placeholder="" 
                                       value="<%=porcDesc%>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-6">
                                <label>Descuento</label>
                                <input readonly class="form-control" type="number" name="descuento" placeholder="" 
                                       value="<%=Descuento%>">
                            </div>
                            <div class = "col-6">
                                <label>Valor Total</label>
                                <input readonly class="form-control" type="number" name="valortotal" placeholder="" 
                                       value="<%=valorTotal%>">
                            </div>
                            </div>
                            <div class = "col-11">
                                <div class="form-group">
                                    <label>Observaciones:</label>
                                    <textarea class="form-control" name="observaciones" 
                                              rows="3" cols="30" placeholder="Ingrese los estudios" 
                                              value="<%=observaciones%>">
                                    </textarea>
                                </div>
                            </div>
                        <%}%>
                            <br><br><br><br><br>
                        
                        
                        <div class = "col-11">
                        <center>
                            <div class = "col-11">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Actualizar" checked="true" >
                                </label>
                            </div>
                            </div>
                        </center>
                        
                        <div class = "col-11">
                            <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                        </div>
                        
                        <div class = "col-11">
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="procesar factura">
                        </div>
                        </div>
                        
                    </form>
                        
                <%}%>
                
                
                <%if ("ActPreFactura".equals(procedencia)) {
                    String nroPreFactura = request.getParameter("NroPreFactura");
                    tipoDocumentoDAO tdDAO = new tipoDocumentoDAO();
                    tipoDocumento td = new tipoDocumento();
                    List<tipoDocumento> listaTtipoDocumento = tdDAO.ConsultarTipoDocumentos();
                    AcudienteDAO aDAO = new AcudienteDAO();
                    Acudiente a = new Acudiente();
                    List<Acudiente> listaAcudiente = aDAO.ConsultarAcudientes();
                    PrefacturaDAO pfDAO = new PrefacturaDAO();
                    Prefactura pf = new Prefactura();
                    List<Prefactura> listaprefac = pfDAO.ConsultarPrefacturas();
                    List<Prefactura> ListapreFactura = pfDAO.ConsultarPreFactura(Integer.parseInt(nroPreFactura));
                    ServicioDAO servDAO = new ServicioDAO();
                    Servicio serv = new Servicio();
                    List<Servicio> listaServicios = servDAO.ConsultarServicios();
                    EstudianteDAO eDAO = new EstudianteDAO();
                    Estudiante e = new Estudiante();
                    List<Estudiante> listaEstudiantes = eDAO.ConsultarEstudiantes();
                    
                    String fechaExp="";
                    String fechaVenc="";
                    String tipoDoc="";
                    String identificacion="";
                    String estudiante ="";
                    String acudiente="";
                    String descripcion="";
                    double subtotal;
                    double porcIVA;
                    double iva;
                    double porcDesc;
                    double Descuento;
                    double valorTotal;
                    String estado="";
                    String observaciones="";
                    for(Prefactura pfac:ListapreFactura)
                        {
                        nroPreFactura = String.valueOf(pfac.getNroPrefactura());
                        fechaExp = pfac.getFechaExp();
                        estado = pfac.getEstado();
                        tipoDoc = pfac.getTipoDocumento();
                        fechaVenc = pfac.getFechaVenc();
                        identificacion = String.valueOf(pfac.getIdentificacion());
                        estudiante = pfac.getAlumno();
                        acudiente = pfac.getAcudiente();
                        descripcion = pfac.getDescripcion();
                        subtotal = pfac.getSubtotal();
                        porcIVA = pfac.getPorcIva();
                        iva = pfac.getIva();
                        porcDesc = pfac.getPorcDescuento();
                        Descuento = pfac.getDescuento();
                        valorTotal = pfac.getValorTotal();
                        observaciones = pfac.getObservaciones();
                %>  
                <br>
                <div class="container col-lg-10 border">
                    <form class="form" name="frmPreFactura" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Convertir Pre factura en Factura</strong></p>
                        </div> 
                        <div class="form-row">
                            <div class = "col-2">
                                <label>Numero de Pre-Factura</label>
                                <input readonly="true" class="form-control" type="number" name="NroPreFactura" 
                                       placeholder="Ingrese el Numero de Factura" value="<%=nroPreFactura%>">
                            </div>
                            <div class = "col-3">
                                <label>Fecha Expedición</label>
                                <input class="form-control" type="date"  name="fechaExp" 
                                       placeholder="Ingrese Fecha Expedición" value="<%=fechaExp%>">
                            </div>
                            <div class = "col-3">
                                <label>Fecha de vencimiento</label>
                                <input class="form-control" type="date" name="fechaVenc" 
                                       placeholder="Ingrese Fecha de vencimiento" value="<%=fechaVenc%>">
                            </div>
                            <div class="col-3"> <label for="estado"> Estado
                                </label>
                                <select class="custom-select" id="estado" name = "estado" required>
                                    <option selected disabled value=""> Seleccione...</option>
                                    <option value="Sin Pagar"> Sin Pagar </option>
                                    <option selected="true" value="Pagado"> Pagado </option>
                                    <option value="Anulado"> Anulado </option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-row">
                            <div class="col-2"> <label for="tipodoc"> Tipo de documento</label>
                                <select class="form-control" name="tipodoc" id="tipodoc" required>
                                    <%  String idTipoDocumento = "";
                                        String nombreDocumento = "";
                                        for (tipoDocumento tdo : listaTtipoDocumento) {
                                            nombreDocumento = tdo.getNombreDocumento();
                                            idTipoDocumento = tdo.getIdDocumento();
                                    %>
                                    <option value="<%=idTipoDocumento%>"><%=nombreDocumento%></option>
                                    <%}%>
                                    <option selected="true" value="<%=tipoDoc%>"> <%=tipoDoc%> </option>
                                </select>
                            </div>
                            <div class = "col-3">
                                <label>Identificacion</label>
                                <input class="form-control" type="number" name="identificacion" placeholder="Ingrese la Identificacion" value="<%=identificacion%>">    
                            </div>
                                
                            <div class = "col-3">
                                <label>Nombre estudiante</label>
                                <select class="form-control" name="estudiante" required>
                                    <%  String idEstudiante = "";
                                        String nombreEstudiante = "";
                                        for (Estudiante est : listaEstudiantes) {
                                            idEstudiante = String.valueOf(est.getIdentificacionEstudiante());
                                            nombreEstudiante = est.getNombreEstudiante();%>
                                    <option value="<%=idEstudiante%>"><%=idEstudiante%> - <%=nombreEstudiante%></option>
                                    <%}%>
                                    <option selected="true" value="<%=estudiante%>"> <%=estudiante%> </option>
                                </select>
                            </div>
                            
                            <div class = "col-3">
                                <label>Acudiente</label>
                                <select class="form-control" name="acudiente" required>
                                    <%  String identAcudiente = "";
                                        String nombreAcudiente = "";
                                        for (Acudiente ac : listaAcudiente) {
                                            nombreAcudiente = String.valueOf(ac.getNombreAcudiente());
                                            identAcudiente = String.valueOf(ac.getIdentificacionAcudiente());
                                    %>
                                    <option value="<%=identAcudiente%>"><%=identAcudiente%> - <%=nombreAcudiente%></option>
                                    <%}%>
                                    <option selected="true" value="<%=acudiente%>"> <%=acudiente%> </option>
                                </select>
                            </div>
                        <br><br>
                        <div class="form-row">
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Servicio a facturar</label>
                                <select class="form-control" name="servicio" required>
                                    <%  int idServicio = 0;
                                        String nombreServicio = "";
                                        
                                        for (Servicio s : listaServicios) {
                                            idServicio = s.getIdServicio();
                                            nombreServicio = s.getNombreServicio();
                                             %>
                                    <option value="<%=idServicio%>"><%=idServicio%> - <%=nombreServicio%></option>
                                    <%}%>
                                    <option selected="true" value="<%=descripcion%>"> <%=descripcion%> </option>
                                </select>
                            </div>
                            </div>
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Forma de pago</label>
                                <select class="form-control" name="formaPago" required>
                                    <option value="Contado">Contado</option>
                                    <option value="Tarjeta crédito">Tarjeta crédito</option>
                                    <option value="Tarjeta débito">Tarjeta débito</option>
                                </select>
                            </div>
                            </div> 
                            <div class = "col-4">
                            <div class="form-group">
                                <label>Vínculo con la institución</label>
                                <select class="form-control" name="vinculo" required>
                                    <option value="Empleado">Empleado</option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Egresado">Egresado</option>
                                    <option value="Externo">Externo</option>
                                </select>
                            </div>
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class = "col-3">
                                <label>Sub total</label>
                                <input readonly class="form-control" type="number" name="subtotal" placeholder=""
                                    value="<%=subtotal%>">
                            </div> 
                            
                            <div class = "col-3">
                                <label>Iva Porcentaje</label>
                                <input readonly class="form-control" type="number" name="porcIva" placeholder=""
                                       value="<%=porcIVA%>">
                            </div>
                            <div class = "col-3">
                                <label>Iva</label>
                                <input readonly class="form-control" type="number" name="iva" placeholder=""
                                       value="<%=iva%>">
                            </div>
                            <div class = "col-3">
                                <label>Porc Descuento</label>
                                <input readonly class="form-control" type="number" name="porcDesc" placeholder="" 
                                       value="<%=porcDesc%>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class = "col-6">
                                <label>Descuento</label>
                                <input readonly class="form-control" type="number" name="descuento" placeholder="" 
                                       value="<%=Descuento%>">
                            </div>
                            <div class = "col-6">
                                <label>Valor Total</label>
                                <input readonly class="form-control" type="number" name="valortotal" placeholder="" 
                                       value="<%=valorTotal%>">
                            </div>
                            </div>
                            <div class = "col-11">
                                <div class="form-group">
                                    <label>Observaciones:</label>
                                    <textarea class="form-control" name="observaciones" 
                                              rows="3" cols="30" placeholder="Ingrese los estudios" 
                                              value="<%=observaciones%>">
                                    </textarea>
                                </div>
                            </div>
                        <%}%>
                            <br><br><br><br><br>
                        
                        
                        <div class = "col-11">
                        <center>
                            <div class = "col-11">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Convertir" checked="true" >
                                </label>
                            </div>
                            </div>
                        </center>
                        
                        <div class = "col-11">
                            <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                        </div>
                        
                        <div class = "col-11">
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="procesar pre-factura">
                        </div>
                        </div>
                        
                    </form>
                <%}%>
                
                
                
                

                <%if ("Cerficado".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmCertificado" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Certificado por Estudiante</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Identificación:</label>
                            <input class="form-control" type="number" name="idEstudiante" placeholder="Ingrese identificación del estudiante" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Generar Certificado Alumno">
                    </form>
                </div>

                <%}%>

                <%if ("pazYSalvo".equals(procedencia)) {
                        //el archivo debe ser escrito de forma horizonal en Excel, el primer y último caracter debe ser un # 
                        //y guardarse como texto delimitado por tabulaciones.
                %>    
                <div class="container col-lg-3">
                    <form name="FormularioPazYSalvo" action="archivo" method="post" enctype="multipart/form-data">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Cargue masivo de Paz y Salvo</strong></p>
                        </div>
                        <div class="form-group">
                            <label for="archivoCargue">Archivo de Cargue</label>
                            <input type="file" class="form-control-file" id="inputFile" name="inputFile">
                        </div>
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Procesar Cargue">
                    </form>
                    <div>
                        <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                    </div>
                </div>
                <%}%>

                <%if ("acercaDe".equals(procedencia)) {

                %>    
                <div class="container col-lg-7"> 
                    <div class="form-group text-justify">
                        <center>
                            <img src="img/jaime_isaza_cadavid.png" width="250"/>
                        </center>
                        <p><strong>Acerca de</strong></p>
                        <p>El software "Escuela de iniciación deportiva" permite administrar de forma fácil
                            y rápida los diferentes deportes y categorías con la que cuenta la Facultad 
                            de Educación Física del Politécnico Colombiano Jaime izasa Cadavid.</p>
                        <p><b>Versión:</b> 01</p>
                        <p><b>Fecha:</b> Abril 2020</p>
                        <p><b>Desarrolladores:</b></p>
                        <p>Edison Cardona - Daniel Monsalve - Anderson Camacho - Brandon Muñeton</p>
                    </div>
                </div>
                <%}%>

                <%if ("ImprimirPrefactura".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmFactura" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Impresión de pre-facturas</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Nro. de pre-factura:</label>
                            <input class="form-control" type="number" name="NroPrefactura" placeholder="Ingrese el nro. de la pre-factura" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Imprimir Pre-factura">
                    </form>
                </div>

                <%}%>



                <%if ("ImprimirFactura".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmFactura" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Impresión de facturas</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Nro. de factura:</label>
                            <input class="form-control" type="number" name="NroFactura" placeholder="Ingrese el nro. de la factura" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Imprimir Factura">
                    </form>
                </div>

                <%}%>

                <%if ("AnularFactura".equals(procedencia)) {
                    String nroFactura = request.getParameter("NroFactura");
                    FacturaDAO fDAO = new FacturaDAO();
                    Factura f = new Factura();
                    List<Factura> ListaFactura = fDAO.ConsultarFactura(Integer.parseInt(nroFactura));
                    
                    String observaciones="";
                    for(Factura fac:ListaFactura)
                        {
                        nroFactura = String.valueOf(fac.getNroFactura());
                        observaciones = fac.getObservaciones();
                %>    
                <br>
                <div class="container col-lg-3 border">
                    <form class="form" name="frmAnulFactura" action="controlador" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Anulación de facturas</strong></p>
                        </div>
                        <div class="form-group ">
                        
                            <label>Nro. de factura:</label>
                            <input readonly class="form-control" type="number" name="NroFactura" 
                                   placeholder="Ingrese el nro. de la factura" required="true" value="<%=nroFactura%>">
                            <div class="form-group">
                                <label>Motivo de anulación</label>
                                <textarea class="form-control" name="observaciones" 
                                          rows="3" cols="30" placeholder="Ingrese los estudios" value="<%=observaciones%>">
                                </textarea>
                            </div>
                            <%}%>
                        </div>
                        <center>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="opciones" id="radioCrear" value="Anular" checked="true" >
                                </label>
                            </div>
                        </center>
                        <div>
                            <center><p><strong>Estado de la transacción:</strong> <%=resultadoOperacion%></p></center>
                        </div>
                        <div class = "col-11">
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="procesar factura">
                        </div>
                    </form>
                </div>

                <%}%>
                
                <%if ("ImprimirNotasGrupo".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmNotasGrupo" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Impresión de notas por grupo</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Nro. de Grupo:</label>
                            <input class="form-control" type="number" name="idGrupo" placeholder="Ingrese el nro. del grupo" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Imprimir Notas Finales">
                    </form>
                </div>

                <%}%>
                
                <%if ("ImprimirNotasGrupoGanaron".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmNotasGrupo" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Impresión de Estudiantes que Ganaron</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Nro. de Grupo:</label>
                            <input class="form-control" type="number" name="idGrupo" placeholder="Ingrese el nro. del grupo" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Imprimir Notas Estudiantes que Ganaron">
                    </form>
                </div>

                <%}%>
                
                <%if ("ImprimirNotasGrupoPerdieron".equals(procedencia)) {

                %>    
                <br>
                <div class="container col-lg-3">
                    <form class="form" name="frmNotasGrupo" action="DescargaPDF" method="post">
                        <div class="form-group text-center">
                            <img src="imagenes/login.png" width="80"/>
                            <p><strong>Impresión de Estudiantes que Perdieron</strong></p>
                        </div>
                        <div class="form-group">
                            <label>Nro. de Grupo:</label>
                            <input class="form-control" type="number" name="idGrupo" placeholder="Ingrese el nro. del grupo" required="true">
                        </div>    
                        <input class="btn btn-success btn-block" type="submit" name="accion" value="Imprimir Notas Estudiantes que Perdieron">
                    </form>
                </div>

                <%}%>
                
                
                <br>
                <div class="col-12">
                    <p class="mt-2 mb-3 text-muted text-center ">&copy;
                        Politécnico Colombiano Jaime Isaza Cadavid © 2020 <br>
                        Campus central* Carrera 48 No. 7 – 151 | El Poblado | PBX: (+57 4) 444 76 54 - (+57 4)
                        319
                        79 00 |
                        NIT: 890980136-6<br>
                        Institución de Educación Superior de caracter pública y departamental sujeta a
                        inspección y
                        vigilancia
                        por parte del Ministerio de Educación.
                        <img src="img/icontec.png" width="70px">           
                    </p>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
